//==================================================================================================================================
class CPEManager
{
public:

	CPEManager();
	~CPEManager();
		
	bool DoesResideInExecutableSection ( uintptr_t AddressToCheck );

	bool IsImage64() { return Is64Bit; }
	bool LoadFile();
	bool MapAsNormal();
	bool OpenFile ( const char* FileToOpen );

	DWORD GetContainingSectionCharacteristicsRVA ( uintptr_t RVA );

	uintptr_t GetAllocationSize();

	PIMAGE_NT_HEADERS32 GetNTHeaders32();
	PIMAGE_NT_HEADERS64 GetNTHeaders64();
	PIMAGE_SECTION_HEADER GetSectionHeader();

	std::vector<uintptr_t>& GetRelocationsVector()
	{
		return RelocationBlock;
	}

	std::vector<uintptr_t>& GetRelocationsVectorRVA()
	{
		return RelocationBlockRVA;
	}

	std::vector<uintptr_t>& GetRelocationsVectorRVA2()
	{
		return RelocationBlockRVA2;
	}

	std::vector<uintptr_t>& GetInstructionGeneralData()
	{
		return InstructionGeneralData;
	}
		
	PIMAGE_BASE_RELOCATION LdrProcessRelocationBlockLongLong ( uintptr_t Address, DWORD Count, PUSHORT TypeOffset, uintptr_t Delta );

	size_t GetSectionCount();

	uintptr_t GetAllocationBase()
	{
		return ( uintptr_t )RawFileMap;
	}
			
private:

	bool IsLoaded;
	bool Is64Bit;
	bool MappedAsNormal;

	HANDLE File;

	LPVOID RawFileMap;
	size_t AllocationSize;

	LARGE_INTEGER FileSize;

	PIMAGE_DOS_HEADER DosHeader;
	PIMAGE_FILE_HEADER FileHeader;

	PIMAGE_NT_HEADERS32 ImageNTHeaders32;
	PIMAGE_NT_HEADERS64 ImageNTHeaders64;

	PIMAGE_SECTION_HEADER SectionHeader;

	std::vector<uintptr_t> RelocationBlock;
	std::vector<uintptr_t> RelocationBlockRVA;
	std::vector<uintptr_t> RelocationBlockRVA2;

	std::vector<uintptr_t> InstructionGeneralData;
	std::vector<uintptr_t> Immediates;
	std::vector<uintptr_t> Displacement;
	std::vector<uintptr_t> DispAndImm;
};
//==================================================================================================================================
#define ROUND_UP(p,align) (((DWORD)(p)+(align)-1) & ~((align)-1))
#define ROUND_DOWN(p,align) ((DWORD)(p) & ~((align)-1))
//==================================================================================================================================