//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "LDE.h"
//==================================================================================================================================
#include "Common.h"
#include "Condenser.h"
#include "Config.h"
#include "Core.h"
#include "PEManager.h"
#include "Thread.h"
#include "FindPattern.h"
#include "Misc.h"
//==================================================================================================================================
uintptr_t GetCodeFromRelocationByOccurance ( CPEManager* File, int iOccurance, uintptr_t In )
{
	uintptr_t Address = 0;

	int iCounter = 0;

	std::vector<uintptr_t>::iterator RelocationsIterator;

	size_t* pFoundInstruction;

	for ( RelocationsIterator = File->GetRelocationsVectorRVA().begin(); RelocationsIterator != File->GetRelocationsVectorRVA().end(); ++RelocationsIterator )
	{		
		if ( *( DWORD_PTR* )( *RelocationsIterator + File->GetAllocationBase() ) == ( File->GetAllocationBase() + In ) && iCounter != iOccurance )
		{
			Address = *RelocationsIterator + File->GetAllocationBase();

			pFoundInstruction = NULL;

			while ( pFoundInstruction == NULL )
			{
				pFoundInstruction = ( size_t* )std::bsearch ( &Address, File->GetInstructionGeneralData().data(),
					File->GetInstructionGeneralData().size(), sizeof ( DWORD_PTR ), compare );

				if ( pFoundInstruction != NULL )
				{
					break;
				}
				else
				{
					Address -= 1;
				}
			}

			iCounter++;
		}

		if ( iCounter == iOccurance )
		{
			break;
		}
	}
	
	return Address;
}
//==================================================================================================================================
void Search ( CFindPattern* pFindPattern, CConfig* pConfig,
	CPEManager* pReference, CPEManager* pCurrent, CThread* pThread, LPVOID lpAllocationCurrent )
{
	bool bHasImmediate = false, bHasDisplacement = false;
		
	CCondenser* pCondenser;

	PBYTE CurrentAddress;

	size_t* pFoundInstruction;

	size_t BestLength, PartialLength, Length;
	
	std::vector<MemoryLocation_t>& MemoryLocation = pConfig->GetConfigEntries();
	std::vector<MemoryLocation_t>::iterator MemoryLocationIterator;

	std::vector<uintptr_t>::iterator RelocationsIterator;

	std::vector<uintptr_t>	empty;

	std::vector<uintptr_t>& RelocationBlockReference = empty;

	uintptr_t CurrentNeutralizeFlags = 0, PreviousNeutralizeFlags = 0;

	uintptr_t Address, BestAddress, RegionEnd, RegionSize, RegionStart, PatternRVA;
	
	if ( pCurrent->IsImage64() )
	{
		pCondenser = new CCondenser ( true, MODE_64, 0xC0000 );
	}
	else
	{
		pCondenser = new CCondenser ( true, MODE_32, 0xC0000 );
	}

	if ( pCondenser == nullptr )
	{
		return;
	}

	for ( size_t SectionIterator = 0; SectionIterator < pCurrent->GetSectionCount(); SectionIterator++ )
	{
		CurrentAddress = ( PBYTE )( ( uintptr_t )lpAllocationCurrent + pCurrent->GetSectionHeader()[ SectionIterator ].VirtualAddress );
		
		memcpy ( CurrentAddress, ( PBYTE )( pCurrent->GetAllocationBase() + pCurrent->GetSectionHeader()[ SectionIterator ].VirtualAddress ), pCurrent->GetSectionHeader()[ SectionIterator ].Misc.VirtualSize );
	}
	
	for ( MemoryLocationIterator = MemoryLocation.begin(); MemoryLocationIterator != MemoryLocation.end(); ++MemoryLocationIterator  )
	{
		CurrentNeutralizeFlags = MemoryLocationIterator->NeutralizeFlags;

		if ( MemoryLocationIterator->StringToSearchFor.length() == 0 )
		{
			if ( pConfig->GetReferenceBinaryVector().size() == 0 ) // no reference binaries
			{
				PatternRVA = pCurrent->GetAllocationBase() + MemoryLocationIterator->RVA;

				RelocationBlockReference = pCurrent->GetRelocationsVector();

				if ( !( pCurrent->GetContainingSectionCharacteristicsRVA ( PatternRVA ) & IMAGE_SCN_MEM_EXECUTE ) )
				{
					if ( std::bsearch ( &PatternRVA, pCurrent->GetRelocationsVector().data(), 
						pCurrent->GetRelocationsVector().size(), sizeof ( uintptr_t ), compare ) )
					{
						for ( RelocationsIterator = pCurrent->GetRelocationsVectorRVA().begin(); RelocationsIterator != pCurrent->GetRelocationsVectorRVA().end(); ++RelocationsIterator )
						{
							if ( *( uintptr_t* )( *RelocationsIterator + pCurrent->GetAllocationBase() ) == PatternRVA )
							{
								Address = *RelocationsIterator + pCurrent->GetAllocationBase();

								pFoundInstruction = NULL;

								while ( pFoundInstruction == NULL )
								{
									pFoundInstruction = ( size_t* )std::bsearch ( &Address, pCurrent->GetInstructionGeneralData().data(),
										pCurrent->GetInstructionGeneralData().size(), sizeof ( uintptr_t ), compare );

									if ( pFoundInstruction != NULL )
									{
										break;
									}
									else
									{
										Address -= 1;
									}
								}

								if ( MemoryLocationIterator->UseCondenser )
								{									
									pCondenser->SetRegionInformation ( pCurrent->GetAllocationBase(), pCurrent->GetAllocationSize() );

									pCondenser->SetMaxBytesToCopy ( MemoryLocationIterator->PatternSize );

									Length = pCondenser->GetFunctionLength ( ( PVOID )Address, true );

									if ( Length > BestLength )
									{
										BestLength = Length;
										
										BestAddress = ( uintptr_t )Address;

										PatternRVA = BestAddress;

										PartialLength = pCondenser->GetLengthOfPartial ( pCondenser->GetFunctionBeginPartialIndex() );
									}
									
								}
								else
								{
									PatternRVA = Address;

									break;
								}
							}
						}
					}
				}
				else
				{					
					if ( MemoryLocationIterator->UseCondenser )
					{
						pCondenser->SetRegionInformation ( pCurrent->GetAllocationBase(), pCurrent->GetAllocationSize() );

						pCondenser->SetMaxBytesToCopy ( MemoryLocationIterator->PatternSize );

						PartialLength = 0;

						if ( pCondenser->GetFunctionLength ( ( PVOID )PatternRVA, true ) )
						{
							PartialLength = pCondenser->GetLengthOfPartial ( pCondenser->GetFunctionBeginPartialIndex() );
						}
					}
				}
			}
			else
			{
				if ( MemoryLocationIterator->ReferenceBinaryIndex != 0 )
				{
					PatternRVA = pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetAllocationBase() + MemoryLocationIterator->RVA;

					RelocationBlockReference = pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVector();
			
					if ( !( pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetContainingSectionCharacteristicsRVA ( PatternRVA ) & IMAGE_SCN_MEM_EXECUTE ) )
					{
						BestLength = 0;

						if ( std::bsearch ( &MemoryLocationIterator->RVA, pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVectorRVA().data(), 
							pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVectorRVA().size(), sizeof ( uintptr_t ), compare ) 
						||
							std::bsearch ( &PatternRVA, pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVector().data(), 
							pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVector().size(), sizeof ( uintptr_t ), compare ) )
						{
							for ( RelocationsIterator = pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVectorRVA().begin(); RelocationsIterator != pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVectorRVA().end(); ++RelocationsIterator )
							{
								if ( *( uintptr_t* )( *RelocationsIterator + pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetAllocationBase() ) == PatternRVA )
								{
									Address = *RelocationsIterator + pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetAllocationBase();

									pFoundInstruction = NULL;

									while ( pFoundInstruction == NULL )
									{
										pFoundInstruction = ( size_t* )std::bsearch ( &Address, pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetInstructionGeneralData().data(),
											pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetInstructionGeneralData().size(), sizeof ( DWORD_PTR ), compare );
											
										if ( pFoundInstruction != NULL )
										{
											break;
										}
										else
										{
											Address -= 1;
										}
									}

									if ( MemoryLocationIterator->UseCondenser )
									{
										pCondenser->SetRegionInformation (  pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetAllocationBase(),  pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetAllocationSize() );

										pCondenser->SetMaxBytesToCopy ( MemoryLocationIterator->PatternSize );

										Length = pCondenser->GetFunctionLength ( ( PVOID )Address, true );

										if ( Length > BestLength )
										{
											BestLength = Length;

											BestAddress = ( uintptr_t )Address;

											PatternRVA = BestAddress;

											PartialLength = pCondenser->GetLengthOfPartial ( pCondenser->GetFunctionBeginPartialIndex() );
										}
									}
									else
									{
										PatternRVA = Address;

										break;
									}
								}
							}
						}
					}
				}
				else
				{
					PatternRVA = pCurrent->GetAllocationBase() + MemoryLocationIterator->RVA;

					RelocationBlockReference = pCurrent->GetRelocationsVector();
			
					if ( !( pCurrent->GetContainingSectionCharacteristicsRVA ( PatternRVA ) & IMAGE_SCN_MEM_EXECUTE ) )
					{
							
						BestLength = 0;

						if ( 
							std::bsearch ( &PatternRVA, pCurrent->GetRelocationsVector().data(), 
							pCurrent->GetRelocationsVector().size(), sizeof ( uintptr_t ), compare ) )
						{
							for ( RelocationsIterator = pCurrent->GetRelocationsVectorRVA().begin(); RelocationsIterator != pCurrent->GetRelocationsVectorRVA().end(); ++RelocationsIterator )
							{
								if ( *( uintptr_t* )( *RelocationsIterator + pCurrent->GetAllocationBase() ) == PatternRVA )
								{				
									Address = *RelocationsIterator + pCurrent->GetAllocationBase();

									pFoundInstruction = NULL;

									while ( pFoundInstruction == NULL )
									{
										pFoundInstruction = ( size_t* )std::bsearch ( &Address, pCurrent->GetInstructionGeneralData().data(),
											pCurrent->GetInstructionGeneralData().size(), sizeof ( DWORD_PTR ), compare );

										if ( pFoundInstruction != NULL )
										{
											break;
										}
										else
										{
											Address -= 1;
										}
									}

									if ( MemoryLocationIterator->UseCondenser )
									{
										pCondenser->SetRegionInformation ( pCurrent->GetAllocationBase(), pCurrent->GetAllocationSize() );

										pCondenser->SetMaxBytesToCopy ( MemoryLocationIterator->PatternSize );

										Length = pCondenser->GetFunctionLength ( ( PVOID )Address, true );

										if ( Length > BestLength )
										{
											BestLength = Length;

											BestAddress = ( uintptr_t )Address;

											PatternRVA = BestAddress;

											PartialLength = pCondenser->GetLengthOfPartial ( pCondenser->GetFunctionBeginPartialIndex() );
										}
									}
									else
									{
										PatternRVA = Address;

										break;
									}
								}
							}
						}
					}
				}
			}
		}

		if ( MemoryLocationIterator->StringToSearchFor.length() != 0 )
		{
			if ( MemoryLocationIterator->SearchBackward
				|| MemoryLocationIterator->SearchForward
				|| MemoryLocationIterator->SearchBetween )
			{
				AddRVAEntry ( &( *MemoryLocationIterator ) ); // add an empty RVA entry
			}
			else
			{
				for ( size_t SectionCounter = 0; SectionCounter < pCurrent->GetSectionCount(); SectionCounter++ )
				{
					if ( pCurrent->GetSectionHeader()[ SectionCounter ].Characteristics & IMAGE_SCN_MEM_READ )
					{
						RegionStart = pCurrent->GetSectionHeader()[ SectionCounter ].VirtualAddress;

						CurrentAddress = ( PBYTE )( ( uintptr_t )lpAllocationCurrent + RegionStart );
			
						RegionSize = pCurrent->GetSectionHeader()[ SectionCounter ].Misc.VirtualSize;

						RegionStart = ( uintptr_t )CurrentAddress;
						RegionEnd = RegionStart + RegionSize;

						pFindPattern->FindPatternData ( pCurrent, ( uintptr_t )lpAllocationCurrent, ( uintptr_t )CurrentAddress, RegionStart, RegionSize, 
							( PBYTE )MemoryLocationIterator->StringToSearchFor.c_str(), MemoryLocationIterator->StringToSearchFor.length(), &( *MemoryLocationIterator ), pThread );
					}
				}
			}
		}
		else
		{
			for ( size_t SectionCounter = 0; SectionCounter < pCurrent->GetSectionCount(); SectionCounter++ )
			{
				if ( pCurrent->GetSectionHeader()[ SectionCounter ].Characteristics & IMAGE_SCN_MEM_EXECUTE )
				{					
					CurrentAddress = ( PBYTE )( ( DWORD_PTR )lpAllocationCurrent + pCurrent->GetSectionHeader()[ SectionCounter ].VirtualAddress );

					RegionSize = pCurrent->GetSectionHeader()[ SectionCounter ].Misc.VirtualSize;

					RegionStart = ( uintptr_t )CurrentAddress;
					RegionEnd = RegionStart + RegionSize;

					Address = RegionStart;
																						
					std::vector<uintptr_t>& RelocationBlockCurrent = pCurrent->GetRelocationsVector();

					uintptr_t Temp;
						
					if ( CurrentNeutralizeFlags != PreviousNeutralizeFlags )
					{
						memcpy ( CurrentAddress, ( PBYTE )( pCurrent->GetAllocationBase() + pCurrent->GetSectionHeader()[ SectionCounter ].VirtualAddress ), pCurrent->GetSectionHeader()[ SectionCounter ].Misc.VirtualSize );
						
						if ( MemoryLocationIterator->NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
						{
							for ( RelocationsIterator = pCurrent->GetRelocationsVectorRVA().begin(); RelocationsIterator != pCurrent->GetRelocationsVectorRVA().end(); ++RelocationsIterator )
							{	
								Temp = *RelocationsIterator + ( DWORD_PTR )lpAllocationCurrent;

								*( uintptr_t* )( Temp ) = 0xCCCCCCCC;
							}
						}
					
						if ( pCurrent->IsImage64() )
						{
							CreatePattern ( CurrentAddress, pCurrent->GetSectionHeader()[ SectionCounter ].Misc.VirtualSize, MemoryLocationIterator->NeutralizeFlags, RelocationBlockCurrent, MODE_64 );
						}
						else
						{
							CreatePattern ( CurrentAddress, pCurrent->GetSectionHeader()[ SectionCounter ].Misc.VirtualSize, MemoryLocationIterator->NeutralizeFlags, RelocationBlockCurrent, MODE_32 );
						}
					}

					if ( MemoryLocationIterator->UseCondenser )
					{

					}
					else
					{
						if ( pConfig->GetReferenceBinaryVector().size() == 0 ) // no reference binaries
						{
							pFindPattern->FindPatternCode ( ( uintptr_t )lpAllocationCurrent, ( uintptr_t )CurrentAddress, RegionStart, RegionSize, 
									( PBYTE )PatternRVA, &( *MemoryLocationIterator ), pCurrent, pThread, RelocationBlockCurrent, RelocationBlockCurrent );
						}
						else
						{
							if ( MemoryLocationIterator->ReferenceBinaryIndex != 0 )
							{
								pFindPattern->FindPatternCode ( ( uintptr_t )lpAllocationCurrent, ( uintptr_t )CurrentAddress, RegionStart, RegionSize, 
									( PBYTE )PatternRVA, &( *MemoryLocationIterator ), pCurrent, pThread, RelocationBlockCurrent, RelocationBlockReference );
							}
							else
							{
								pFindPattern->FindPatternCode ( ( uintptr_t )lpAllocationCurrent, ( uintptr_t )CurrentAddress, RegionStart, RegionSize, 
									( PBYTE )PatternRVA, &( *MemoryLocationIterator ), pCurrent, pThread, RelocationBlockCurrent, RelocationBlockCurrent );
							}
						}
					}
				}
			}
		}

		PreviousNeutralizeFlags = CurrentNeutralizeFlags;
	}

	if ( pCondenser != nullptr )
	{
		delete pCondenser;
	}
}
//==================================================================================================================================
bool Match ( char* Base, char* Pattern )
{
	if ( _stricmp ( Base, Pattern ) == 0 )
	{
		return true;
	}
	
	return false;
}
//==================================================================================================================================
bool IsEqual ( DWORD_PTR dwFirst, DWORD_PTR dwSecond )
{
	return ( dwFirst < dwSecond );
}
//==================================================================================================================================
bool IsNotEqual ( DWORD_PTR dwFirst, DWORD_PTR dwSecond )
{
	return ( dwFirst != dwSecond );
}
//==================================================================================================================================
uintptr_t LocateVTableEntry ( int ReferenceBinary, size_t CurrentIndex, CFindPattern* pFindPattern, CPEManager* pReference, CPEManager* pCurrent, CThread* pThread, MemoryLocation_t* pCurrentMemoryLocation, uintptr_t VTableEntry )
{
	double BestRatio = 0.5, CurrentBestRatio = 0.5;

	uintptr_t Address, RegionEnd, RegionSize, RegionStart, 
			PatternRVA;

	LPVOID AllocationCurrent;

	size_t PatternSize, SectionIterator;

	std::vector<uintptr_t>	empty;

	std::vector<uintptr_t>& RelocationBlockCurrent = empty;
	std::vector<uintptr_t>& RelocationBlockReference = empty;

	if ( ReferenceBinary == 0 ) // no reference binaries
	{
		PatternRVA = VTableEntry;

		RelocationBlockCurrent = pCurrent->GetRelocationsVector();
		RelocationBlockReference = pCurrent->GetRelocationsVector();
	}
	else
	{
		PatternRVA = VTableEntry;
	
		RelocationBlockCurrent = pCurrent->GetRelocationsVector();
		RelocationBlockReference = pReference [ ReferenceBinary - 1 ].GetRelocationsVector();
	}

	for ( SectionIterator = 0; SectionIterator < pCurrent->GetSectionCount(); SectionIterator++ )
	{		
		if ( pCurrent->GetSectionHeader()[ SectionIterator ].Characteristics & IMAGE_SCN_MEM_EXECUTE )
		{			
			AllocationCurrent = VirtualAlloc ( NULL, pCurrent->GetSectionHeader()[ SectionIterator ].Misc.VirtualSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );

			if ( AllocationCurrent == NULL )
			{
				return 0;
			}

			RegionSize = pCurrent->GetSectionHeader()[ SectionIterator ].Misc.VirtualSize;
			
			RegionStart = ( uintptr_t )AllocationCurrent;
			RegionEnd = RegionStart + RegionSize;

			Address = RegionStart;

			memcpy ( ( PBYTE )RegionStart, ( PBYTE )( pCurrent->GetAllocationBase() + pCurrent->GetSectionHeader()[ SectionIterator ].VirtualAddress ), RegionSize );

			std::vector<uintptr_t>& RelocationBlockCurrent = pCurrent->GetRelocationsVector();

			CreatePattern ( ( PBYTE )RegionStart, RegionSize, pCurrentMemoryLocation->NeutralizeFlags, RelocationBlockCurrent, MODE_32 );

			if ( pCurrentMemoryLocation->VTableEntries [ CurrentIndex ].PatternSize == 0 )
			{
				PatternSize = pCurrentMemoryLocation->PatternSize;
			}
			else
			{
				PatternSize = pCurrentMemoryLocation->VTableEntries [ CurrentIndex ].PatternSize;
			}

			Address = pFindPattern->FindPatternVTable ( RegionStart, RegionStart, RegionSize, ( PBYTE )PatternRVA, PatternSize, pCurrentMemoryLocation, pCurrent, pThread, RelocationBlockCurrent, RelocationBlockReference );
						
			if ( Address != 0 && BestRatio >= CurrentBestRatio )
			{
				Address = Address - ( uintptr_t )AllocationCurrent;

				Address += pCurrent->GetSectionHeader()[ SectionIterator ].VirtualAddress;
			}
			
			VirtualFree ( AllocationCurrent, 0, MEM_RELEASE );
		}
	}

	return Address;
}
//==================================================================================================================================
void FindData ( CFindPattern* pFindPattern, CConfig* pConfig,
	CPEManager* pReference, CPEManager* pCurrent, CThread* pThread )
{
	using namespace std;
		
	char SectionName[9];

	int Counter = 0, CurrentLength = 0;

	LDEDisasm_t Data;

	LPVOID AllocationCurrent;

	ofstream outputfile;

	size_t Index, *pFoundInstruction = NULL;
		
	std::vector<MemoryLocation_t>::iterator MemoryLocationIterator, MemoryLocationIterator2;
	std::vector<FOUND_RVAS_t>::iterator it, it2;
	
	std::vector<ReferenceBinary_t>::iterator ReferenceBinaryIterator;
	std::vector<uintptr_t>::iterator RelocationsIterator;

	std::vector<MemoryLocation_t>& MemoryLocation = pConfig->GetConfigEntries();

	uintptr_t ImageBase, ImageEnd, CurrentPointer;
	
	if ( pCurrent->IsImage64() )
	{
		AllocationCurrent = VirtualAlloc ( NULL, pCurrent->GetNTHeaders64()->OptionalHeader.SizeOfImage, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
	}
	else
	{
		AllocationCurrent = VirtualAlloc ( NULL, pCurrent->GetNTHeaders32()->OptionalHeader.SizeOfImage, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
	}

	if ( AllocationCurrent == NULL )
	{ 
		return;
	}
		
	Search ( pFindPattern, pConfig, pReference, pCurrent, pThread, AllocationCurrent );

	outputfile.open ( pConfig->GetOutputPath() );

	if ( outputfile.is_open() )
	{
		outputfile << hex;

		for ( size_t SectionCounter = 0; SectionCounter < pCurrent->GetSectionCount(); SectionCounter++ )
		{
			memset ( SectionName, 0, sizeof ( SectionName ) );
										
			if ( pCurrent->GetSectionHeader()[ SectionCounter ].Name[0] == '.' )
			{
				for ( int i = 0; i < 8; i++ )
				{
					SectionName[i] = toupper ( pCurrent->GetSectionHeader()[ SectionCounter ].Name[i+1] );
				}
			}
			else
			{
				for ( int i = 0; i < 8; i++ )
				{
					SectionName[i] = toupper ( pCurrent->GetSectionHeader()[ SectionCounter ].Name[i] );
				}
			}

			outputfile << "#define " << pConfig->GetGlobalName() << "_SECTION_" << SectionName << "_RVA 0x" << pCurrent->GetSectionHeader()[ SectionCounter ].VirtualAddress << endl;
			outputfile << "#define " << pConfig->GetGlobalName() << "_SECTION_" << SectionName << "_SIZE 0x" << ROUND_UP ( pCurrent->GetSectionHeader()[ SectionCounter ].Misc.VirtualSize, Core.GetSizeOfPage() ) << endl << endl;
		}

		ImageBase = pCurrent->GetAllocationBase();
		ImageEnd = ImageBase + pCurrent->GetAllocationSize();

		bool Found = false;

		uintptr_t RVA1, RVA2;
		intptr_t Occurance1, Occurance2;
		intptr_t Counter, CurrentLength, Limit;
		uintptr_t VTable;

		size_t* pFoundInstruction;

		LDEDisasm_t Data;

		Limit = 0;

		std::vector<uintptr_t>& RelocationBlockTarget = pCurrent->GetRelocationsVector();

		for ( MemoryLocationIterator = MemoryLocation.begin(); MemoryLocationIterator != MemoryLocation.end(); ++MemoryLocationIterator )
		{
			for ( it = MemoryLocationIterator->m_FoundRVAS.begin(); it != MemoryLocationIterator->m_FoundRVAS.end(); ++it )
			{
				outputfile << hex;

				CurrentPointer = ImageBase;

				CurrentPointer += it->RVA;

				RVA1 = 0;

				pFoundInstruction = NULL;

				if ( MemoryLocationIterator->SearchBackward
					|| MemoryLocationIterator->SearchForward
					|| MemoryLocationIterator->SearchBetween )
				{
					if ( MemoryLocationIterator->SearchBetween )
					{

					}
					else
					{
						for ( MemoryLocationIterator2 = MemoryLocation.begin(); MemoryLocationIterator2 != MemoryLocation.end(); ++MemoryLocationIterator2 )
						{
							if ( MemoryLocationIterator->SearchEntryName1 == MemoryLocationIterator2->EntryName )
							{
								for ( it2 = MemoryLocationIterator2->m_FoundRVAS.begin(); it2 != MemoryLocationIterator2->m_FoundRVAS.end(); ++it2 )
								{
									if ( it2->RVA != 0 )
									{
										RVA1 = it2->RVA;
										Occurance1 = MemoryLocationIterator->Occurance1;

										break;
									}
								}
							}

							if ( RVA1 != 0 )
							{
								break;
							}
						}

						CurrentPointer = GetCodeFromRelocationByOccurance ( pCurrent, Occurance1, RVA1 );

						if ( CurrentPointer != 0 )
						{
							pFoundInstruction = ( size_t* )std::bsearch ( &CurrentPointer, pCurrent->GetInstructionGeneralData().data(),
								pCurrent->GetInstructionGeneralData().size(), sizeof ( uintptr_t ), compare );

							if ( pFoundInstruction != NULL )
							{
								Index = pFoundInstruction - ( size_t* )pCurrent->GetInstructionGeneralData().data();

								do
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];

									memset ( &Data, 0, sizeof ( Data ) );
																										
									if ( pCurrent->IsImage64() )
									{
										CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_64 );
									}
									else
									{
										CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_32 );
									}
													
									if ( CurrentLength != -1 )
									{
										if ( Data.Immediate.Immediate64 != 0 
											&& Data.Immediate.Immediate64 >= pCurrent->GetAllocationBase() && Data.Immediate.Immediate64 <= pCurrent->GetAllocationBase() + pCurrent->GetAllocationSize()
											&& !( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Immediate.Immediate64 ) & IMAGE_SCN_MEM_EXECUTE )
											&& !( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Immediate.Immediate64 ) & IMAGE_SCN_MEM_WRITE )
											&& ( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Immediate.Immediate64 ) & IMAGE_SCN_MEM_READ )
											&& ( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Immediate.Immediate64 ) & IMAGE_SCN_MEM_READ ) )
										{																
											if ( Match ( ( char* )Data.Immediate.Immediate64, ( char* )MemoryLocationIterator->StringToSearchFor.c_str() ) )
											{
												break;
											}
										}
										if ( Data.Displacement.Displacement32
											&& Data.Displacement.Displacement32 >= pCurrent->GetAllocationBase() && Data.Displacement.Displacement32 <= pCurrent->GetAllocationBase() + pCurrent->GetAllocationSize()
											&& !( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Displacement.Displacement32 ) & IMAGE_SCN_MEM_EXECUTE )
											&& !( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Displacement.Displacement32 ) & IMAGE_SCN_MEM_WRITE ) 
											&& ( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Displacement.Displacement32 ) & IMAGE_SCN_MEM_READ )
											&& ( pCurrent->GetContainingSectionCharacteristicsRVA ( Data.Displacement.Displacement32 ) & IMAGE_SCN_MEM_READ ) )
										{
											if ( Match ( ( char* )Data.Displacement.Displacement32, ( char* )MemoryLocationIterator->StringToSearchFor.c_str()) )
											{
												break;
											}
										}																		
									}

									if ( MemoryLocationIterator->SearchBackward != false )
									{
										Index--;
									}
									if ( MemoryLocationIterator->SearchForward != false )
									{
										Index++;
									}
																													
								}while ( CurrentLength != -1 && Index >= 0 && Index < pCurrent->GetInstructionGeneralData().size() );
							}
						}
					}
				}

				pFoundInstruction = NULL;
				
				if ( MemoryLocationIterator->GetDisplacement != false 
					|| MemoryLocationIterator->GetImmediate != false
					|| MemoryLocationIterator->GetRelative != false )
				{
					while ( pFoundInstruction == NULL )
					{
						pFoundInstruction = ( size_t* )std::bsearch ( &CurrentPointer, pCurrent->GetInstructionGeneralData().data(),
							pCurrent->GetInstructionGeneralData().size(), sizeof ( uintptr_t ), compare );

						if ( pFoundInstruction != NULL )
						{
							break;
						}
						else
						{
							CurrentPointer -= 1;
						}
					}

					Index = pFoundInstruction - ( size_t* )pCurrent->GetInstructionGeneralData().data();

					if ( MemoryLocationIterator->RetrieveDisplacement < 0 
						|| MemoryLocationIterator->RetrieveImmediate < 0
						|| MemoryLocationIterator->RetrieveRelative < 0 )
					{
						Counter = 1;

						do{
							memset ( &Data, 0, sizeof ( LDEDisasm_t ) );

							if ( pCurrent->IsImage64() )
							{
								CurrentLength = LDEDisasm ( &Data, ( PBYTE )pCurrent->GetInstructionGeneralData()[ Index ], MODE_64 );
							}
							else
							{
								CurrentLength = LDEDisasm ( &Data, ( PBYTE )pCurrent->GetInstructionGeneralData()[ Index ], MODE_32 );
							}

							if ( MemoryLocationIterator->GetDisplacement )
							{											
								if ( Data.HasDisplacement )
								{
									if ( MemoryLocationIterator->GetRange )
									{
										if ( ( int )Data.Displacement.Displacement32 >= MemoryLocationIterator->Range1 
											&& ( int )Data.Displacement.Displacement32 <= MemoryLocationIterator->Range2 )
										{
											Counter--;
										}
									}
									else
									{
										Counter--;
									}
								}
								if ( Counter == MemoryLocationIterator->RetrieveDisplacement )
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];

									Found = true;

									break;
								}
							}
							else if ( MemoryLocationIterator->GetImmediate )
							{
								if ( Data.HasImmediate != 0  )
								{
									if ( MemoryLocationIterator->GetRange )
									{
										if ( ( int )Data.Immediate.Immediate64 >= MemoryLocationIterator->Range1 
											&& ( int )Data.Immediate.Immediate64 <= MemoryLocationIterator->Range2 )
										{
											Counter--;
										}
									}
									else
									{
										Counter--;
									}
								}
								if ( Counter == MemoryLocationIterator->RetrieveImmediate )
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];

									Found = true;

									break;
								}
							}
							else if ( MemoryLocationIterator->GetRelative )
							{
								if ( Data.HasRelative != 0 )
								{
									if ( MemoryLocationIterator->GetRange )
									{
										if ( ( int )Data.Relative.Relative32 >= MemoryLocationIterator->Range1 
											&& ( int )Data.Relative.Relative32 <= MemoryLocationIterator->Range2 )
										{
											Counter--;
										}
									}
									else
									{
										Counter--;
									}
								}

								if ( Counter == MemoryLocationIterator->RetrieveRelative )
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];

									Found = true;

									break;
								}
							}

							Index--;
											
						}while ( CurrentLength != -1 && Index >= 0 && Index < pCurrent->GetInstructionGeneralData().size() );
					}
					else if ( MemoryLocationIterator->RetrieveDisplacement >= 0 
						|| MemoryLocationIterator->RetrieveImmediate >= 0
						|| MemoryLocationIterator->RetrieveRelative >= 0 )
					{
						Counter = -1;

						do{
							memset ( &Data, 0, sizeof ( LDEDisasm_t ) );

							if ( pCurrent->IsImage64() )
							{
								CurrentLength = LDEDisasm ( &Data, ( PBYTE )pCurrent->GetInstructionGeneralData()[ Index ], MODE_64 );
							}
							else
							{
								CurrentLength = LDEDisasm ( &Data, ( PBYTE )pCurrent->GetInstructionGeneralData()[ Index ], MODE_32 );
							}

							if ( MemoryLocationIterator->GetDisplacement )
							{											
								if ( Data.HasDisplacement )
								{
									if ( MemoryLocationIterator->GetRange )
									{
										if ( ( int )Data.Displacement.Displacement32 >= MemoryLocationIterator->Range1 
											&& ( int )Data.Displacement.Displacement32 <= MemoryLocationIterator->Range2 )
										{
											Counter++;
										}
									}
									else
									{
										Counter++;
									}
								}

								if ( Counter == MemoryLocationIterator->RetrieveDisplacement )
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];

									Found = true;

									break;
								}
							}
							else if ( MemoryLocationIterator->GetImmediate )
							{
								if ( Data.HasImmediate != 0  )
								{
									if ( MemoryLocationIterator->GetRange )
									{
										if ( ( int )Data.Immediate.Immediate64 >= MemoryLocationIterator->Range1 
											&& ( int )Data.Immediate.Immediate64 <= MemoryLocationIterator->Range2 )
										{
											Counter++;
										}
									}
									else
									{
										Counter++;
									}
								}

								if ( Counter == MemoryLocationIterator->RetrieveImmediate )
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];
									
									Found = true;

									break;
								}
							}
							else if ( MemoryLocationIterator->GetRelative )
							{
								if ( Data.HasRelative != 0 )
								{
									if ( MemoryLocationIterator->GetRange )
									{
										if ( ( int )Data.Relative.Relative32 >= MemoryLocationIterator->Range1 
											&& ( int )Data.Relative.Relative32 <= MemoryLocationIterator->Range2 )
										{
											Counter++;
										}
									}
									else
									{
										Counter++;
									}
								}

								if ( Counter == MemoryLocationIterator->RetrieveRelative )
								{
									CurrentPointer = pCurrent->GetInstructionGeneralData()[ Index ];

									Found = true;

									break;
								}
							}

							Index++;
											
						}while ( CurrentLength != -1 && Index >= 0 && Index < pCurrent->GetInstructionGeneralData().size() );
					}
				}

				if ( pCurrent->IsImage64() )
				{
					CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_64 );
				}
				else
				{
					CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_32 );
				}

				if ( CurrentLength != -1 )
				{
					if ( MemoryLocationIterator->GetDisplacement )
					{
						outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_Displacement 0x" << Data.Displacement.Displacement32 << endl<< endl;
					}
					if ( MemoryLocationIterator->GetImmediate )
					{
						outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_Immediate 0x" << Data.Immediate.Immediate64 << endl<< endl;
					}
					if ( MemoryLocationIterator->GetRelative )
					{
						outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_Relative 0x" << Data.Relative.Relative32 << endl<< endl;
					}

					if ( MemoryLocationIterator->RVAToDisplacement )
					{
						outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_RVADisplacement 0x" << ( Data.Displacement.Displacement32 - pCurrent->GetAllocationBase() ) << endl<< endl;
					}
					if ( MemoryLocationIterator->RVAToImmediate )
					{
						outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_RVAImmediate 0x" << ( Data.Immediate.Immediate64 - pCurrent->GetAllocationBase() ) << endl<< endl;
					}
					if ( MemoryLocationIterator->RVAToRelative )
					{
						outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_RVARelative 0x" << ( CurrentPointer + Data.Relative.Relative32 + CurrentLength ) - pCurrent->GetAllocationBase() << endl << endl;
					}
				}

				outputfile << dec;

				if ( CurrentLength > 0 && CurrentLength != -1 )
				{
					if ( MemoryLocationIterator->MarkedAsVTable )
					{
						uintptr_t TargetBinaryVTableSize = 0;
						uintptr_t ReferenceVTableSize = 0;
						uintptr_t ReferenceBinaryVTableSize = 0;
						uintptr_t* ReferenceVTable = nullptr;
						uintptr_t* TargetVTable = nullptr;
						uintptr_t ReferenceVTableVA = 0;
						uintptr_t Method = 0;
						uintptr_t VTableIndex = 0;
						size_t MethodCounter = 0;

						VTable = 0;

						switch ( MemoryLocationIterator->MarkAsVTable )
						{
							case USE_DISP:
							{
								VTable = Data.Displacement.Displacement32;
							}
							break;

							case USE_IMM:
							{
								VTable = Data.Immediate.Immediate64;
							}
							break;
						}
						
						if ( VTable != 0 &&
							std::bsearch ( &VTable, RelocationBlockTarget.data(), RelocationBlockTarget.size(), sizeof ( uintptr_t ), compare ) )
						{
							TargetVTable = ( uintptr_t* )VTable;
							
							if ( RelocationBlockTarget.size() > 0 )
							{
								while ( pCurrent->DoesResideInExecutableSection ( TargetVTable [ TargetBinaryVTableSize ] ) )
								{
									if ( std::binary_search ( RelocationBlockTarget.begin(), RelocationBlockTarget.end(), TargetVTable [ TargetBinaryVTableSize ], IsEqual ) )
									{
										TargetBinaryVTableSize++;
									}
								}
							}

							outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName << "_VTableSize " << TargetBinaryVTableSize << endl;

							if ( MemoryLocationIterator->ReferenceBinaryIndex != 0 )
							{
								std::vector<uintptr_t>& RelocationBlockReference = pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetRelocationsVector();

								ReferenceVTableVA = GetCodeFromRelocationByOccurance ( &pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ], 1, MemoryLocationIterator->RVA );

								if ( !pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].DoesResideInExecutableSection ( ReferenceVTableVA ) )
								{
									ReferenceVTableVA = pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].GetAllocationBase() + MemoryLocationIterator->RVA;
								}

								if ( pCurrent->IsImage64() )
								{
									CurrentLength = LDEDisasm ( &Data, ( PBYTE )ReferenceVTableVA, MODE_64 );
								}
								else
								{
									CurrentLength = LDEDisasm ( &Data, ( PBYTE )ReferenceVTableVA, MODE_32 );
								}

								if ( CurrentLength != -1 )
								{
									switch ( MemoryLocationIterator->MarkAsVTable )
									{
										case USE_DISP:
										{
											ReferenceVTableVA = Data.Displacement.Displacement32;
										}
										break;

										case USE_IMM:
										{
											ReferenceVTableVA = Data.Immediate.Immediate64;
										}
										break;
									}
									
									ReferenceVTable = ( uintptr_t* )ReferenceVTableVA;

									while ( pReference [ MemoryLocationIterator->ReferenceBinaryIndex - 1 ].DoesResideInExecutableSection ( ReferenceVTable [ ReferenceVTableSize ] ) )
									{
										if ( std::binary_search ( RelocationBlockReference.begin(), RelocationBlockReference.end(), ReferenceVTable [ ReferenceVTableSize ], IsEqual ) )
										{
											ReferenceVTableSize++;
										}
									}
								}

								if ( MemoryLocationIterator->VTableEntries.size() != 0 )
								{
									for ( size_t VTable = 0; VTable < MemoryLocationIterator->VTableEntries.size(); VTable++ )
									{
										Found = false;

										if ( pConfig->GetReferenceBinaryVector().size() != 0 
											&& MemoryLocationIterator->ReferenceBinaryIndex != 0 )
										{
											if ( MemoryLocationIterator->VTableEntries [ VTable ].Index < TargetBinaryVTableSize 
												&& MemoryLocationIterator->VTableEntries [ VTable ].Index < ReferenceVTableSize )
											{
												VTableIndex = MemoryLocationIterator->VTableEntries [ VTable ].Index;

												if ( ReferenceVTableSize != 0 )
												{
													Method = ReferenceVTable [ VTableIndex ];
												}
												else
												{
													Method = TargetVTable [ VTableIndex ];
												}

												Method = LocateVTableEntry ( MemoryLocationIterator->ReferenceBinaryIndex, VTable, pFindPattern, pReference, pCurrent, pThread, &(*MemoryLocationIterator), Method );
											
												Method += pCurrent->GetAllocationBase();

												for ( MethodCounter = 0; MethodCounter < TargetBinaryVTableSize; MethodCounter++ )
												{
													if ( Method == TargetVTable [ MethodCounter ] )
													{
														Found = true;

														break;
													}
												}

												if ( Found == true )
												{
													outputfile << "#define " << pConfig->GetGlobalName() << "_" << MemoryLocationIterator->EntryName <<"_" << MemoryLocationIterator->VTableEntries[ VTable ].VTableName << " " << MethodCounter << endl;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		outputfile.close();
	}

	VirtualFree ( AllocationCurrent, 0, MEM_RELEASE );
		
	return;
}
//==================================================================================================================================