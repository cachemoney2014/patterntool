//=============================================================================================================
// group 1
//=============================================================================================================
#define PREFIX_SEGMENT_CS				0x2E
#define PREFIX_SEGMENT_SS				0x36
#define PREFIX_SEGMENT_DS				0x3E
#define PREFIX_SEGMENT_ES				0x26
#define PREFIX_SEGMENT_FS				0x64
#define PREFIX_SEGMENT_GS				0x65
#define PREFIX_BRANCH_NOT_TAKEN			0x2E
#define PREFIX_BRANCH_TAKEN				0x3E
#define PREFIX_WAIT						0x9B
//=============================================================================================================
// group 2
//=============================================================================================================
#define PREFIX_LOCK						0xF0
#define PREFIX_REPNZ					0xF2
#define PREFIX_REPX						0xF3
//=============================================================================================================
// group 3
//=============================================================================================================
#define PREFIX_OPERAND_SIZE				0x66
#define PREFIX_SSE_PRECISION_OVERRIDE	0x66
//=============================================================================================================
// group 4
//=============================================================================================================
#define PREFIX_ADDRESS_SIZE				0x67
//=============================================================================================================
#define PREFIX_REX						0x40
//=============================================================================================================
#define OPCODE_TWO_BYTE					0x0F
#define PREFIX_3DNOW					0x0F
//=============================================================================================================
#define FLAG_PREFIX_OVERRIDE_SIZE		( 1 << 0 )
#define	FLAG_PREFIX_OVERRIDE_ADDRESS	( 1 << 1 )
#define	FLAG_PREFIX_LOCK				( 1 << 2 )
#define	FLAG_PREFIX_REPEAT				( 1 << 3 )
#define	FLAG_PREFIX_SEGMENT				( 1 << 4 )
#define FLAG_PREFIX_WAIT				( 1 << 5 )
#define FLAG_PREFIX_REX					( 1 << 6 )
#define FLAG_PREFIX_0F					( 1 << 7 )
#define	FLAG_OPCODE2					( 1 << 8 )
#define	FLAG_MODRM						( 1 << 9 )
#define	FLAG_SIB						( 1 << 10 )
#define	FLAG_DISP8						( 1 << 11 )
#define	FLAG_DISP16						( 1 << 12 )
#define	FLAG_DISP32						( 1 << 13 )
#define FLAG_DISP64						( 1 << 14 )
#define	FLAG_IMM8						( 1 << 15 )
#define	FLAG_IMM16						( 1 << 16 )
#define	FLAG_IMM32						( 1 << 17 )
#define	FLAG_IMM64						( 1 << 18 )
#define	FLAG_REL8						( 1 << 19 )
#define	FLAG_REL16						( 1 << 20 )
#define	FLAG_REL32						( 1 << 21 )
#define FLAG_3DNOW						( 1 << 22 )
#define FLAG_IMM8_2						( 1 << 23 )
#define FLAG_IMM16_2					( 1 << 24 )
#define FLAG_IMM32_2					( 1 << 25 )
#define FLAG_IMM64_2					( 1 << 26 )
#define FLAG_SEGMENT_REL				( 1 << 27 )
#define FLAG_OPCEXT						( 1 << 28 )
#define FLAG_SEGMENT_REGISTER			( 1 << 29 )
//=============================================================================================================
#define MAX_INSTRUCTION_SIZE			16
//=============================================================================================================
typedef struct
{
	uintptr_t CurrentPointer;

	unsigned char Prefix;
	unsigned char Prefix0F;
	unsigned char Prefix26;
	unsigned char Prefix2E;
	unsigned char Prefix36;
	unsigned char Prefix3E;
	unsigned char Prefix64;
	unsigned char Prefix65;
	unsigned char Prefix66;
	unsigned char Prefix67;
	unsigned char Prefix9B;
	unsigned char PrefixF0;
	unsigned char PrefixF2;
	unsigned char PrefixF3;
	unsigned char PrefixREX;
	unsigned char REXPrefix;

	unsigned char PrefixLock;
	unsigned char PrefixRepeat;
	unsigned char PrefixSegment;
	unsigned char PrefixWait;
	unsigned char PrefixOperandSizeOverride;
	unsigned char PrefixAddressSizeOverride;
	unsigned char PrefixBranchTaken;
	unsigned char PrefixBranchNotTaken;
	unsigned char Prefix3DNow;

	unsigned char Opcode;
	unsigned char Opcode2;
	unsigned char ModRM;
	unsigned char SIB;

	unsigned char Register;

	unsigned char ModRM_Mod;
	unsigned char ModRM_Reg;
	unsigned char ModRM_RM;

	unsigned char RMRegister;

	unsigned char SIB_Scale;
	unsigned char SIB_Index;
	unsigned char SIB_Base;

	unsigned char BaseRegister;

	unsigned char Scalar;

	unsigned char HasDisplacement;
	unsigned char HasImmediate;
	unsigned char HasRelative;

	unsigned char UseRegisterToRegister;
	unsigned char UsingSIB;

	unsigned char UseDisplacementOnly;
	unsigned char DisplacementSize;

	unsigned short Segment;
	
	unsigned char IsPrefixAddressSizeOverrideValid;
	unsigned char IsPrefixOperandSizeOverideValid;
	unsigned char IsPrefixRepeatValid;
	unsigned char IsLockPrefixValid;
	unsigned char HasDirectionFlag;

	unsigned char OpcodeFlags;

	union
	{
		unsigned char Displacement8;
		unsigned short Displacement16;
		unsigned long Displacement32;
		unsigned long Displacement64;
	}Displacement;

	union
	{
		unsigned char Immediate8;
		unsigned short Immediate16;
		unsigned long Immediate32;
		unsigned long long Immediate64;
	}Immediate;

	union
	{
		unsigned char Immediate8;
		unsigned short Immediate16;
		unsigned long Immediate32;
		unsigned long long Immediate64;
	}Immediate2;

	union
	{
		signed char Relative8;
		signed short Relative16;
		signed long Relative32;
	}Relative;
		
	unsigned short REX_W; // Operand size. 0: Default, 1: 64 bit
	unsigned short REX_R; // ModRM.reg extension
	unsigned short REX_X; // SIB.index extension
	unsigned short REX_B; // ModRM.rm extension

	int Flags;
	int Length;
	
}LDEDisasm_t;
//=============================================================================================================
#define MODE_32	1
#define MODE_64	2
//=============================================================================================================
#define REG_ESP	4
#define REG_EBP	5
#define ALL_GENERAL_PURPOSE 127
#define REG_INVALID	255
//=============================================================================================================
#include "sal.h"
//=============================================================================================================
int LDEDisasm ( _Inout_ LDEDisasm_t* pDisasm, _In_ unsigned char* Address, _In_ int Mode );
//=============================================================================================================
void LDEDisasm32 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer, _In_ unsigned char CurrentValue );
void LDEDisasm64 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer, _In_ unsigned char CurrentValue );
void LDEModRMAndSIB3264 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer );
void LDEDecodeSIB3264 ( _Inout_ LDEDisasm_t* pDisasm );
void LDEDecodeModRM3264 ( _Inout_ LDEDisasm_t* pDisasm );
//=============================================================================================================