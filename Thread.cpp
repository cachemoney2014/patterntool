//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Thread.h"
//=============================================================================================================
CThread::CThread()
{
	SYSTEM_INFO SYSINFO;

	NumberOfProcessors	= 0;

	EventHandles = NULL;

	ThreadHandles = NULL;

	ThreadIDs = NULL;

	memset ( &SYSINFO, 0, sizeof ( SYSTEM_INFO ) );

	GetSystemInfo ( &SYSINFO );

	NumberOfProcessors = SYSINFO.dwNumberOfProcessors;

	NumberOfProcessors = 1;
		
	EventHandles = new HANDLE [ NumberOfProcessors ];

	ThreadHandles = new HANDLE [ NumberOfProcessors ];

	ThreadIDs = new DWORD [ NumberOfProcessors ];

	for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
	{
		if ( EventHandles != nullptr )
		{
			EventHandles [ Iterator ] = INVALID_HANDLE_VALUE;
		}
		if ( ThreadHandles != nullptr )
		{
			ThreadHandles [ Iterator ] = INVALID_HANDLE_VALUE;
		}
	}

	IsWriteable = 0;
}
//=============================================================================================================
CThread::~CThread()
{
	if ( EventHandles != nullptr
		&& ThreadHandles != nullptr )
	{
		for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
		{
			if ( EventHandles [ Iterator ] != INVALID_HANDLE_VALUE )
			{
				CloseHandle ( EventHandles [ Iterator ] );
			}
			if ( ThreadHandles [ Iterator ] != INVALID_HANDLE_VALUE )
			{
				CloseHandle ( ThreadHandles [ Iterator ] );
			}
		}
	}

	if ( EventHandles != nullptr )
	{
		delete [] EventHandles;
	}
	if ( ThreadHandles != nullptr )
	{
		delete [] ThreadHandles;
	}
	if ( ThreadIDs != nullptr )
	{
		delete [] ThreadIDs;
	}
}
//=============================================================================================================
size_t CThread::GetNumberOfProcessors()
{
	return NumberOfProcessors;
}
//=============================================================================================================
size_t CThread::GetThreadIndexByThreadID ( DWORD ThreadID )
{
	for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
	{
		if ( ThreadIDs [ Iterator ] == ThreadID )
		{
			return Iterator;
		}
	}

	return 0;
}
//=============================================================================================================
bool CThread::IsExitSignaled ( void )
{
	return ( ExitSignal == 1 );
}
//=============================================================================================================
bool CThread::CanWrite ( void )
{
	return ( IsWriteable == 0 );
}
//=============================================================================================================
bool CThread::AcquireWrite ( void )
{
	return ( InterlockedExchange ( &IsWriteable, 0 ) == 1 );
}
//=============================================================================================================
bool CThread::UnAcquireWrite ( void )
{
	return ( InterlockedExchange ( &IsWriteable, 1 ) == 0 );
}
//=============================================================================================================
LONG CThread::SignalExit ( void )
{
	return InterlockedExchange ( &ExitSignal, 1 );
}
//=============================================================================================================
HANDLE CThread::GetEventHandle ( size_t uiIndex )
{
	return EventHandles [ uiIndex ];
}
//=============================================================================================================
bool CThread::CreateRunningGroup ( LPVOID lpProcedure, LPVOID lpArgs )
{
	ExitSignal = 0;

	if ( CreatedThreadGroup == true || ThreadHandles == NULL || EventHandles == NULL )
	{
		return false;
	}

	for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
	{
		ThreadHandles [ Iterator ] = CreateThread ( NULL, NULL, ( LPTHREAD_START_ROUTINE  )lpProcedure, 
			lpArgs, CREATE_SUSPENDED, &ThreadIDs [ Iterator ] );

		if ( ThreadHandles [ Iterator ] == INVALID_HANDLE_VALUE )
		{
			CreatedThreadGroup = false;

			return false;
		}

		EventHandles [ Iterator ] = CreateEvent ( NULL, TRUE, FALSE, "" );

		if ( EventHandles [ Iterator ] == INVALID_HANDLE_VALUE )
		{
			CreatedThreadGroup = false;

			return false;
		}
	}

	CreatedThreadGroup = true;

	return true;
}
//=============================================================================================================
bool CThread::IsRunningGroupFinished ( void )
{
	size_t ThreadsReady = 0;

	if ( CreatedThreadGroup == true )
	{
		for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
		{
			if ( WaitForSingleObject ( EventHandles [ Iterator ], 0 ) == WAIT_OBJECT_0 )
			{
				ThreadsReady++;
			}
		}
	}

	return ( ThreadsReady == NumberOfProcessors );
}
//=============================================================================================================
void CThread::StartRunningGroup ( void )
{
	if ( CreatedThreadGroup == true )
	{
		for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
		{
			ResumeThread ( ThreadHandles [ Iterator ] );
		}
	}
}
//=============================================================================================================
void CThread::EndRunningGroup ( void )
{
	if ( CreatedThreadGroup == true )
	{
		for ( size_t Iterator = 0; Iterator < NumberOfProcessors; Iterator++ )
		{
			CloseHandle ( ThreadHandles [ Iterator ] );

			ThreadHandles [ Iterator ] = INVALID_HANDLE_VALUE;

			CloseHandle ( EventHandles [ Iterator ] );

			EventHandles [ Iterator ] = INVALID_HANDLE_VALUE;
		}

		CreatedThreadGroup = false;
	}
}
//=============================================================================================================
void CThread::SignalEvent ( HANDLE hEventToSignal )
{
	SetEvent ( hEventToSignal );
}
//=============================================================================================================
void CThread::UnSignalEvent ( HANDLE hEventToReset )
{
	ResetEvent ( hEventToReset );
}
//=============================================================================================================