//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "SHA.h"
 //==================================================================================================================================
#include <openssl/sha.h>
//==================================================================================================================================
bool SHAHashFile256 ( const char* Path, char* Hash )
{
	DWORD FileSize = 0;
	DWORD BytesRead = 0;
	DWORD BytesToRead = 0;
	DWORD BytesRemaining = 0;
	DWORD ReadBufferSize = 262144;
		
	HANDLE File;

	SHA256_CTX SHA256Context;

	void* ReadBuffer;

	unsigned char SHA256Digest [ SHA256_DIGEST_LENGTH ];
	
    if ( !SHA256_Init ( &SHA256Context ) )
	{
		std::cout << "Failed to initalize SHA256" << std::endl;

        return false;
	}

	File = CreateFileA ( Path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL );

	if ( File != INVALID_HANDLE_VALUE )
	{
		FileSize = GetFileSize ( File, NULL );

		// Calculate checksum from the bytes.
		BytesRemaining = FileSize;

		ReadBuffer = malloc ( ReadBufferSize );

		if ( ReadBuffer )
		{
			BOOL ReadError = FALSE;

			while ( BytesRemaining > 0 )
			{
				BytesToRead = ( ( BytesRemaining > ReadBufferSize ) ? ReadBufferSize : BytesRemaining );
				
				if ( !ReadFile ( File, ReadBuffer, ReadBufferSize, &BytesRead, 0 ) || ( BytesToRead != BytesRead ) )
				{
					ReadError = TRUE;

					break;
				}

				if ( !SHA256_Update ( &SHA256Context, ( unsigned char* )ReadBuffer, BytesRead ) )
				{
					std::cout << "SHA256_Update failed" << std::endl;

					return false;
				}

				BytesRemaining -= BytesRead;
			}

			if ( !ReadError )
			{
				if ( !SHA256_Final ( SHA256Digest, &SHA256Context ) )
				{
					std::cout << "SHA256_Final failed" << std::endl;

					return false;
				}
			}

			free ( ReadBuffer );
		}
	}

	for ( size_t i = 0; i < SHA256_DIGEST_LENGTH; i++ )
	{
		sprintf_s ( Hash+i*2, sizeof ( SHA256HASHSIZE ), "%02x", SHA256Digest[i] );
	}

	return true;
}
//==================================================================================================================================