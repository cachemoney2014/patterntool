//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "LDE.h"
#include "Common.h"
//=============================================================================================================
uintptr_t GetBranchAddress ( LDEDisasm_t* pDisasm )
{
	if ( pDisasm->Prefix0F == 0 )
	{
		switch ( pDisasm->Opcode )
		{
			case INSTR_RELJMP:
			case INSTR_RELCALL:
			{
				return pDisasm->CurrentPointer + pDisasm->Relative.Relative32 + pDisasm->Length;
			}
			break;

			case INSTR_SHORTJMP:
			case INSTR_RELJCX:
			{
				return pDisasm->CurrentPointer + pDisasm->Relative.Relative8 + pDisasm->Length;
			}
			break;

			default:
			{
				if ( pDisasm->Opcode >= INSTR_SHORTJCC_BEGIN && pDisasm->Opcode <= INSTR_SHORTJCC_END ||
					pDisasm->Opcode >= INSTR_LOOPNZ && pDisasm->Opcode <= INSTR_RELJCX )
				{
					return pDisasm->CurrentPointer + pDisasm->Relative.Relative8 + pDisasm->Length;
				}
			}
			break;
		}
	}
	else
	{
		if ( pDisasm->Opcode >= INSTR_JCC_BEGIN && pDisasm->Opcode <= INSTR_JCC_END )
		{
			return pDisasm->CurrentPointer + pDisasm->Relative.Relative32 + pDisasm->Length;
		}
	}
	
	return 0;
}
//=============================================================================================================
BOOL IsConditionalBranch ( LDEDisasm_t* pDisasm )
{
	if ( pDisasm->Prefix0F == 0 )
	{
		if ( pDisasm->Opcode >= INSTR_SHORTJCC_BEGIN && pDisasm->Opcode <= INSTR_SHORTJCC_END
			|| pDisasm->Opcode >= INSTR_LOOPNZ && pDisasm->Opcode <= INSTR_RELJCX )
		{
			return TRUE;
		}
	}
	else
	{
		if ( pDisasm->Opcode >= INSTR_JCC_BEGIN && pDisasm->Opcode <= INSTR_JCC_END )
		{
			return TRUE;
		}
	}

	return FALSE;
}
//=============================================================================================================
BOOL IsUnConditionalBranch ( LDEDisasm_t* pDisasm )
{
	switch ( pDisasm->Opcode )
	{
		case INSTR_SHORTJMP:
		{
			return TRUE;
		}
		break;

		case INSTR_RELJMP:
		{
			return TRUE;
		}
		break;
	}

	return FALSE;
}
//=============================================================================================================
BOOL IsBranchOrReturn ( LDEDisasm_t* pDisasm )
{
	if ( pDisasm->Prefix0F == 0 )
	{		
		if ( pDisasm->Opcode >= INSTR_SHORTJCC_BEGIN && pDisasm->Opcode <= INSTR_SHORTJCC_END
			|| pDisasm->Opcode >= INSTR_LOOPNZ && pDisasm->Opcode <= INSTR_RELJCX )
		{
			return TRUE;
		}
		else if ( pDisasm->Opcode == INSTR_RET 
			|| pDisasm->Opcode == INSTR_RETN  
			|| pDisasm->Opcode == INSTR_RETFN
			|| pDisasm->Opcode == INSTR_RETF )
		{
			return TRUE;
		}
		else if ( pDisasm->Opcode == INSTR_RELJMP 
			|| pDisasm->Opcode == INSTR_SHORTJMP 
			|| pDisasm->Opcode == INSTR_FARJMP )
		{
			return TRUE;
		}
		else if ( pDisasm->Opcode == INSTR_FF )
		{
			if ( pDisasm->ModRM_Reg == 4
				|| pDisasm->ModRM_Reg == 5 )
			{
				return TRUE;
			}
		}
		else if ( pDisasm->Opcode == INSTR_RELCALL )
		{
			return TRUE;
		}
	}
	else
	{
		if ( pDisasm->Opcode >= INSTR_JCC_BEGIN && pDisasm->Opcode <= INSTR_JCC_END )
		{
			return TRUE;
		}
	}

	return FALSE;
}
//=============================================================================================================