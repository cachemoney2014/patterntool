//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "Core.h"
#include "SHA.h"
//==================================================================================================================================
CCore Core;
//==================================================================================================================================
CCore::CCore()
{
	char Directory [ MAX_PATH ];

	SYSTEM_INFO SYSINFO;

	if ( GetCurrentDirectoryA ( MAX_PATH, Directory ) )
	{
		CurrentDirectory = Directory;
	}

	memset ( &SYSINFO, 0, sizeof ( SYSTEM_INFO ) );

	GetSystemInfo ( &SYSINFO );

	SizeOfPage = SYSINFO.dwPageSize;
}
//==================================================================================================================================
CCore::~CCore()
{

}
//==================================================================================================================================
int CCore::GetUpdatedIndex()
{
	int UpdateIndex = 0;

	for ( size_t Iterator = 0; Iterator < TrackedFiles.size(); Iterator++ )
	{						
		if ( TrackedFiles[ Iterator ].NeedsUpdate == true )
		{
			UpdateIndex = ( 1 << Iterator );
		}
	}

	return UpdateIndex;
}
//==================================================================================================================================
void CCore::DumpUpdatedTrackedFiles()
{
	using namespace std;
	
	BOOL Return;

	char* BaseName = NULL, TrackedName[ MAX_PATH ];

	DWORD BytesRead, BytesWritten;

	HANDLE FileToDump, FileToCreate;
		
	LPVOID RawAllocation;

	LARGE_INTEGER FileSize;

	size_t SavedPosition = 0;

	string DirectoryToCreate, FileToOpen, FileNameToSave;

	SYSTEMTIME st;

	GetSystemTime ( &st );

	for ( size_t Iterator = 0; Iterator < TrackedFiles.size(); Iterator++ )
	{
		if ( TrackedFiles[ Iterator ].NeedsUpdate )
		{
			for ( size_t StringIterator = 0; StringIterator < TrackedFiles[ Iterator ].Path.size(); StringIterator++ )
			{
				if ( TrackedFiles[ Iterator ].Path[ StringIterator ] == '\\' || TrackedFiles[ Iterator ].Path[ StringIterator ] == '/' )
				{
					BaseName = &TrackedFiles[ Iterator ].Path[ StringIterator + 1 ];
				}
			}

			if ( BaseName != NULL )
			{
				FileToOpen = SteamPath;

				FileToOpen += TrackedFiles[ Iterator ].Path;

				FileToDump = CreateFileA ( FileToOpen.c_str(), GENERIC_READ, FILE_SHARE_READ, 
					NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );

				DirectoryToCreate = CurrentDirectory;

				DirectoryToCreate += "\\";

				DirectoryToCreate += TrackedFiles[ Iterator ].TrackedPrefix;

				sprintf_s ( TrackedName, MAX_PATH, "%i-%i-%i_%s", st.wYear, st.wMonth, st.wDay, BaseName );

				FileNameToSave = DirectoryToCreate;

				FileNameToSave += "\\";

				FileNameToSave += TrackedName;

				Return = CreateDirectoryA ( DirectoryToCreate.c_str(), NULL );

				if ( Return != 0 || ( Return == 0 && GetLastError() == ERROR_ALREADY_EXISTS ) )
				{
					if ( FileToDump != INVALID_HANDLE_VALUE )
					{
						if ( GetFileSizeEx ( FileToDump, &FileSize ) )
						{
							RawAllocation = VirtualAlloc ( NULL, FileSize.LowPart, MEM_COMMIT, PAGE_READWRITE );

							if ( RawAllocation != NULL )
							{
								if ( ReadFile ( FileToDump, RawAllocation, FileSize.LowPart, &BytesRead, NULL ) )
								{
									FileToCreate = CreateFileA ( FileNameToSave.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, 
										NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );

									if ( FileToCreate != INVALID_HANDLE_VALUE )
									{
										WriteFile ( FileToCreate, RawAllocation, FileSize.LowPart, &BytesWritten, NULL );
										
										CloseHandle ( FileToCreate );
									}
								}

								VirtualFree ( RawAllocation, 0, MEM_RELEASE );
							}
						}

						CloseHandle ( FileToDump );
					}
				}
			}
		}
	}
}
//==================================================================================================================================
void CCore::UpdateTrackedFilesInformation()
{
	using namespace std;

	ofstream TrackedFileInformationStream;

	string TrackedFileInformation;

	TrackedFileInformation = CurrentDirectory;

	TrackedFileInformation += "\\";

	TrackedFileInformation += "TrackedFileInformation.ini";

	TrackedFileInformationStream.open ( TrackedFileInformation, ios::trunc );

	for ( size_t Iterator = 0; Iterator < TrackedFiles.size(); Iterator++ )
	{
		TrackedFileInformationStream << TrackedFiles [ Iterator ].Path << " 0x" << TrackedFiles [ Iterator ].SHAHash << endl;
	}

	if ( TrackedFileInformationStream.is_open() == true )
	{
		TrackedFileInformationStream.close();
	}
}
//==================================================================================================================================
bool CCore::CheckTrackedFilesForUpdates()
{
	using namespace std;

	char SHA256Hash [ SHA256HASHSIZE * 2 ];

	ifstream TrackedFileInformationStream;

	size_t LineCount = 0;

	streampos EmptyFileSize = 0;

	string CurrentFilePath, FileLine, FilePath, TrackedFileInformation;
		
	TrackedFileInformation = CurrentDirectory;

	TrackedFileInformation += "\\";

	TrackedFileInformation += "TrackedFileInformation.ini";

	TrackedFileInformationStream.open ( TrackedFileInformation, ios::binary | ios::ate );

	if ( TrackedFileInformationStream.is_open() == false )
	{
		cout << "Did not find TrackedFileInformation.ini in the directory." << endl;
	}
	else
	{
		TrackedFileInformationStream.open ( TrackedFileInformation );
	}
	if ( TrackedFileInformationStream.is_open() )
	{
		TrackedFileInformationStream.seekg ( 0, ios::beg );
						
		while ( getline ( TrackedFileInformationStream, FileLine ) )
		{				
			CurrentFilePath = FileLine.substr ( 0, FileLine.find ( " 0x" ) );

			for ( size_t Iterator = 0; Iterator < TrackedFiles.size(); Iterator++ )
			{						
				if ( TrackedFiles[ Iterator ].Path == CurrentFilePath )
				{
					TrackedFiles[ Iterator ].SHAHash = FileLine.substr ( FileLine.find ( "0x" ) + 2, FileLine.find ( '\r' ) );

					TrackedFiles[ Iterator ].SHAHash.erase ( TrackedFiles[ Iterator ].SHAHash.size() - 1 );
					
					FilePath = SteamPath;

					FilePath += TrackedFiles[ Iterator ].Path;

					TrackedFiles[ Iterator ].Found = true;
						
					if ( SHAHashFile256 ( FilePath.c_str(), SHA256Hash ) )
					{			
						if ( TrackedFiles[ Iterator ].SHAHash != SHA256Hash )
						{
							TrackedFiles[ Iterator ].NeedsUpdate = true;

							TrackedFiles[ Iterator ].SHAHash = SHA256Hash; 
						}
					}
					else
					{
						cout << "SHA hash failed." << endl;

						if ( TrackedFileInformationStream.is_open() )
						{
							TrackedFileInformationStream.close();
						}

						return false;
					}
				}
			}
		}

		for ( size_t Iterator = 0; Iterator < TrackedFiles.size(); Iterator++ )
		{
			if ( TrackedFiles[ Iterator ].Found != true )
			{
				FilePath = SteamPath;

				FilePath += TrackedFiles[ Iterator ].Path;

				if ( SHAHashFile256 ( FilePath.c_str(), SHA256Hash ) )
				{			
					if ( TrackedFiles[ Iterator ].SHAHash != SHA256Hash )
					{
						TrackedFiles[ Iterator ].NeedsUpdate = true;

						TrackedFiles[ Iterator ].SHAHash = SHA256Hash; 
					}
				}
				else
				{
					cout << "SHA hash failed." << endl;

					if ( TrackedFileInformationStream.is_open() )
					{
						TrackedFileInformationStream.close();
					}

					return false;
				}
			}
		}
	}
	else
	{
		for ( size_t Iterator = 0; Iterator < TrackedFiles.size(); Iterator++ )
		{
			FilePath = SteamPath;

			FilePath += TrackedFiles[ Iterator ].Path;
						
			if ( SHAHashFile256 ( FilePath.c_str(), SHA256Hash ) )
			{			
				if ( TrackedFiles[ Iterator ].SHAHash != SHA256Hash )
				{
					TrackedFiles[ Iterator ].NeedsUpdate = true;

					TrackedFiles[ Iterator ].SHAHash = SHA256Hash; 
				}
			}
			else
			{
				cout << "SHA hash failed." << endl;

				if ( TrackedFileInformationStream.is_open() )
				{
					TrackedFileInformationStream.close();
				}

				return false;
			}
		}
	}
	

	if ( TrackedFileInformationStream.is_open() )
	{
		TrackedFileInformationStream.close();
	}

	return true;
}
//==================================================================================================================================
bool CCore::GetTrackedFiles()
{	
	using namespace std;

	bool Return = false;

	ifstream TrackedFileStream;

	size_t LineCounter = 1;

	string FileLine, TrackedFileName, TrackedFilePath, TrackedPrefix;

	streampos EmptyFileSize = 0;

	TrackedFilesInformation_t TrackedFilesInformation;

	TrackedFileName += CurrentDirectory;

	TrackedFileName += "\\";

	TrackedFileName += "TrackedFiles.ini";

	TrackedFileStream.open ( TrackedFileName, ios::binary | ios::ate );

	if ( TrackedFileStream.is_open() == false )
	{
		cout << "Did not find TrackedFiles.ini in the directory." << endl;
	
		return Return;
	}
	else
	{
		if ( TrackedFileStream.tellg() == EmptyFileSize )
		{
			cout << "TrackedFiles.ini is empty." << endl;
		}
		else
		{
			TrackedFileStream.seekg ( 0, ios::beg );

			while ( getline ( TrackedFileStream, FileLine ) )
			{
				if ( !FileLine.find ( " tracked_" ) )
				{
					cout << "Missing tracked_ prefix on line " << LineCounter << endl;

					Return = false;

					break;
				}
				else
				{
					TrackedFilePath = FileLine.substr ( 0, FileLine.find ( " tracked_" ) );

					TrackedPrefix = FileLine.substr ( FileLine.find ( "tracked_" ), FileLine.find ( '\r' ) );

					TrackedPrefix.erase ( TrackedPrefix.size() - 1 );

					TrackedFilesInformation.Path = TrackedFilePath;

					TrackedFilesInformation.TrackedPrefix = TrackedPrefix;

					TrackedFiles.push_back ( TrackedFilesInformation );

					Return = true;
				}

				LineCounter++;
			}
		}
	}

	if ( TrackedFileStream.is_open() )
	{
		TrackedFileStream.close();
	}

	return Return;
}
//==================================================================================================================================
bool CCore::GetSteamPath()
{
	bool Return = false;

	HKEY ValveKey;

	LSTATUS SteamRegistryKey;

	uintptr_t SizeOfBuffer = 1024;

	char* SteamBuffer = new char [ SizeOfBuffer ];

	if ( SteamBuffer != nullptr )
	{
		SteamRegistryKey = RegOpenKeyExA ( HKEY_CURRENT_USER,
			TEXT("SOFTWARE\\Valve"),
			0,
			KEY_READ,
			&ValveKey );

		if ( SteamRegistryKey == ERROR_SUCCESS )
		{
			SteamRegistryKey = RegGetValueA ( ValveKey, "Steam", "SteamPath", RRF_RT_REG_SZ, NULL, SteamBuffer, ( LPDWORD )&SizeOfBuffer );

			if ( SteamRegistryKey == ERROR_SUCCESS )
			{
				SteamRegistryKey = RegCloseKey ( ValveKey );

				if ( SteamRegistryKey == ERROR_SUCCESS )
				{
					SteamPath = SteamBuffer;

					Return = true;
				}
			}
		}

		delete SteamBuffer;
	}

	return Return;
}
//==================================================================================================================================
uintptr_t CCore::GetSizeOfPage ( void )
{
	return SizeOfPage;
}
//==================================================================================================================================