//==================================================================================================================================
class CFindPattern
{
public:

	CFindPattern() {};
	~CFindPattern() {};

	void FindPatternCode ( uintptr_t AllocationBase, uintptr_t Current, uintptr_t Base, uintptr_t Size, 
		PBYTE Pattern, MemoryLocation_t* pCurrentMemoryLocation, CPEManager* pCurrent, CThread* pThread, std::vector<uintptr_t>& RelocationBlockCurrent, std::vector<uintptr_t>& RelocationBlockReference );

	void FindPatternData ( CPEManager* pCurrent, uintptr_t AllocationBase, uintptr_t Current, uintptr_t Base, uintptr_t Size, 
		PBYTE Pattern, size_t SizeOfPattern, MemoryLocation_t* pCurrentMemoryLocation, CThread* pThread );

	uintptr_t CFindPattern::FindPatternVTable ( uintptr_t Current, uintptr_t Base, uintptr_t Size, 
		PBYTE Pattern, size_t SizeOfPattern, MemoryLocation_t* pCurrentMemoryLocation, CPEManager* pCurrent, CThread* pThread, std::vector<uintptr_t>& RelocationBlockCurrent, std::vector<uintptr_t>& RelocationBlockReference );

private:

};
//==================================================================================================================================
typedef struct
{	
	uintptr_t BlockBegin;
	uintptr_t BlockEnd;

	double BestOutRatio;
		
	uintptr_t BestAddress;
	
	std::vector<FOUND_RVAS_t> FoundRVAS;
}FindPattern_Data_t;
//==================================================================================================================================
typedef struct
{
	CPEManager* Current;
	CThread* Thread;

	double BestOverallRatio;

	uintptr_t AllocationBase;

	uintptr_t RegionBase;
	uintptr_t RegionSize;

	FindPattern_Data_t* FindPatternData;
		
	PBYTE Pattern;

	size_t SizeOfPattern;

	MemoryLocation_t* CurrentMemoryLocation;
}Thread_Data_t;
//==================================================================================================================================
extern void AddRVAEntry ( MemoryLocation_t* pCurrentMemoryLocation );
extern void AddRVAEntry ( Thread_Data_t* pThreadData, uintptr_t BestAddress, double BestRatio, size_t ThreadIndex );
//==================================================================================================================================
extern double BMGetRatioMatchData ( uintptr_t Base, uintptr_t BlockBegin, uintptr_t BlockSize, PBYTE Pattern, size_t SizeOfPattern, char* BadCharSkip, char* GoodSuffixes, int& Shift );
extern double GetRatioMatch ( uintptr_t Base, PBYTE Pattern, size_t SizeOfPattern );
extern double GetRatioMatch ( uintptr_t Base, uintptr_t BlockBegin, uintptr_t BlockSize, PBYTE Pattern, size_t SizeOfPattern );
//==================================================================================================================================
#define XSIZE   30
#define ASIZE   256
//==================================================================================================================================
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
//==================================================================================================================================