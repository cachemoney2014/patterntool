//=============================================================================================================
#pragma once
//=============================================================================================================
class CThread
{
public:

		CThread();
		~CThread();
								
		bool CreateRunningGroup ( LPVOID Procedure, LPVOID Args );

		bool CanWrite ( void );

		bool IsExitSignaled ( void );

		bool IsRunningGroupFinished ( void );

		HANDLE GetEventHandle ( size_t Index );

		LONG SignalExit ( void );

		size_t GetNumberOfProcessors();

		size_t GetThreadIndexByThreadID ( DWORD ThreadID );

		bool AcquireWrite ( void );

		bool UnAcquireWrite ( void );

		void EndRunningGroup();

		void StartRunningGroup();

		void SignalEvent ( HANDLE EventToSignal );

		void UnSignalEvent ( HANDLE EventToReset );

private:
		
		bool CreatedThreadGroup;

		volatile LONG ExitSignal;

		volatile LONG IsWriteable;

		DWORD* ThreadIDs;

		HANDLE* EventHandles, *ThreadHandles;

		size_t NumberOfProcessors;

//		CPEFile* m_pCurrent;
//		CPEFile* m_pReferences;
};
//=============================================================================================================