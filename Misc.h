//=============================================================================================================
int __cdecl compare ( const void *ap, const void *bp );
//=============================================================================================================
void CreatePattern ( PBYTE Signature, size_t SizeOfSignature, size_t NeutralizeFlags, std::vector<uintptr_t>& RelocationBlock, int Mode );
size_t GetAdjustedSignatureSize ( PBYTE Signature, size_t SizeOfSignature, int Mode );
//=============================================================================================================