//==================================================================================================================================
typedef struct
{
	bool Found;
	bool NeedsUpdate;

	std::string Path;
	std::string TrackedPrefix;
	std::string SHAHash;
}TrackedFilesInformation_t;
//==================================================================================================================================
class CCore
{
public:

	CCore();
	~CCore();
		
	bool CheckTrackedFilesForUpdates();
	void UpdateTrackedFilesInformation();
	
	bool GetSteamPath();

	bool GetTrackedFiles();

	const char* ReturnSteamPath()
	{
		return SteamPath.c_str();
	}

	int GetUpdatedIndex();

	std::vector<TrackedFilesInformation_t>& GetTrackedFilesInformation()
	{
		return TrackedFiles;
	}

	uintptr_t GetSizeOfPage ( void );

	void DumpUpdatedTrackedFiles();

private:

	std::string CurrentDirectory;
	std::string SteamPath;

	std::vector<TrackedFilesInformation_t> TrackedFiles;

	uintptr_t SizeOfPage;
};
//==================================================================================================================================
extern CCore Core;
//==================================================================================================================================