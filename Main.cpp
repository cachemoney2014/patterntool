//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "Config.h"
#include "Core.h"
#include "PEManager.h"
#include "Thread.h"
#include "FindPattern.h"
//==================================================================================================================================
#include "Scanner.h"
//==================================================================================================================================
int main ( int argc, char* argv[] )
{
	using namespace std;

	bool Dump = false, Success = true;

	CConfig* PatternToolConfig = nullptr;
	CFindPattern* pFindPattern = nullptr;
	CPEManager* pCurrent = nullptr, *pReference = nullptr;
	CThread* pThread = nullptr;

	std::vector<ReferenceBinary_t>::iterator ReferenceBinaryIterator;

	size_t ReferenceBinaryCounter = 0;

	if ( argc == 2 )
	{
		if ( !_stricmp ( argv[1], "dump" ) )
		{
			Dump = true;
		}
	}
	
	if ( Core.GetSteamPath() && Core.GetTrackedFiles() )
	{
		if ( strlen ( argv[1] ) == 0 )
		{
			if ( Core.CheckTrackedFilesForUpdates() )
			{
				Core.UpdateTrackedFilesInformation();
			     
				if ( Dump )
				{
					Core.DumpUpdatedTrackedFiles();
				}

				return Core.GetUpdatedIndex();
			}
		}
		else
		{		
			PatternToolConfig = new CConfig ( argv[1] );

			if ( PatternToolConfig && PatternToolConfig->ConfigNotFound() != true )
			{
				if ( PatternToolConfig->ExecuteConfig() )
				{
					pCurrent = new CPEManager;

					pFindPattern = new CFindPattern;

					pReference = new CPEManager [ PatternToolConfig->GetReferenceBinaryVector().size() ];

					pThread = new CThread;

					std::vector<ReferenceBinary_t>& ReferenceBinary = PatternToolConfig->GetReferenceBinaryVector();

					if ( pCurrent == nullptr || pReference == nullptr || pThread == nullptr || pFindPattern == nullptr )
					{
						Success = false;
					}

					for ( ReferenceBinaryIterator = ReferenceBinary.begin(); ReferenceBinaryIterator != ReferenceBinary.end(); ++ReferenceBinaryIterator  )
					{
						if ( pReference != nullptr && pReference [ ReferenceBinaryCounter ].OpenFile ( ReferenceBinaryIterator->ReferenceBinaryPath.c_str() ) )
						{
							if ( !pReference [ ReferenceBinaryCounter ].LoadFile() || !pReference [ ReferenceBinaryCounter ].MapAsNormal() )
							{
								Success = false;

								break;
							}
						}
					
						ReferenceBinaryCounter++;
					}

					if ( Success == true )
					{
						if ( pCurrent->OpenFile ( PatternToolConfig->ReturnBinaryToLoad() ) )
						{
							if ( pCurrent->LoadFile() && pCurrent->MapAsNormal() )
							{
								FindData ( pFindPattern, PatternToolConfig, pReference, pCurrent, pThread );
							}
						}
					}
				}
			}
			if ( pCurrent != nullptr )
			{
				delete pCurrent;
			}
			if ( pReference != nullptr )
			{
				delete [] pReference;
			}
			if ( pThread != nullptr )
			{
				delete pThread;
			}
			if ( PatternToolConfig != nullptr )
			{
				delete PatternToolConfig;
			}
		}
	}
	
	return 0;
}
//==================================================================================================================================