//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "Config.h"
#include "Core.h"
#include "LDE.h"
#include "Misc.h"
#include "PEManager.h"
#include "Thread.h"
#include "FindPattern.h"
//==================================================================================================================================
void AddRVAEntry ( MemoryLocation_t* pCurrentMemoryLocation );
void AddRVAEntry ( Thread_Data_t* pThreadData, uintptr_t BestAddress, double BestRatio, size_t ThreadIndex );
//==================================================================================================================================
void FillSuffixes ( char* pszPattern, size_t uiPatternSize, char* pszGoodSuffixes ) // todo: clean this up later
{
	int uiIterator = 0, uiPatternEnd = 0, uiTemporary = 0;

	int iSuffixes[ XSIZE ];

	iSuffixes[ uiPatternSize - 1 ] = uiPatternSize;
	uiPatternEnd = uiPatternSize - 1;

	for ( uiIterator = uiPatternSize - 2; uiIterator >= 0; --uiIterator )
	{
		if ( uiIterator > uiPatternEnd && iSuffixes[ uiIterator + uiPatternSize - 1 - uiTemporary] < uiIterator - uiPatternEnd )
		{
			iSuffixes[ uiIterator ] = iSuffixes[ uiIterator + uiPatternSize - 1 - uiTemporary];
		}
		else
		{
			if ( uiIterator < uiPatternEnd )
			{
				uiPatternEnd = uiIterator;
			}

			uiTemporary = uiIterator;

			while ( uiPatternEnd >= 0 && pszPattern[ uiPatternEnd ] == pszPattern[ uiPatternEnd + uiPatternSize - 1 - uiTemporary ] )
			{
				--uiPatternEnd;
			}
			
			iSuffixes[ uiIterator ] = uiTemporary - uiPatternEnd;
		}
	}

	for ( uiIterator = 0; uiIterator < uiPatternSize; uiIterator++ )
	{
		pszGoodSuffixes[ uiIterator ] = uiPatternSize;
	}

	uiTemporary = 0;

	for ( uiIterator = uiPatternSize - 1; uiIterator >= 0; --uiIterator )
	{
		if ( iSuffixes[ uiIterator ] == uiIterator + 1 )
		{
			for ( ; uiTemporary < uiPatternSize - 1 - uiIterator; ++uiTemporary )
			{
				if ( pszGoodSuffixes[ uiTemporary ] == uiPatternSize )
				{
					pszGoodSuffixes[ uiTemporary ] = uiPatternSize - 1 - uiIterator;
				}
			}
		}
	}
	for ( uiIterator = 0; uiIterator <= uiPatternSize - 2; ++uiIterator )
	{
		pszGoodSuffixes[ uiPatternSize - 1 - iSuffixes[ uiIterator ] ] = uiPatternSize - 1 - uiIterator;
	}
}
//==================================================================================================================================
void FillShiftTable ( char* Pattern, size_t PatternSize, char* BadCharSkip )
{	
	for ( int i = 0; i <= UCHAR_MAX; i++ )
	{
		BadCharSkip[i] = PatternSize;
	}
	for ( int i = 0; i < PatternSize - 1; i++ )
	{
		BadCharSkip[ Pattern[i] ] = ( int )PatternSize - i - 1;
	}
}
//==================================================================================================================================
double BMGetRatioMatchData ( uintptr_t Base, uintptr_t BlockBegin, uintptr_t BlockSize, PBYTE Pattern, size_t SizeOfPattern, char* BadCharSkip, char* GoodSuffixes, int& Shift )
{
	double Ratio = 0;

	uintptr_t BlockEnd, PatternEnd;

	PBYTE CurrentPointer;

	size_t Matched;

	int PatternSize = SizeOfPattern, Position = SizeOfPattern;

	Matched = 0;

	BlockEnd = BlockBegin + BlockSize;

	PatternEnd = Base + SizeOfPattern;

	CurrentPointer = ( PBYTE )Base;
		
	if ( PatternEnd < BlockEnd )
	{
		while ( Pattern[ Position - 1 ] == CurrentPointer[ Position - 1 ] )
		{				
			Matched++;
			
			Position--;

			if ( Position <= 0 )
			{
				break;
			}
		}
	}

	if ( Position == 0 )
	{
		Ratio = 1.0;
	}
	else
	{
		if ( Matched > SizeOfPattern )
		{
			Ratio = 0;
		}
		else
		{
			Ratio = ( double )Matched / ( double )SizeOfPattern;
		}
	}

	if ( Position > 0 )
	{
		Shift = MAX ( BadCharSkip[ CurrentPointer[ Position-1 ] ] - PatternSize + 1 - Position, GoodSuffixes[ Position-1 ] );
	}
	
	return Ratio;
}
//==================================================================================================================================
double GetRatioMatch ( uintptr_t Base, PBYTE Pattern, size_t SizeOfPattern )
{
	double Ratio = 0;

	PBYTE CurrentPointer;

	size_t Matched, Position;
			
	CurrentPointer = ( PBYTE )Base;

	Position = 0;

	Matched = 0;

	while ( Position < SizeOfPattern )
	{	
		if ( *Pattern++ == *CurrentPointer++ )
		{
			Matched++;
		}
		
		Position++;
	}

	if ( Matched > SizeOfPattern )
	{
		Ratio = 0;
	}
	else
	{
		Ratio = ( double )Matched / ( double )SizeOfPattern;
	}
	
	return Ratio;
}
//==================================================================================================================================
double GetRatioMatch ( uintptr_t Base, uintptr_t BlockBegin, uintptr_t BlockSize, PBYTE Pattern, size_t SizeOfPattern )
{
	double Ratio = 0;

	uintptr_t BlockEnd;

	PBYTE CurrentPointer;

	size_t Matched, Position;
			
	CurrentPointer = ( PBYTE )Base;

	Position = 0;

	Matched = 0;

	BlockEnd = BlockBegin + BlockSize;

	while ( Position < SizeOfPattern && ( uintptr_t )CurrentPointer < BlockEnd )
	{	
		if ( *Pattern++ == *CurrentPointer++ )
		{
			Matched++;
		}
							
		Position++;
	}

	if ( Matched > SizeOfPattern )
	{
		Ratio = 0;
	}
	else
	{
		Ratio = ( double )Matched / ( double )SizeOfPattern;
	}
	
	return Ratio;
}
//==================================================================================================================================
DWORD MatchCodeThreaded ( LPVOID lpArgs )
{
	Thread_Data_t* pThreadData = ( Thread_Data_t* )lpArgs;
		
	double CurrentRatio, BestRatio;

	int CurrentLength;

	PBYTE CurrentPattern, CurrentPointer;

	size_t ThreadIndex;

	uintptr_t BestAddress, RegionEnd, RegionBase, TotalRegion;

	ThreadIndex = pThreadData->Thread->GetThreadIndexByThreadID ( GetCurrentThreadId() );

	CurrentPointer = ( PBYTE )pThreadData->FindPatternData [ ThreadIndex ].BlockBegin;

	RegionEnd = pThreadData->FindPatternData [ ThreadIndex ].BlockBegin + pThreadData->FindPatternData [ ThreadIndex ].BlockEnd;

	RegionBase = pThreadData->RegionBase;

	TotalRegion = pThreadData->RegionBase + pThreadData->RegionSize;

	TotalRegion -= MAX_INSTRUCTION_SIZE; // MAX INSTRUCTION SIZE

	BestAddress = 0;
	BestRatio = 0;
	CurrentRatio = 0;

	uintptr_t dwDelta;
		
	LDEDisasm_t Data;

	while ( ( uintptr_t )CurrentPointer < RegionEnd - pThreadData->SizeOfPattern )
	{
		memset ( &Data, 0, sizeof ( LDEDisasm_t ) );

		if ( pThreadData->Current->IsImage64() )
		{
			CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_64 );
		}
		else
		{
			CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_32 );
		}
	
		dwDelta = ( uintptr_t )CurrentPointer - RegionBase;
				
		CurrentPattern = CurrentPointer;
				
		CurrentRatio = GetRatioMatch ( ( uintptr_t )CurrentPattern, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern );
								
		if ( CurrentRatio > BestRatio )
		{
			BestRatio = CurrentRatio;

			BestAddress = ( uintptr_t )CurrentPointer;
		}

		if ( CurrentRatio == 1.0f 
			&& pThreadData->CurrentMemoryLocation->FindAllOccurances == false )
		{
			pThreadData->Thread->SignalExit();

			AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );

			break;
		}
		
		if ( pThreadData->Thread->IsExitSignaled() == true )
		{
			break;
		}

		if ( CurrentLength != -1 )
		{
			CurrentPointer += CurrentLength;				
		}
		else
		{
			CurrentPointer++;
		}
	}
	if ( pThreadData->CurrentMemoryLocation->FindAllOccurances == true )
	{
		CurrentPointer = ( PBYTE )pThreadData->FindPatternData [ ThreadIndex ].BlockBegin;

		while ( ( uintptr_t )CurrentPointer < RegionEnd - pThreadData->SizeOfPattern
			&& ( uintptr_t )CurrentPointer < TotalRegion - pThreadData->SizeOfPattern )
		{			
			memset ( &Data, 0, sizeof ( LDEDisasm_t ) );

			if ( pThreadData->Current->IsImage64() )
			{
				CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_64 );
			}
			else
			{
				CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_32 );
			}
						
			CurrentPattern = CurrentPointer;

			CurrentRatio = GetRatioMatch ( ( uintptr_t )CurrentPattern, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern );
			
			dwDelta = ( ( uintptr_t )CurrentPointer - pThreadData->AllocationBase );

			if ( CurrentRatio >= BestRatio )
			{
				BestRatio = CurrentRatio;

				BestAddress = ( uintptr_t )CurrentPointer;

				AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );
			}
	
			if ( CurrentLength != -1 )
			{
				CurrentPointer += CurrentLength;				
			}
			else
			{
				CurrentPointer++;
			}
		}
	}
	else
	{
		if ( BestRatio != 1.0f && BestAddress != 0 )
		{	
			AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );
		}
	}
	
	while ( pThreadData->Thread->CanWrite() == false )
	{
		Sleep ( 0 );

		if ( pThreadData->Thread->AcquireWrite() )
		{
			if ( BestRatio > pThreadData->BestOverallRatio )
			{
				pThreadData->BestOverallRatio = BestRatio;
			}

			break;
		}
	}
		
	pThreadData->Thread->UnAcquireWrite();

	pThreadData->Thread->SignalEvent ( pThreadData->Thread->GetEventHandle ( ThreadIndex ) );

	pThreadData->FindPatternData [ ThreadIndex ].BestOutRatio = BestRatio;
	
	return 0;
}
//==================================================================================================================================
DWORD MatchCodeVTableThreaded ( LPVOID lpArgs )
{
	Thread_Data_t*			pThreadData = ( Thread_Data_t* )lpArgs;

	uintptr_t BestAddress, RegionEnd, RegionBase, TotalRegion;

	double CurrentRatio, BestRatio;

	int CurrentLength;

	PBYTE CurrentPattern, CurrentPointer;

	size_t ThreadIndex;

	ThreadIndex = pThreadData->Thread->GetThreadIndexByThreadID ( GetCurrentThreadId() );

	CurrentPointer = ( PBYTE )pThreadData->FindPatternData [ ThreadIndex ].BlockBegin;

	RegionEnd = pThreadData->FindPatternData [ ThreadIndex ].BlockBegin + pThreadData->FindPatternData [ ThreadIndex ].BlockEnd;

	RegionBase = pThreadData->RegionBase;

	TotalRegion = pThreadData->RegionBase + pThreadData->RegionSize;

	TotalRegion -= MAX_INSTRUCTION_SIZE; // MAX INSTRUCTION SIZE

	BestAddress = 0;
	BestRatio = 0;
	CurrentRatio = 0;

	LDEDisasm_t Data;

	while ( ( DWORD_PTR )CurrentPointer < RegionEnd - pThreadData->SizeOfPattern )
	{
		memset ( &Data, 0, sizeof ( LDEDisasm_t ) );
		
		if ( pThreadData->Current->IsImage64() )
		{
			CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_64 );
		}
		else
		{
			CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_32 );
		}
		
		CurrentPattern = CurrentPointer;
				
		CurrentRatio = GetRatioMatch ( ( uintptr_t )CurrentPattern, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern );
								
		if ( CurrentRatio > BestRatio )
		{
			BestRatio = CurrentRatio;

			BestAddress = ( uintptr_t )CurrentPointer;
		}

		if ( CurrentRatio == 1.0f 
			&& pThreadData->CurrentMemoryLocation->FindAllOccurances == false )
		{
			pThreadData->Thread->SignalExit();

			break;
		}
		
		if ( pThreadData->Thread->IsExitSignaled() == true )
		{
			break;
		}
						
		if ( CurrentLength == -1 )
		{
			CurrentPointer++;
		}
		else
		{
			CurrentPointer += CurrentLength;
		}
	}
	if ( pThreadData->CurrentMemoryLocation->FindAllOccurances == true )
	{
		CurrentPointer = ( PBYTE )pThreadData->FindPatternData [ ThreadIndex ].BlockBegin;

		while ( ( uintptr_t )CurrentPointer < RegionEnd 
			&& ( uintptr_t )CurrentPointer < TotalRegion )
		{			
			memset ( &Data, 0, sizeof ( LDEDisasm_t ) );

			if ( pThreadData->Current->IsImage64() )
			{
				CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_64 );
			}
			else
			{
				CurrentLength = LDEDisasm ( &Data, ( PBYTE )CurrentPointer, MODE_32 );
			}
			
			CurrentPattern = CurrentPointer;

			if ( pThreadData->CurrentMemoryLocation->UseCondenser )
			{				
				CurrentRatio = GetRatioMatch ( ( uintptr_t )CurrentPattern, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern );
			}
			else
			{
				CurrentRatio = GetRatioMatch ( ( uintptr_t )CurrentPointer, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern );
			}

			if ( CurrentRatio >= BestRatio )
			{
				BestRatio = CurrentRatio;

				BestAddress = ( uintptr_t )CurrentPointer;
			}
									
			if ( CurrentLength == -1 )
			{
				CurrentPointer++;
			}
			else
			{
				CurrentPointer += CurrentLength;
			}
		}
	}
		
	while ( pThreadData->Thread->CanWrite() == false )
	{
		Sleep ( 0 );

		if ( pThreadData->Thread->AcquireWrite() )
		{
			break;
		}
	}

	if ( BestRatio > pThreadData->BestOverallRatio )
	{
		pThreadData->BestOverallRatio = BestRatio;
	}
		
	pThreadData->FindPatternData [ ThreadIndex ].BestOutRatio = BestRatio;
	pThreadData->FindPatternData [ ThreadIndex ].BestAddress = BestAddress;

	pThreadData->Thread->UnAcquireWrite();

	pThreadData->Thread->SignalEvent ( pThreadData->Thread->GetEventHandle ( ThreadIndex ) );

	return 0;
}
//==================================================================================================================================
DWORD MatchDataThreaded ( LPVOID lpArgs )
{
	char BadCharSkip [ 256 ];
	char GoodSuffixes [ 256 ];

	double BestRatio, CurrentRatio;

	int Shift;

	PBYTE CurrentPointer;

	size_t Size, ThreadIndex;

	Thread_Data_t* pThreadData = ( Thread_Data_t* )lpArgs;

	uintptr_t BestAddress, RegionEnd, RegionBase, RVA, TotalRegion;
	 
	memset ( BadCharSkip, 0, sizeof ( BadCharSkip ) );
	memset ( GoodSuffixes, 0, sizeof ( GoodSuffixes ) );

	FillShiftTable ( ( char* )pThreadData->Pattern, pThreadData->SizeOfPattern, BadCharSkip );
	FillSuffixes ( ( char* )pThreadData->Pattern, pThreadData->SizeOfPattern, GoodSuffixes );

	ThreadIndex = pThreadData->Thread->GetThreadIndexByThreadID ( GetCurrentThreadId() );

	CurrentPointer = ( PBYTE )pThreadData->FindPatternData [ ThreadIndex ].BlockBegin;

	RegionEnd = pThreadData->FindPatternData [ ThreadIndex ].BlockBegin + pThreadData->FindPatternData [ ThreadIndex ].BlockEnd;

	RegionBase = pThreadData->RegionBase;

	TotalRegion = pThreadData->RegionBase + pThreadData->RegionSize;

	BestRatio = 0;

	Size = pThreadData->FindPatternData [ ThreadIndex ].BlockEnd;

	while ( ( DWORD_PTR )CurrentPointer < RegionEnd - pThreadData->SizeOfPattern
		&& ( DWORD_PTR )CurrentPointer < TotalRegion - pThreadData->SizeOfPattern )
	{
		CurrentRatio = BMGetRatioMatchData ( ( uintptr_t )CurrentPointer, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern, BadCharSkip, GoodSuffixes, Shift );
		
		if ( CurrentRatio >= BestRatio  )
		{
			BestRatio = CurrentRatio;

			BestAddress = ( uintptr_t )CurrentPointer;
		}
		
		if ( CurrentRatio == 1.0f 
			&& pThreadData->CurrentMemoryLocation->FindAllOccurances == false )
		{	
			if ( pThreadData->CurrentMemoryLocation->IsReferenced == true )
			{
				RVA = BestAddress - pThreadData->AllocationBase;
				
				if ( std::bsearch ( &RVA, pThreadData->Current->GetRelocationsVectorRVA2().data(), pThreadData->Current->GetRelocationsVectorRVA2().size(), sizeof ( DWORD_PTR ), compare ) )
				{
					pThreadData->Thread->SignalExit();

					AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );

					break;
				}
			}
			else
			{
				pThreadData->Thread->SignalExit();

				AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );

				break;
			}
		}
		
		if ( pThreadData->Thread->IsExitSignaled() == true )
		{
			break;
		}

		CurrentPointer += Shift;
	}
	if ( pThreadData->CurrentMemoryLocation->FindAllOccurances == true )
	{
		CurrentPointer = ( PBYTE )pThreadData->FindPatternData [ ThreadIndex ].BlockBegin;

		while ( ( uintptr_t )CurrentPointer < RegionEnd - pThreadData->SizeOfPattern
			&& ( uintptr_t )CurrentPointer < TotalRegion - pThreadData->SizeOfPattern )
		{

			CurrentRatio = BMGetRatioMatchData ( ( uintptr_t )CurrentPointer, pThreadData->FindPatternData [ ThreadIndex ].BlockBegin, pThreadData->FindPatternData [ ThreadIndex ].BlockEnd, pThreadData->Pattern, pThreadData->SizeOfPattern, BadCharSkip, GoodSuffixes, Shift );
			
			if ( CurrentRatio >= BestRatio )
			{
				if ( pThreadData->CurrentMemoryLocation->IsReferenced == true )
				{
					RVA = BestAddress - pThreadData->AllocationBase;

					if ( std::bsearch ( &RVA, pThreadData->Current->GetRelocationsVectorRVA2().data(), pThreadData->Current->GetRelocationsVectorRVA2().size(), sizeof ( DWORD_PTR ), compare )
/*						|| std::bsearch ( &RVA, pThreadData->Current->GetDisplacementAndImmediate().data(), pThreadData->Current->GetDisplacementAndImmediate().size(), sizeof ( DWORD_PTR ), compare )*/ 
						
						)
					{
						BestRatio = CurrentRatio;

						BestAddress = ( uintptr_t )CurrentPointer;

						AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );
					}
				}
				else
				{
					BestRatio = CurrentRatio;

					BestAddress = ( uintptr_t )CurrentPointer;

					AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );
				}
			}

			CurrentPointer += Shift;
		}
	}
	else
	{
		if ( BestRatio != 1.0f && BestAddress != 0 )
		{
			if ( pThreadData->CurrentMemoryLocation->IsReferenced == true )
			{
				RVA = BestAddress - pThreadData->AllocationBase;

				if ( std::bsearch ( &RVA, pThreadData->Current->GetRelocationsVectorRVA2().data(), pThreadData->Current->GetRelocationsVectorRVA2().size(), sizeof ( uintptr_t ), compare )
/*					|| std::bsearch ( &RVA, pThreadData->Current->GetDisplacementAndImmediate().data(), pThreadData->Current->GetDisplacementAndImmediate().size(), sizeof ( uintptr_t ), compare )*/ )
				{
					AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );
				}
			}
			else
			{
				AddRVAEntry ( pThreadData, BestAddress, BestRatio, ThreadIndex );
			}
		}
	}

	while ( pThreadData->Thread->CanWrite() == false )
	{
		Sleep ( 0 );

		if ( pThreadData->Thread->AcquireWrite() )
		{
			break;
		}
	}

	if ( BestRatio > pThreadData->BestOverallRatio )
	{
		pThreadData->BestOverallRatio = BestRatio;
	}
		
	pThreadData->Thread->UnAcquireWrite();

	pThreadData->Thread->SignalEvent ( pThreadData->Thread->GetEventHandle ( ThreadIndex ) );

	pThreadData->FindPatternData [ ThreadIndex ].BestOutRatio = BestRatio;

	return 0;
}
//==================================================================================================================================
void CFindPattern::FindPatternCode ( uintptr_t AllocationBase, uintptr_t Current, uintptr_t Base, uintptr_t Size, 
	PBYTE Pattern, MemoryLocation_t* pCurrentMemoryLocation, CPEManager* pCurrent, CThread* pThread, std::vector<uintptr_t>& RelocationBlockCurrent, std::vector<uintptr_t>& RelocationBlockReference )
{
	double BestRatio, CurrentRatio;
	
	size_t uiIterator, SizeOfPattern;

	std::vector<FOUND_RVAS_t>::iterator it;

	PBYTE ScanPattern;

	Thread_Data_t* pThreadData = NULL;

	uintptr_t BestAddress, BlockSize, RegionEnd;
	
	RegionEnd = Base + Size;

	BestAddress = 0;
	CurrentRatio = 0;

	if ( pCurrent->IsImage64() )
	{

	}
	else
	{
		SizeOfPattern = GetAdjustedSignatureSize ( Pattern, pCurrentMemoryLocation->PatternSize, MODE_32 );
	}

	ScanPattern = new unsigned char [ SizeOfPattern ];

	if ( ScanPattern == nullptr )
	{
		return;
	}

	memcpy ( ScanPattern, Pattern, SizeOfPattern );

	if ( pCurrent->IsImage64() )
	{

	}
	else
	{
		CreatePattern ( ScanPattern, SizeOfPattern, pCurrentMemoryLocation->NeutralizeFlags, RelocationBlockReference, MODE_32 );
	}

	pThreadData = new Thread_Data_t;

	if ( pThreadData != NULL )
	{
		pThreadData->Current = pCurrent;

		pThreadData->BestOverallRatio = 0;

		pThreadData->FindPatternData = new FindPattern_Data_t [ pThread->GetNumberOfProcessors() ];

		if ( pThreadData->FindPatternData != NULL )
		{
			if ( RegionEnd < Core.GetSizeOfPage() )
			{
				BlockSize = Core.GetSizeOfPage() / pThread->GetNumberOfProcessors();
			}
			else
			{
				BlockSize = ROUND_DOWN ( Size, Core.GetSizeOfPage() ) / pThread->GetNumberOfProcessors();
			}

			pThreadData->AllocationBase = AllocationBase;

			pThreadData->RegionBase = Base;
			pThreadData->RegionSize = ROUND_UP ( Size, Core.GetSizeOfPage() );

			pThreadData->Thread = pThread;

			pThreadData->Pattern = ScanPattern;
			
			pThreadData->CurrentMemoryLocation = pCurrentMemoryLocation;
			pThreadData->SizeOfPattern = SizeOfPattern;

			for ( size_t ThreadCounter = 0; ThreadCounter < pThread->GetNumberOfProcessors(); ThreadCounter++ )
			{
				pThreadData->FindPatternData [ ThreadCounter ].BlockBegin = Base + BlockSize * ThreadCounter;
				pThreadData->FindPatternData [ ThreadCounter ].BlockEnd = BlockSize;
			}
			
			pThread->CreateRunningGroup ( ( LPVOID )MatchCodeThreaded, ( LPVOID )pThreadData );

			pThread->StartRunningGroup();

			while ( pThread->IsRunningGroupFinished() == false );

			pThread->EndRunningGroup();

			BestRatio = 0;

			for ( uiIterator = 0; uiIterator < pThread->GetNumberOfProcessors(); uiIterator++ )
			{
				for ( it = pThreadData->FindPatternData [ uiIterator ].FoundRVAS.begin(); it != pThreadData->FindPatternData [ uiIterator ].FoundRVAS.end(); ++it )
				{
					if ( it->RatioMatch > BestRatio )
					{
						BestRatio = it->RatioMatch;
					}
				}
			}
			
			for ( uiIterator = 0; uiIterator < pThread->GetNumberOfProcessors(); uiIterator++ )
			{
				for ( it = pThreadData->FindPatternData [ uiIterator ].FoundRVAS.begin(); it != pThreadData->FindPatternData [ uiIterator ].FoundRVAS.end(); ++it )
				{
					if ( it->RatioMatch >= pThreadData->BestOverallRatio )
					{
						it->OriginalPatternVA = ( uintptr_t )Pattern;

						pCurrentMemoryLocation->m_FoundRVAS.insert ( pCurrentMemoryLocation->m_FoundRVAS.end(), *it );
					}
				}
			}

			delete [] pThreadData->FindPatternData;
		}

		delete pThreadData;
	}

	delete [] ScanPattern;

	return;
}
//=============================================================================================================
void CFindPattern::FindPatternData ( CPEManager* pCurrent, uintptr_t AllocationBase, uintptr_t Current, uintptr_t Base, uintptr_t Size, 
									PBYTE Pattern, size_t SizeOfPattern, MemoryLocation_t* pCurrentMemoryLocation, CThread* pThread )
{
	uintptr_t BestAddress, BlockSize, RegionEnd;

	std::vector<FOUND_RVAS_t>::iterator it;

	Thread_Data_t* pThreadData = NULL;
	
	RegionEnd = Base + Size;

	BestAddress = 0;
	
	pThreadData = new Thread_Data_t;

	if ( pThreadData != NULL )
	{
		pThreadData->Current = pCurrent;

		pThreadData->FindPatternData = new FindPattern_Data_t [ pThread->GetNumberOfProcessors() ];

		if ( pThreadData->FindPatternData != nullptr )
		{
			if ( RegionEnd < Core.GetSizeOfPage() )
			{
				BlockSize = Core.GetSizeOfPage() / pThread->GetNumberOfProcessors();
			}
			else
			{
				BlockSize = ROUND_DOWN ( Size, Core.GetSizeOfPage() ) / pThread->GetNumberOfProcessors();
			}

			pThreadData->AllocationBase = AllocationBase;

			pThreadData->Current = pCurrent;

			pThreadData->RegionBase = Base;
			pThreadData->RegionSize = ROUND_UP ( Size, Core.GetSizeOfPage() );

			pThreadData->Thread = pThread;

			pThreadData->Pattern = Pattern;
			
			pThreadData->CurrentMemoryLocation = pCurrentMemoryLocation;
			pThreadData->SizeOfPattern = SizeOfPattern;

			for ( size_t ThreadCounter = 0; ThreadCounter < pThread->GetNumberOfProcessors(); ThreadCounter++ )
			{
				pThreadData->FindPatternData [ ThreadCounter ].BlockBegin = Base + BlockSize * ThreadCounter;
				pThreadData->FindPatternData [ ThreadCounter ].BlockEnd = BlockSize;
			}

			pThread->CreateRunningGroup ( ( LPVOID )MatchDataThreaded, ( LPVOID )pThreadData );

			pThread->StartRunningGroup();

			while ( pThread->IsRunningGroupFinished() == false );

			pThread->EndRunningGroup();

			for ( size_t ThreadCounter = 0; ThreadCounter < pThread->GetNumberOfProcessors(); ThreadCounter++ )
			{
				for ( it = pThreadData->FindPatternData [ ThreadCounter ].FoundRVAS.begin(); it != pThreadData->FindPatternData [ ThreadCounter ].FoundRVAS.end(); ++it )
				{					
					if ( it->RatioMatch >= pThreadData->BestOverallRatio )
					{
						pCurrentMemoryLocation->m_FoundRVAS.insert ( pCurrentMemoryLocation->m_FoundRVAS.end(), *it );
					}
				}
			}

			delete [] pThreadData->FindPatternData;
		}

		delete pThreadData;
	}

	return;
}
//==================================================================================================================================
uintptr_t CFindPattern::FindPatternVTable ( uintptr_t Current, uintptr_t Base, uintptr_t Size, 
	PBYTE Pattern, size_t SizeOfPattern, MemoryLocation_t* pCurrentMemoryLocation, CPEManager* pCurrent, CThread* pThread, std::vector<uintptr_t>& RelocationBlockCurrent, std::vector<uintptr_t>& RelocationBlockReference )
{
	double BestRatio, CurrentRatio;
	
	uintptr_t BestAddress, BlockSize, RegionEnd;

	MemoryLocation_t CurrentMemoryLocation;

	PBYTE ScanPattern;

	Thread_Data_t* pThreadData = NULL;

	BestRatio = 0;

	if ( pCurrent->IsImage64() )
	{

	}
	else
	{
		SizeOfPattern = GetAdjustedSignatureSize ( Pattern, pCurrentMemoryLocation->PatternSize, MODE_32 );
	}

	ScanPattern = new unsigned char [ SizeOfPattern ];

	if ( ScanPattern == nullptr )
	{
		return 0;
	}

	memcpy ( ScanPattern, Pattern, SizeOfPattern );

	if ( pCurrent->IsImage64() )
	{

	}
	else
	{
		CreatePattern ( ScanPattern, SizeOfPattern, pCurrentMemoryLocation->NeutralizeFlags, RelocationBlockReference, MODE_32 );
	}
	
	RegionEnd = Base + Size;

	BestAddress = 0;
	BestRatio = 0;
	CurrentRatio = 0;

	pThreadData = new Thread_Data_t;

	if ( pThreadData != NULL )
	{
		pThreadData->Current = pCurrent;

		pThreadData->FindPatternData = new FindPattern_Data_t [ pThread->GetNumberOfProcessors() ];

		if ( pThreadData->FindPatternData != NULL )
		{
			if ( RegionEnd < Core.GetSizeOfPage() )
			{
				BlockSize = Core.GetSizeOfPage() / pThread->GetNumberOfProcessors();
			}
			else
			{
				BlockSize = ROUND_DOWN ( Size, Core.GetSizeOfPage() ) / pThread->GetNumberOfProcessors();
			}

			pThreadData->RegionBase = ROUND_DOWN ( Base, Core.GetSizeOfPage() );
			pThreadData->RegionSize = ROUND_UP ( Size, Core.GetSizeOfPage() );

			pThreadData->Thread = pThread;

			pThreadData->Pattern = ScanPattern;
			pThreadData->SizeOfPattern = SizeOfPattern;

			for ( size_t ThreadCounter = 0; ThreadCounter < pThread->GetNumberOfProcessors(); ThreadCounter++ )
			{
				pThreadData->FindPatternData [ ThreadCounter ].BlockBegin = Base + BlockSize * ThreadCounter;
				pThreadData->FindPatternData [ ThreadCounter ].BlockEnd = BlockSize;
			}

			pThreadData->CurrentMemoryLocation = &CurrentMemoryLocation;

			pThread->CreateRunningGroup ( ( LPVOID )MatchCodeVTableThreaded, ( LPVOID )pThreadData );

			pThread->StartRunningGroup();

			while ( pThread->IsRunningGroupFinished() == false );

			pThread->EndRunningGroup();

			for ( size_t ThreadCounter = 0; ThreadCounter < pThread->GetNumberOfProcessors(); ThreadCounter++ )
			{
				if ( pThreadData->FindPatternData [ ThreadCounter ].BestOutRatio > BestRatio )
				{
					BestRatio = pThreadData->FindPatternData [ ThreadCounter ].BestOutRatio;

					BestAddress = pThreadData->FindPatternData [ ThreadCounter ].BestAddress;
				}
			}

			delete [] pThreadData->FindPatternData;
		}

		delete pThreadData;
	}

	return BestAddress;
}
//==================================================================================================================================
void AddRVAEntry ( MemoryLocation_t* pCurrentMemoryLocation )
{	
	FOUND_RVAS_t FOUND_RVA;

	memset ( &FOUND_RVA, 0, sizeof ( FOUND_RVAS_t ) );

	pCurrentMemoryLocation->m_FoundRVAS.push_back ( FOUND_RVA );
}
//==================================================================================================================================
void AddRVAEntry ( Thread_Data_t* pThreadData, uintptr_t BestAddress, double BestRatio, size_t ThreadIndex )
{
	FOUND_RVAS_t FOUND_RVA;

	memset ( &FOUND_RVA, 0, sizeof ( FOUND_RVAS_t ) );

	FOUND_RVA.RatioMatch = BestRatio;
	FOUND_RVA.VA = BestAddress;
	FOUND_RVA.RVA = BestAddress - pThreadData->AllocationBase;

	FOUND_RVA.PatternBuffer = ( PBYTE )( pThreadData->Current->GetAllocationBase() + ( BestAddress - pThreadData->AllocationBase ) );

	pThreadData->FindPatternData[ ThreadIndex ].FoundRVAS.push_back ( FOUND_RVA );
}
//==================================================================================================================================