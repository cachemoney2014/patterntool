//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "LDE.h"
#include "Common.h"
#include "Condenser.h"
//=============================================================================================================
int Compare(const void *ap, const void *bp)
{
    const uintptr_t *a = (uintptr_t *) ap;
    const uintptr_t *b = (uintptr_t *) bp;
    if(*a < *b)
        return -1;
    else if(*a > *b)
        return 1;
    else
        return 0;
}
//=============================================================================================================
int CompareBlocksBsearch(const void *ap, const void *bp)
{
    const Block_t *a = (Block_t *) ap;
    const Block_t *b = (Block_t *) bp;
    if( a->BlockStart < b->BlockStart )
        return -1;
    else if( a->BlockStart > b->BlockStart )
        return 1;
    else
        return 0;
}
//=============================================================================================================
bool CompareBlocks ( const Block_t& lhs, const Block_t & rhs )
{
	return ( lhs.BlockStart < rhs.BlockStart );
}
//=============================================================================================================
bool CompareBasicBlocks ( const BasicBlock_t& lhs, const BasicBlock_t & rhs )
{
	return ( lhs.BlockStart < rhs.BlockStart );
}
//=============================================================================================================
bool CompareBlocksUnique ( const Block_t& lhs, const Block_t & rhs )
{
	return ( lhs.BlockStart == rhs.BlockStart && lhs.BlockEnd == rhs.BlockEnd );
}
//=============================================================================================================
bool CompareBasicBlocksUnique ( const BasicBlock_t& lhs, const BasicBlock_t & rhs )
{
	return ( lhs.BlockStart == rhs.BlockStart && lhs.BlockEnd == rhs.BlockEnd );
}
//=============================================================================================================
CCondenser::CCondenser ( bool FollowCalls, uintptr_t DisasmMode, size_t AllocationSize )
{
	m_FollowCalls = FollowCalls;
	m_DisasmMode = DisasmMode;
	
	m_FixedFunction = NULL;

	m_MaxAllocationSize = AllocationSize;

	m_FixedFunction = new BYTE [ AllocationSize ];

	if ( m_FixedFunction != NULL )
	{
		memset ( m_FixedFunction, 0, AllocationSize );
	}

	m_Partials = NULL;

	m_PartialCount = 0;

	m_InFixup = false;

	m_MaxBytes = MAX_INSTRUCTIONS;
}
//=============================================================================================================
CCondenser::~CCondenser()
{
	size_t Iterator;

	if ( m_FixedFunction != NULL )
	{
		delete m_FixedFunction;
	}
	if ( m_Partials != NULL )
	{
		for ( Iterator = 0; Iterator < m_PartialCount; Iterator++ )
		{
			if ( m_Partials [ Iterator ] != NULL )
			{
				delete m_Partials [ Iterator ];
			}
		}

		delete m_Partials;

		m_Partials = NULL;

		m_PartialCount = 0;
	}
}
//========================================================================================
PBYTE CCondenser::GetPartial ( size_t uiIndex )
{
	if ( m_PartialCount == 0 )
	{
		return NULL;
	}

	if ( uiIndex < m_PartialCount )
	{
		return m_Partials [ uiIndex ];
	}

	return NULL; 
}
//========================================================================================
size_t CCondenser::GetLengthOfPartial ( size_t uiIndex )
{
	uintptr_t Length;

	Length = 0;

	if ( m_PartialCount == 0 )
	{
		return 0;
	}

	if ( uiIndex < m_PartialCount )
	{
		Length = GetSizeOfBlock ( ( uintptr_t )m_Partials [ uiIndex ] );

		return Length;
	}

	return 0; 
}
//========================================================================================
int CCondenser::GetInstructionLength ( uintptr_t Address )
{
	int Length = -1;
	
	memset ( &m_Disasm, 0, sizeof ( LDEDisasm_t ) );
	
	Length = LDEDisasm ( &m_Disasm, ( unsigned char* )Address, m_DisasmMode );

	return Length;
}
//========================================================================================
uintptr_t CCondenser::GetFunctionEnd ( uintptr_t Address )
{	
	m_BlockEnd = GetBranchListFromBlock ( Address );
		
	if ( m_Branches.size() == 0 
		|| m_Blocks.size() >= MAX_BLOCKS 
		|| m_Branches.size() >= MAX_BRANCHES
		|| m_Instructions.size() >= MAX_INSTRUCTIONS )
	{
		return m_BlockEnd;
	}
				
	for ( uintptr_t m_BranchIterator = 0; m_BranchIterator < m_Branches.size(); m_BranchIterator++ )
	{
		if ( m_Branches[ m_BranchIterator ] <= m_BlockEnd ||  m_Branches[ m_BranchIterator ] == m_PreviousBranch )											  
		{
			continue;
		}
				
		m_BlockEnd = GetBranchListFromBlock ( m_Branches[ m_BranchIterator ] );
		
		m_PreviousBranch = m_Branches[ m_BranchIterator ];
	}

	return m_BlockEnd;
}
//========================================================================================
uintptr_t CCondenser::GetBranchListFromBlock ( uintptr_t CurrentBlock )
{
	Block_t Block;

	uintptr_t* FoundInstance = NULL;

	uintptr_t CurrentAddress = CurrentBlock;
	uintptr_t BranchAddress;

	while ( 1 ) // keep going until we hit a predefined end point, jump to previous branch, or retn opcode(s)
	{
		m_Length = GetInstructionLength ( CurrentAddress );
		
		if ( IsEndPoint ( CurrentBlock ) )
		{
			break;
		}

		BranchAddress = GetBranchAddressWrapper ( &m_Disasm );

		if ( IsUnConditionalBranch ( &m_Disasm ) && BranchAddress != 0 && BranchAddress > CurrentAddress )
		{
			CurrentAddress = BranchAddress;
		}
		else
		{
			CurrentAddress += m_Length;
		}
	}

	if ( m_Blocks.size() < MAX_BLOCKS )
	{
		memset ( &Block, 0, sizeof ( Block_t ) );

		Block.BlockStart = CurrentBlock;
		Block.BlockEnd =  CurrentAddress;

		FoundInstance = ( uintptr_t* )std::bsearch ( &Block, m_Blocks.data(), m_Blocks.size(), sizeof ( Block_t ), CompareBlocksBsearch );

		if ( FoundInstance == NULL )
		{			
			m_Blocks.push_back ( Block );
		}
	}

	std::sort ( m_Blocks.begin(), m_Blocks.end(), CompareBlocks );
	std::sort ( m_Instructions.begin(), m_Instructions.end() );
	std::sort ( m_InstructionRVA.begin(), m_InstructionRVA.end() );
	std::sort ( m_InstructionBackup.begin(), m_InstructionBackup.end() );
	
	return CurrentAddress;
}
//=============================================================================================================
uintptr_t CCondenser::GetBranchAddressWrapper ( LDEDisasm_t* pDisasm )
{
	signed long Offset = 0;

	uintptr_t BranchAddress = GetBranchAddress ( pDisasm );

	if ( BranchAddress != 0 )
	{
		if ( pDisasm->Prefix0F == 0 )
		{
			switch ( pDisasm->Opcode )
			{
				case INSTR_RELCALL:
				{
					BranchAddress = pDisasm->CurrentPointer + pDisasm->Length;

					if ( m_FollowCalls || m_InFixup )
					{
						BranchAddress = pDisasm->CurrentPointer + pDisasm->Relative.Relative32 + pDisasm->Length;
					}
				}
				break;
				
				default:
				{
					
				}
				break;
			}
		}
	}

	return BranchAddress;
}
//=============================================================================================================
uintptr_t CCondenser::GetSizeOfBlock ( uintptr_t CurrentBlock )
{
	BOOL FoundEnd;
	
	uintptr_t CurrentInstruction;

	uintptr_t BranchAddress = 0;

	FoundEnd = 0;

	CurrentInstruction = CurrentBlock;

	while ( FoundEnd != 1 )
	{
		m_Length = GetInstructionLength ( CurrentInstruction );

		if ( m_Length != -1 )
		{
			BranchAddress = GetBranchAddressWrapper ( &m_Disasm );
		
			if ( m_Disasm.Prefix0F == 0 )
			{
				switch ( m_Disasm.Opcode )
				{
					case INSTR_RET:
					case INSTR_RETN:
					case INSTR_RETFN:
					case INSTR_RETF:
					{
						FoundEnd = 1;
					}
					break;

					case INSTR_RELJMP:
					case INSTR_SHORTJMP:
					{
						if ( BranchAddress <= CurrentBlock ) // returning to a previous branch
						{
							FoundEnd = 1;
						}
					}
					break;
					
					case INSTR_FF:
					{
						if ( m_Disasm.ModRM_Reg == 4
							|| m_Disasm.ModRM_Reg == 5 )
						{
							FoundEnd = 1;
						}
					}
					break;

					default:
					{
					}
					break;
				}
			}

			CurrentInstruction += m_Length;
		}
		else
		{
			FoundEnd = 1;

			break;
		}
	}

	return CurrentInstruction - CurrentBlock;
}
//=============================================================================================================
BOOL CCondenser::IsEndPoint ( uintptr_t CurrentBlock )
{
	BasicBlock_t BasicBlock;

	BOOL ReturnValue;

	JumpTable_t JumpTable = {0};

	uintptr_t* FoundInstance;
	
	uintptr_t BranchAddress = 0;
	uintptr_t Scale = 0;
	uintptr_t JumpTableSize = 0;

	ReturnValue = 0;

	memset ( &BasicBlock, 0, sizeof ( BasicBlock_t ) );

	if ( m_Length != -1 )
	{
		BranchAddress = GetBranchAddressWrapper ( &m_Disasm );
		
		switch ( m_Disasm.Opcode )
		{
			case INSTR_RET:
			case INSTR_RETN:
			case INSTR_RETFN:
			case INSTR_RETF:
			{
				ReturnValue = 1;
			}
			break;

			case INSTR_RELJMP:
			case INSTR_SHORTJMP:
			{
				if ( BranchAddress <= CurrentBlock ) // returning to a previous branch
				{
					ReturnValue = 1;
				}
			}
			break;
			
			case INSTR_FF:
			{
				if ( m_Disasm.ModRM_Reg == 4
					|| m_Disasm.ModRM_Reg == 5 )
				{
					ReturnValue = 1;
				}
			}
			break;

			default:
			{
			}
			break;
		}
	
		if ( m_Instructions.size() < MAX_INSTRUCTIONS && m_Instructions.size() < m_MaxBytes )
		{
			FoundInstance = ( uintptr_t* )std::bsearch ( &m_Disasm.CurrentPointer, m_Instructions.data(), m_Instructions.size(), sizeof ( uintptr_t ), Compare );

			if ( FoundInstance == NULL && ShouldSkipInstr() == false && ShouldSkipJump() == false )
			{
				m_Instructions.push_back ( m_Disasm.CurrentPointer );
				m_InstructionRVA.push_back ( m_Disasm.CurrentPointer - m_RegionBase );
				m_InstructionBackup.push_back ( m_Disasm.CurrentPointer );
			}
		}
		else
		{
			ReturnValue = 1; // just pre-emptively return
		}

		if ( m_BranchesSorted.size() < MAX_BRANCHES )
		{
			if ( BranchAddress != 0 )
			{
				FoundInstance = ( uintptr_t* )std::bsearch ( &BranchAddress, m_BranchesSorted.data(), m_BranchesSorted.size(), sizeof ( uintptr_t ), Compare );

				if ( FoundInstance == NULL )
				{
					m_Branches.push_back ( BranchAddress );
					m_BranchesSorted.push_back ( BranchAddress );
				}

				std::sort ( m_BranchesSorted.begin(), m_BranchesSorted.end() );
			}
		}
	}
	else
	{
		ReturnValue = 1;
	}

	return ReturnValue;
}
//========================================================================================
void CCondenser::RemoveDuplicateBranches ( void )
{
	std::vector<uintptr_t>::iterator BranchIterator;

	std::sort ( m_Branches.begin(), m_Branches.end() );

	BranchIterator = std::unique ( m_Branches.begin(), m_Branches.end() );

	if ( BranchIterator != m_Branches.end() )
	{
		m_Branches.erase ( BranchIterator, m_Branches.end() );
	}

	std::sort ( m_Branches.begin(), m_Branches.end() );
}
//=============================================================================================================
void CCondenser::RemoveDuplicateBlocks ( void )
{
	std::vector<Block_t>::iterator BlockIterator;

	std::sort ( m_Blocks.begin(), m_Blocks.end(), CompareBlocks );

	BlockIterator = std::unique ( m_Blocks.begin(), m_Blocks.end(), CompareBlocksUnique );

	if ( BlockIterator != m_Blocks.end() )
	{
		m_Blocks.erase ( BlockIterator, m_Blocks.end() );
	}

	std::sort ( m_Blocks.begin(), m_Blocks.end(), CompareBlocks );
}
//=============================================================================================================
void CCondenser::RemoveDuplicateInstructions ( void )
{
	std::vector<uintptr_t>::iterator InstructionIterator;

	std::sort ( m_Instructions.begin(), m_Instructions.end() );

	InstructionIterator = std::unique ( m_Instructions.begin(), m_Instructions.end() );

	if ( InstructionIterator != m_Instructions.end() )
	{
		m_Instructions.erase ( InstructionIterator, m_Instructions.end() );
	}

	std::sort ( m_Instructions.begin(), m_Instructions.end() );
	
	std::sort ( m_InstructionBackup.begin(), m_InstructionBackup.end() );

	InstructionIterator = std::unique ( m_InstructionBackup.begin(), m_InstructionBackup.end() );

	if ( InstructionIterator != m_InstructionBackup.end() )
	{
		m_InstructionBackup.erase ( InstructionIterator, m_InstructionBackup.end() );
	}

	std::sort ( m_InstructionBackup.begin(), m_InstructionBackup.end() );
}
//=============================================================================================================
PBYTE CCondenser::GetCopiedFunctionBuffer ( PVOID pvFunctionBegin )
{
	if ( GetFunctionLength ( pvFunctionBegin, true ) != 0 )
	{
		return m_FixedFunction;
	}

	return NULL;
}
//=============================================================================================================
PBYTE CCondenser::GetFunctionBuffer()
{
	if ( m_FixedFunction != NULL )
	{
		return m_FixedFunction;
	}

	return NULL;
}
//=============================================================================================================
BOOL CCondenser::ShouldSkipJump()
{
	BOOL IsNotBranching = true;

	signed long Relative = 0;

	if ( m_Disasm.Prefix0F == 0 )
	{
		switch ( m_Disasm.Opcode )
		{
//			case INSTR_RELCALL:
			case INSTR_RELJMP:
			case INSTR_RELJCX:
			case INSTR_SHORTJMP:
			{
				Relative = m_Disasm.Relative.Relative32;

				IsNotBranching = false;
			}
			break;

			default:
			{
				if ( m_Disasm.Opcode >= INSTR_SHORTJCC_BEGIN && m_Disasm.Opcode <= INSTR_SHORTJCC_END
					 || m_Disasm.Opcode >= INSTR_LOOPNZ && m_Disasm.Opcode <= INSTR_RELJCX  )
				{
					Relative = m_Disasm.Relative.Relative32;

					IsNotBranching = false;
				}
			}
		}
	}
	else
	{
		if ( m_Disasm.Opcode >= INSTR_JCC_BEGIN && m_Disasm.Opcode <= INSTR_JCC_END )
		{
			Relative = m_Disasm.Relative.Relative32;

			IsNotBranching = false;
		}
	}
	if ( IsNotBranching == false )
	{
		return ( Relative == 0 );
	}

	return false;
}
//=============================================================================================================
BOOL CCondenser::ShouldSkipInstr()
{
	if ( m_Disasm.Prefix0F == 0 )
	{
		switch ( m_Disasm.Opcode )
		{
			case INSTR_NOP:
			{
				return true;
			}
			break;

			case INSTR_INT3:
			{
				return true;
			}
			break;

			default:
			{

			}
			break;
		}
	}

	return false;
}
//=============================================================================================================
BOOL CCondenser::ShouldSkipJump ( uintptr_t CurrentInstructionAddress )
{
	uintptr_t BranchAddress;

	uintptr_t NextInstructionIndex, Offset;

	uintptr_t* FoundInstance;

	Offset = 0;

	// TODO: Check CALL $+5, as this simply pushes the next EIP onto the stack
	// Emulation may be necessary to determine if it's used or not.

	if ( m_Disasm.Prefix0F == 0 )
	{
		switch ( m_Disasm.Opcode )
		{
//			case INSTR_RELCALL:
			case INSTR_RELJMP:
			case INSTR_RELJCX:
			case INSTR_SHORTJMP:
			{
				Offset = 1;
			}
			break;

			default:
			{
				if ( m_Disasm.Opcode >= INSTR_SHORTJCC_BEGIN && m_Disasm.Opcode <= INSTR_SHORTJCC_END
					 || m_Disasm.Opcode >= INSTR_LOOPNZ && m_Disasm.Opcode <= INSTR_RELJCX  )
				{
					Offset = 1;
				}
			}
		}
	}
	else
	{
		if ( m_Disasm.Opcode >= INSTR_JCC_BEGIN && m_Disasm.Opcode <= INSTR_JCC_END )
		{
			Offset = 2;
		}
	}
	if ( Offset != 0 )
	{
		FoundInstance = NULL;

		BranchAddress = GetBranchAddressWrapper ( &m_Disasm );
				
		FoundInstance = ( uintptr_t* )std::bsearch ( &BranchAddress, m_Instructions.data(), m_Instructions.size(), sizeof ( uintptr_t ), Compare );

		if ( FoundInstance != NULL )
		{
			NextInstructionIndex = FoundInstance - ( uintptr_t* )m_Instructions.data();

			if ( m_Instructions[ NextInstructionIndex - 1 ] == CurrentInstructionAddress )
			{
				return true;
			}
		}
	}

	return false;
}
//=============================================================================================================
void CCondenser::ProcessFunction ( PVOID pvFunction )
{
	uintptr_t BranchIterator;

	uintptr_t BranchAddress, FunctionEnd, PseudoFunctionEnd;

	FunctionEnd = GetFunctionEnd ( ( uintptr_t )pvFunction );

	if ( m_BranchesSorted.size() != 0 )
	{
		BranchIterator = 0;

		for ( BranchIterator = 0; BranchIterator < m_Branches.size(); BranchIterator++ ) // start checking each block for a jump to unprocessed code
		{					
			BranchAddress = m_Branches[ BranchIterator ];

			if ( BranchAddress != 0 ) // we found a branching instruction, check the branch to see if it was processed or not
			{					
				if ( std::bsearch ( &BranchAddress, m_Instructions.data(), m_Instructions.size(), sizeof ( uintptr_t ), Compare ) == NULL )
				{
					PseudoFunctionEnd = GetFunctionEnd ( BranchAddress ); // process it

					if ( PseudoFunctionEnd == 0 )
					{
						return;
					}
																
					if ( std::bsearch ( &BranchAddress, m_BranchesSorted.data(), m_BranchesSorted.size(), sizeof ( uintptr_t ), Compare ) == NULL ) 
					{
						m_Branches.push_back ( BranchAddress );
					}

					if ( m_Instructions.size() >= m_MaxBytes )
					{
						break;
					}
				}
			}
		}
	}

	RemoveDuplicateBranches();
	RemoveDuplicateInstructions();
	RemoveDuplicateBlocks();
}
//=============================================================================================================
size_t CCondenser::CleanupAndCopyCode ( PVOID pvFunctionBegin )
{
	PBYTE pbFixedFunction;

	size_t TotalLength = 0;

	std::vector<uintptr_t>::iterator InstructionIterator;

	if ( m_FixedFunction != NULL )
	{
		pbFixedFunction = m_FixedFunction;
		
		// we have already eliminated some of the dead code, but now we must clean up our branch list so
		// we can eliminate branches that simply advance to the next EIP in the code section, rel = 0

		for ( uintptr_t Iterator = 0; Iterator < m_Instructions.size(); Iterator++ )
		{
			m_Length = GetInstructionLength( m_Instructions[ Iterator ] );
			
			if ( m_Length != -1 )
			{
				if ( m_Instructions[ Iterator ] == ( uintptr_t )pvFunctionBegin )
				{
					m_FunctionOut = ( uintptr_t )pbFixedFunction; // set the true out
				}

				if ( ShouldSkipJump ( m_Instructions[ Iterator ] ) != 0 || ShouldSkipInstr() )
				{					
					InstructionIterator = m_Instructions.begin();

					std::advance ( InstructionIterator, Iterator );
			
					m_Instructions.erase ( InstructionIterator );

					Iterator -= 1;
				}
				else
				{
					m_CopiedInstructions.push_back ( ( uintptr_t )pbFixedFunction );

					memcpy ( pbFixedFunction, ( PVOID )m_Instructions [ Iterator ], m_Length );
					
					pbFixedFunction += m_Length;

					TotalLength += m_Length;
				}
			}
		}
	}

	return TotalLength;
}
//=============================================================================================================
void CCondenser::CopyPartials ( PVOID pvFunctionBegin )
{
	size_t Length = 0;

	if ( m_Blocks.size() != 0 )
	{			
		m_Partials = new PBYTE [ m_Blocks.size() ];
						
		if ( m_Partials != NULL )
		{
			for ( uintptr_t Iterator = 0; Iterator < m_Blocks.size(); Iterator++ ) // now we can copy the partials
			{
				Length = m_Blocks[ Iterator ].BlockEnd - m_Blocks[ Iterator ].BlockStart;

				Length += GetInstructionLength ( m_Blocks[ Iterator ].BlockEnd );

				m_Partials [ m_PartialCount ] = new BYTE [ Length ];
				
				if ( m_Partials [ m_PartialCount ] == NULL )
				{
					break;
				}

				if ( m_Blocks[ Iterator ].BlockStart == ( uintptr_t )pvFunctionBegin )
				{
					m_FunctionBegin = ( uintptr_t )pvFunctionBegin;
				}
				
				memcpy ( m_Partials [ m_PartialCount++ ], ( PVOID )m_Blocks[ Iterator ].BlockStart, Length );
			}
		}
	}
}
//=============================================================================================================
void CCondenser::ClearPartials ( void )
{
	if ( m_Partials != NULL )
	{
		for ( uintptr_t Iterator = 0; Iterator < m_PartialCount; Iterator++ )
		{
			if ( m_Partials [ Iterator ] != NULL )
			{
				delete m_Partials [ Iterator ];
			}
		}

		delete m_Partials;

		m_Partials = NULL;

		m_PartialCount = 0;
	}
}
//=============================================================================================================
size_t CCondenser::GetFunctionLength ( PVOID pvFunctionBegin, bool FixupRelatives )
{
	size_t TotalLength;

	m_Blocks.clear();
	m_Instructions.clear();
	m_JumpTables.clear();
	m_Branches.clear();
	m_InstructionRVA.clear();
	m_CopiedInstructions.clear();
	m_InstructionBackup.clear();
	m_BranchesSorted.clear();
		
	ClearPartials();
	
	ProcessFunction ( pvFunctionBegin );

	TotalLength = CleanupAndCopyCode ( pvFunctionBegin );

	if ( FixupRelatives )
	{
		m_InFixup = true;

		RepairJumpsAndCalls ( TotalLength );

		m_InFixup = false;
	}
	
	CopyPartials ( pvFunctionBegin );
		
	return TotalLength;
}
//=============================================================================================================
void CCondenser::RepairJumpsAndCalls ( uintptr_t FunctionLength )
{
	bool SpecialCase = false;

	uintptr_t Relative = 0;

	uintptr_t BranchAddress = 0, CopyBuffer = 0, Instruction = 0;

	int OldLength = 0;

	uintptr_t CurrentInstruction = 0, Offset  = 0, Scale = 0;

	uintptr_t* FoundInstance;
	
	std::vector<uintptr_t>::iterator InstructionIterator;

	std::sort ( m_InstructionBackup.begin(), m_InstructionBackup.end() );

	unsigned char* FunctionBuffer = m_FixedFunction;
	
	for ( uintptr_t Iterator = 0; Iterator < m_Instructions.size(); Iterator++ )
	{
		CopyBuffer = m_CopiedInstructions [ Iterator ];

		Instruction = m_Instructions [ Iterator ];

		m_Length = GetInstructionLength ( Instruction );

		BranchAddress = GetBranchAddressWrapper ( &m_Disasm );

		if ( BranchAddress != 0 )
		{
			if ( m_Length == -1 )
			{
				return;
			}

			Offset = 0;

			if ( m_Disasm.Prefix0F == 0 )
			{
				switch ( m_Disasm.Opcode )
				{
					case INSTR_RELCALL:
					case INSTR_RELJMP:
					case INSTR_RELJCX:
					case INSTR_SHORTJMP:
					{
						Offset = 1;
					}
					break;
														
					default:
					{
						if ( m_Disasm.Opcode >= INSTR_SHORTJCC_BEGIN && m_Disasm.Opcode <= INSTR_SHORTJCC_END
							 || m_Disasm.Opcode >= INSTR_LOOPNZ && m_Disasm.Opcode <= INSTR_RELJCX )
						{
							Offset = 1;
						}
					}
				}
			}
			else
			{
				if ( m_Disasm.Opcode >= INSTR_JCC_BEGIN && m_Disasm.Opcode <= INSTR_JCC_END )
				{
					Offset = 2;
				}
			}
			if ( Offset != 0 )
			{
				FoundInstance = ( uintptr_t* )std::bsearch ( &BranchAddress, m_Instructions.data(), m_Instructions.size(), sizeof ( uintptr_t ), Compare );

				if ( FoundInstance != NULL )
				{
					CurrentInstruction = FoundInstance - ( uintptr_t* )m_Instructions.data();
					
					BranchAddress = m_CopiedInstructions [ CurrentInstruction ];
				}
				else
				{
					if ( m_FollowCalls != false )
					{
						OldLength = m_Length;

						FoundInstance = ( uintptr_t* )std::bsearch ( &BranchAddress, m_InstructionBackup.data(), m_InstructionBackup.size(), sizeof ( uintptr_t ), Compare );

						if ( FoundInstance != NULL )
						{
							CurrentInstruction = FoundInstance - ( uintptr_t* )m_InstructionBackup.data();

							BranchAddress = ( uintptr_t )GetBranchAddressWrapper ( &m_Disasm );

							if ( BranchAddress != 0 )
							{		
								FoundInstance = ( uintptr_t* )std::bsearch ( &BranchAddress, m_Instructions.data(), m_Instructions.size(), sizeof ( uintptr_t ), Compare );

								if ( FoundInstance != NULL )
								{
									CurrentInstruction = FoundInstance - ( uintptr_t* )m_Instructions.data();
					
									BranchAddress = m_CopiedInstructions [ CurrentInstruction ];
								}
							}
						}

						m_Length = OldLength;
					}
				}
				
				Relative = BranchAddress - CopyBuffer - m_Length;

				if ( m_Length == 2 )
				{
					*( BYTE* )( CopyBuffer + Offset ) = ( BYTE )Relative;
				}
				else
				{
					*( DWORD* )( CopyBuffer + Offset ) = ( DWORD )Relative;
				}
			}
		}
	}
}
//=============================================================================================================
void CCondenser::SetRegionInformation ( uintptr_t RegionBase, uintptr_t RegionSize )
{
	m_RegionBase = RegionBase;
	m_RegionSize = RegionSize;
	m_RegionEnd = RegionBase + RegionSize;
}
//=============================================================================================================
size_t CCondenser::GetFunctionBeginPartialIndex()
{
	for ( size_t Iterator = 0; Iterator < m_Blocks.size(); Iterator++ )
	{
		if ( m_Blocks[ Iterator ].BlockStart == m_FunctionBegin )
		{
			return Iterator;
		}
	}

	return 0;
}
//=============================================================================================================