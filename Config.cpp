//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "Config.h"
#include "Core.h"
//==================================================================================================================================
CConfig::CConfig ( char* FileName )
{
	using namespace std;

	char Directory [ MAX_PATH ];
	
	FileConfigStream.open ( FileName );

	if ( FileConfigStream.is_open() == false )
	{
		FailedToOpenFile = true;
	}

	if ( GetCurrentDirectoryA ( MAX_PATH, Directory ) )
	{
		CurrentDirectory = Directory;
	}
}
//==================================================================================================================================
CConfig::~CConfig()
{
	
}
//==================================================================================================================================
bool CConfig::ExecuteConfig()
{
	bool Success = true;

	using namespace std;
	
	size_t LineCounter = 1;

	string Field;
	string FileLine;

	vector<string> Fields;

	stringstream LineToProcess;
	
	while ( getline ( FileConfigStream, FileLine ) )
	{
		LineToProcess.str ( FileLine );

		while ( getline ( LineToProcess, Field, ' ' ) )
		{
			Fields.push_back ( Field );		
		}

		if ( Fields.size() != 0 )
		{
			if ( !ProcessLine ( Fields, LineCounter ) )
			{
				Success = false;
			}
		}

		LineCounter++;

		Fields.clear();
		LineToProcess.clear();
	}

	if ( FailedToOpenFile == false )
	{
		FileConfigStream.close();
	}

	return Success;
}
//==================================================================================================================================
bool CConfig::ProcessLine ( std::vector<std::string>& Line, size_t LineCounter )
{
	using namespace std;

	bool FoundBinary = false, ReturnValue = true;

	HANDLE ReferenceBinaryHandle;

	MemoryLocation_t MemoryLocation;

	memset ( &MemoryLocation, 0, sizeof ( MemoryLocation_t ) );

	std::string FilePath;

	ReferenceBinary_t ReferenceBinary;

	if ( Line[0] == "setoutputpath" )
	{
		OutputPath = Line[1];
	}
	else if ( Line[0] == "using" )
	{
		BinaryToLoad = Core.ReturnSteamPath();

		for ( size_t Iterator = 0; Iterator < Core.GetTrackedFilesInformation().size(); Iterator++ )
		{
			if ( Line[1] == Core.GetTrackedFilesInformation()[ Iterator ].TrackedPrefix )
			{
				ReferenceBinaryDirectory = Core.GetTrackedFilesInformation()[ Iterator ].TrackedPrefix;

				BinaryToLoad += Core.GetTrackedFilesInformation()[ Iterator ].Path;

				FoundBinary = true;

				break;
			}
		}

		if ( FoundBinary == false )
		{
			cout << "Invalid tracked prefix, please check TrackedFiles.ini." << endl;

			ReturnValue = false;
		}
	}
	else if ( Line[0] == "setglobalname" )
	{
		GlobalName = Line[1];
	}
	else if ( Line[0] == "setoutputpath" )
	{
		OutputPath = Line[1];
	}
	else if ( Line[0] == "addreferencebinary" )
	{
		FilePath = CurrentDirectory;

		FilePath += "\\";

		FilePath += ReferenceBinaryDirectory;

		FilePath += "\\";

		FilePath += Line[1];

		ReferenceBinaryHandle = CreateFileA ( FilePath.c_str(), GENERIC_READ, FILE_SHARE_READ, 
							NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );

		if ( ReferenceBinaryHandle == INVALID_HANDLE_VALUE )
		{
			cout << "Non existing reference binary on line " << LineCounter << " ." << endl;
			cout << "Did you forget to specify the using keyword at the start?" << endl;
			cout << "Or do you even have reference binaries in the tracked diectory?" << endl;

			ReturnValue = false;
		}
		else
		{
			ReferenceBinary.ReferenceBinaryPath = FilePath;

			ReferenceBinaries.push_back ( ReferenceBinary );

			CloseHandle ( ReferenceBinaryHandle );
		}
	}
	else if ( Line[0] == "var_declare" )
	{
		ConfigEntries.push_back ( MemoryLocation );
	}
	else
	{
		if ( ConfigEntries.size() != 0 )
		{
			if ( Line[0] == "var_setname" )
			{
				ConfigEntries.back().EntryName = Line[1];
			}
			else if ( Line[0] == "var_setrva" )
			{
				ConfigEntries.back().RVA = stoul ( Line[1], nullptr, 16 );
			}
			else if ( Line[0] == "var_setreferencebinaryindex" )
			{
				ConfigEntries.back().ReferenceBinaryIndex = stoul ( Line[1], nullptr, 10 );

				if ( ConfigEntries.back().ReferenceBinaryIndex > ReferenceBinaries.size() )
				{
					cout << "Out of bounds reference binary index on line " << LineCounter << endl;

					ReturnValue = false;
				}
			}
			else if ( Line[0] == "var_setrange" )
			{
				ConfigEntries.back().GetRange = true;

				ConfigEntries.back().Range1 = stol ( Line[1], nullptr, 10 );
				ConfigEntries.back().Range2 = stol ( Line[2], nullptr, 10 );
			}
			else if ( Line[0] == "var_searchstring" )
			{
				ConfigEntries.back().StringToSearchFor = Line[1];
			}
			else if ( Line[0] == "var_usecondenser" )
			{
				ConfigEntries.back().UseCondenser = true;
			}
			else if ( Line[0] == "var_stringisreferenced" )
			{
				ConfigEntries.back().IsReferenced = true;
			}
			else if ( Line[0] == "var_searchbackward" )
			{
				ConfigEntries.back().SearchEntryName1 = Line[1];

				ConfigEntries.back().Occurance1 = stol ( Line[2], nullptr, 10 );
				
				ConfigEntries.back().SearchBackward = true;
			}
			else if ( Line[0] == "var_searchforward" )
			{
				ConfigEntries.back().SearchEntryName1 = Line[1];

				ConfigEntries.back().Occurance1 = stoul ( Line[2], nullptr, 10 );

				ConfigEntries.back().SearchForward = true;
			}
			else if ( Line[0] == "var_getrvatodisplacement" )
			{
				ConfigEntries.back().RVAToDisplacement = true;
			}
			else if ( Line[0] == "var_getrvatoimmediate" )
			{
				ConfigEntries.back().RVAToImmediate = true;
			}
			else if ( Line[0] == "var_getrvatorelative" )
			{
				ConfigEntries.back().RVAToRelative = true;
			}
			else if ( Line[0] == "var_retrievedisplacement" )
			{
				ConfigEntries.back().GetDisplacement = true;

				ConfigEntries.back().RetrieveDisplacement = stol ( Line[1], nullptr, 10 );
			}
			else if ( Line[0] == "var_retrieveimmediate" )
			{
				ConfigEntries.back().GetImmediate = true;

				ConfigEntries.back().RetrieveImmediate = stol ( Line[1], nullptr, 10 );
			}
			else if ( Line[0] == "var_retrieverelative" )
			{
				ConfigEntries.back().GetRelative = true;

				ConfigEntries.back().RetrieveRelative = stol ( Line[1], nullptr, 10 );
			}
			else if ( Line[0] == "var_setpatternsize" )
			{
				ConfigEntries.back().PatternSize = stoul ( Line[1], nullptr, 16 );
			}
			else if ( Line[0] == "var_findalloccurances" )
			{
				ConfigEntries.back().FindAllOccurances = true;
			}
			else if ( Line[0] == "var_markasvtable" )
			{
				ConfigEntries.back().MarkedAsVTable = true;

				if ( Line[1] == "disp" )
				{
					ConfigEntries.back().MarkAsVTable = USE_DISP;
				}
				else if ( Line[1] == "imm" )
				{
					ConfigEntries.back().MarkAsVTable = USE_IMM;
				}
				else
				{
					ConfigEntries.back().MarkAsVTable = NONE;
				}
			}
			else if ( Line[0] == "var_trackvtableentries" )
			{
				VTable_Entry_t VTable;

				for ( size_t Iterator = 1; Iterator < Line.size(); Iterator += 3  )
				{
					memset ( &VTable, 0, sizeof ( VTable_Entry_t ) );

					VTable.VTableName = Line[ Iterator ];
					VTable.Index = stol ( Line[ Iterator + 1 ], nullptr, 10 );
					VTable.PatternSize = stoul ( Line[ Iterator + 2 ], nullptr, 16 );

					ConfigEntries.back().VTableEntries.push_back ( VTable );
				}
			}
			else if ( Line[0] == "var_neutralize" )
			{
				for ( size_t Iterator = 1; Iterator < Line.size(); Iterator++ )
				{
					if ( Line [ Iterator ] == "disp8" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_8BIT_DISP;
					}
					if ( Line [ Iterator ] == "disp16" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_16BIT_DISP;
					}
					if ( Line [ Iterator ] == "disp32" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_32BIT_DISP;
					}
					if ( Line [ Iterator ] == "disp64" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_64BIT_DISP;
					}
					if ( Line [ Iterator ] == "imm8" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_8BIT_IMM;
					}
					if ( Line [ Iterator ] == "imm16" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_16BIT_IMM;
					}
					if ( Line [ Iterator ] == "imm32" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_32BIT_IMM;
					}
					if ( Line [ Iterator ] == "imm64" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_64BIT_IMM;
					}
					if ( Line [ Iterator ] == "rel8" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_8BIT_REL;
					}
					if ( Line [ Iterator ] == "rel16" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_16BIT_REL;
					}
					if ( Line [ Iterator ] == "rel32" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_32BIT_REL;
					}
					if ( Line [ Iterator ] == "reg32" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_REGISTERS_32;
					}
					if ( Line [ Iterator ] == "reg64" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_REGISTERS_64;
					}
					if ( Line [ Iterator ] == "relocations" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_RELOCATIONS;
					}
					if ( Line [ Iterator ] == "everything" )
					{
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_8BIT_DISP;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_16BIT_DISP;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_32BIT_DISP;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_64BIT_DISP;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_8BIT_IMM;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_16BIT_IMM;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_32BIT_IMM;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_64BIT_IMM;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_8BIT_REL;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_16BIT_REL;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_32BIT_REL;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_REGISTERS_32;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_REGISTERS_64;
						ConfigEntries.back().NeutralizeFlags |= NEUTRALIZE_RELOCATIONS;
					}
				}
			}
		}
	}

	return ReturnValue;
}
//=============================================================================================================
const char* CConfig::ReturnBinaryToLoad ( void )
{
	return BinaryToLoad.c_str();
}
//==================================================================================================================================