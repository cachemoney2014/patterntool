//=============================================================================================================
#define MAX_BLOCKS						0x2000
#define MAX_BRANCHES					0x8000
#define MAX_INSTRUCTIONS				0x40000
//=============================================================================================================
#define MAX_INSTRUCTION_SIZE			16
//=============================================================================================================
class CCondenser
{
	public:

		CCondenser ( bool FollowCalls, uintptr_t DisasmMode, size_t AllocationSize );
		~CCondenser();
		
		BOOL ShouldSkipJump();
		BOOL ShouldSkipJump ( uintptr_t CurrentInstructionAddress );
		BOOL ShouldSkipInstr();

		BOOL IsEndPoint ( uintptr_t CurrentBlock );
		size_t GetFunctionLength ( PVOID pvFunctionBegin, bool FixupRelatives );

		int GetInstructionLength ( uintptr_t Address );

		PBYTE GetCopiedFunctionBuffer ( PVOID pvFunctionBegin );

		PBYTE GetFunctionBuffer();

		uintptr_t GetSizeOfBlock ( uintptr_t CurrentBlock );
		uintptr_t GetFunctionEnd ( uintptr_t Address );
		uintptr_t GetBranchListFromBlock ( uintptr_t CurrentBlock );
		uintptr_t GetBranchAddressWrapper ( LDEDisasm_t* pDisasm );

		void ProcessFunction ( PVOID pvFunction );

		void RemoveDuplicateBasicBlocks();
		void RemoveDuplicateBlocks();
		void RemoveDuplicateBranches();
		void RemoveDuplicateInstructions();
		void RemoveDuplicateInstructionDataEntries();
		
		void RepairJumpsAndCalls ( uintptr_t FunctionLength );

		void SetRegionInformation ( uintptr_t RegionBase, uintptr_t RegionSize );

		DWORD_PTR GetFunctionOut() { return m_FunctionOut; }

		size_t GetPartialCount() { return m_PartialCount; }

		size_t GetFunctionBeginPartialIndex();

		PBYTE GetPartial ( size_t uiIndex );
		size_t GetLengthOfPartial ( size_t uiIndex );

		void SetMaxBytesToCopy ( size_t MaxBytes ) { m_MaxBytes = MaxBytes; }

		size_t CleanupAndCopyCode ( PVOID pvFunctionBegin );
		void CopyPartials ( PVOID pvFunctionBegin );
		
		void ClearPartials ( void );

	private:

		bool m_FollowCalls, m_InFixup, m_StartedTracking;

		uintptr_t m_FunctionOut;

		uintptr_t m_RegionBase;
		uintptr_t m_RegionSize;
		uintptr_t m_RegionEnd;

		int m_Length;

		PBYTE* m_Partials;
		
		PBYTE m_FixedFunction;
		
		uintptr_t m_PreviousBranch;
		uintptr_t m_BlockEnd;

		uintptr_t m_FunctionBegin;

		size_t m_MaxAllocationSize, m_MaxBytes, m_PartialCount;

		std::vector<Block_t> m_Blocks;

		std::vector<uintptr_t> m_Branches;
		std::vector<uintptr_t> m_ConditionalBranches;
		std::vector<uintptr_t> m_BranchRVA;
		std::vector<uintptr_t> m_CopiedInstructions;
		std::vector<JumpTable_t> m_JumpTables;
		std::vector<uintptr_t> m_Instructions;
		std::vector<uintptr_t> m_InstructionRVA;
		std::vector<uintptr_t> m_InstructionBackup;
		std::vector<uintptr_t> m_InvalidAddresses;
		std::vector<uintptr_t> m_BranchesSorted;

		uintptr_t m_DisasmMode;

		LDEDisasm_t m_Disasm;
};
//=============================================================================================================