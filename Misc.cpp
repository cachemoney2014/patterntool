//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "LDE.h"
//=============================================================================================================
#include "Common.h"
#include "Config.h"
//=============================================================================================================
int __cdecl compare(const void *ap, const void *bp)
{
    const uintptr_t *a = (uintptr_t *) ap;
    const uintptr_t *b = (uintptr_t *) bp;
    if(*a < *b)
        return -1;
    else if(*a > *b)
        return 1;
    else
        return 0;
}
//=============================================================================================================
BYTE GetNeutralizedOpcode32 ( LDEDisasm_t* pDisasm )
{
	BYTE Opcode = pDisasm->Opcode;

	if ( pDisasm->OpcodeFlags == 'Z' )
	{
		if ( ( Opcode & 0x0F ) < 9 )
		{
			Opcode &= 0xF0;
		}
		else
		{
			Opcode &= 0xF8;
		}
	}

	if ( pDisasm->HasDirectionFlag )
	{
		Opcode &= 0xFD; // strip direction flag
	}

	return Opcode;
}
//=============================================================================================================
BYTE GetNeutralizedModRM ( LDEDisasm_t* pDisasm )
{
	BYTE NeutralizedModRM = 0;

	NeutralizedModRM = pDisasm->ModRM & 0xC0;

	if ( pDisasm->Flags & FLAG_SIB || pDisasm->ModRM_Mod == 0 && pDisasm->ModRM_RM == 5 )
	{
		NeutralizedModRM |= pDisasm->ModRM_RM;
	}
		
	return NeutralizedModRM;
}
//=============================================================================================================
BYTE GetNeutralizedSIB ( LDEDisasm_t* pDisasm )
{
	BYTE NeutralizedSIB = 0;

	NeutralizedSIB = pDisasm->SIB & 0xC0;

	if ( pDisasm->SIB_Base == 5 )
	{
		NeutralizedSIB |= pDisasm->SIB_Base;
	}
		
	return NeutralizedSIB;
}
//=============================================================================================================
void Assemble32 ( PBYTE Output, LDEDisasm_t* pDisasm, size_t NeutralizeFlags, std::vector<uintptr_t>& RelocationBlock )
{
	if ( pDisasm->Flags & FLAG_PREFIX_OVERRIDE_SIZE )
	{
		*Output++ = pDisasm->PrefixOperandSizeOverride;
	}
	if ( pDisasm->Flags & FLAG_PREFIX_OVERRIDE_ADDRESS ) 
	{	
		*Output++ = pDisasm->PrefixAddressSizeOverride;
	}
	if ( pDisasm->Flags & FLAG_PREFIX_LOCK )
	{
		*Output++ = pDisasm->PrefixLock;
	}
	if ( pDisasm->Flags & FLAG_PREFIX_REPEAT )
	{
		*Output++ = pDisasm->PrefixRepeat;
	}
	if ( pDisasm->Flags & FLAG_PREFIX_SEGMENT )
	{
		*Output++ = pDisasm->PrefixSegment;
	}
	if ( pDisasm->Flags & FLAG_PREFIX_WAIT )
	{
		*Output++ = pDisasm->PrefixWait;
	}
	if ( pDisasm->Flags & FLAG_PREFIX_0F )
	{
		*Output++ = pDisasm->Prefix0F;
	}

	if ( pDisasm->Flags & FLAG_3DNOW )
	{
		*Output++ = pDisasm->Prefix3DNow;

		if ( pDisasm->Flags & FLAG_MODRM )
		{
			if ( NeutralizeFlags & NEUTRALIZE_REGISTERS_32 )
			{
				*Output++ = GetNeutralizedModRM ( pDisasm );
			}
			else
			{
				*Output++ = pDisasm->ModRM;
			}
		}
		if ( pDisasm->Flags & FLAG_SIB )
		{
			if ( NeutralizeFlags & NEUTRALIZE_REGISTERS_32 )
			{
				*Output++ = GetNeutralizedSIB ( pDisasm );
			}
			else
			{
				*Output++ = pDisasm->SIB;
			}
		}

		*Output++ = GetNeutralizedOpcode32 ( pDisasm );
	}
	else
	{
			
		*Output++ = GetNeutralizedOpcode32 ( pDisasm );
	
		if ( pDisasm->Flags & FLAG_OPCODE2 )
		{		
			*Output++ = pDisasm->Opcode2;
		}
		if ( pDisasm->Flags & FLAG_MODRM )
		{
			if ( NeutralizeFlags & NEUTRALIZE_REGISTERS_32 )
			{
				*Output++ = GetNeutralizedModRM ( pDisasm );
			}
			else
			{
				*Output++ = pDisasm->ModRM;
			}
		}
		if ( pDisasm->Flags & FLAG_SIB )
		{
			if ( NeutralizeFlags & NEUTRALIZE_REGISTERS_32 )
			{
				*Output++ = GetNeutralizedSIB ( pDisasm );
			}
			else
			{
				*Output++ = pDisasm->SIB;
			}
		}
		if ( pDisasm->Flags & FLAG_DISP8 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_8BIT_DISP )
			{
				*Output++ = 0xCC;
			}
			else
			{
				*Output++ = pDisasm->Displacement.Displacement8;
			}
		}
		else if ( pDisasm->Flags & FLAG_DISP16 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_16BIT_DISP )
			{
				memset ( Output, 0xCC, sizeof ( uint16_t ) );
			}
			else
			{
				memcpy ( Output, &pDisasm->Displacement.Displacement16, sizeof ( uint16_t ) );
			}
		
			Output += sizeof ( uint16_t );
		}
		else if ( pDisasm->Flags & FLAG_DISP32 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_32BIT_DISP || NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
			{
				if ( NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
				{
					if ( std::bsearch ( &pDisasm->Displacement.Displacement32, RelocationBlock.data(), RelocationBlock.size(), sizeof ( DWORD_PTR ), compare ) )
					{
						memset ( Output, 0xCC, sizeof ( uint32_t ) );
					}
				}
				if ( NeutralizeFlags & NEUTRALIZE_32BIT_DISP )
				{
					memset ( Output, 0xCC, sizeof ( uint32_t ) );
				}
			}
			else
			{
				memcpy ( Output, &pDisasm->Displacement.Displacement32, sizeof ( uint32_t ) );
			}
		
			Output += sizeof ( uint32_t );
		}
		if ( pDisasm->Flags & FLAG_IMM8 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_8BIT_IMM )
			{
				*Output++ = 0xCC;
			}
			else
			{
				*Output++ = pDisasm->Immediate.Immediate8;
			}
		}
		else if ( pDisasm->Flags & FLAG_IMM16 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_16BIT_IMM )
			{
				memset ( Output, 0xCC, sizeof ( uint32_t ) );
			}
			else
			{
				memcpy ( Output, &pDisasm->Immediate.Immediate16, sizeof ( uint16_t ) );
			}
		
			Output += sizeof ( uint16_t );
		}
		else if ( pDisasm->Flags & FLAG_IMM32 )
		{	
			if ( NeutralizeFlags & NEUTRALIZE_32BIT_IMM || NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
			{
				if ( NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
				{
					if ( std::bsearch ( &pDisasm->Immediate.Immediate32, RelocationBlock.data(), RelocationBlock.size(), sizeof ( DWORD_PTR ), compare ) )
					{
						memset ( Output, 0xCC, sizeof ( uint32_t ) );
					}
				}
				if ( NeutralizeFlags & NEUTRALIZE_32BIT_IMM  )
				{
					memset ( Output, 0xCC, sizeof ( uint32_t ) );
				}
			}
			else
			{
				memcpy ( Output, &pDisasm->Immediate.Immediate32, sizeof ( uint32_t ) );
			}

			Output += sizeof ( uint32_t );
		}
		if ( pDisasm->Flags & FLAG_IMM8_2 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_8BIT_IMM )
			{
				*Output++ = 0xCC;
			}
			else
			{
				*Output++ = pDisasm->Immediate2.Immediate8;
			}
		}
		if ( pDisasm->Flags & FLAG_REL8 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_8BIT_REL || NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
			{
				*Output++ = 0xCC;
			}
			else
			{
				*Output++ = pDisasm->Relative.Relative8;
			}
		}
		else if ( pDisasm->Flags & FLAG_REL16 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_16BIT_REL || NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
			{					
				memset ( Output, 0xCC, sizeof ( uint16_t ) );
				
			}
			else
			{
				memcpy ( Output, &pDisasm->Relative.Relative16, sizeof ( uint16_t ) );
			}

			Output += sizeof ( uint16_t );
		}
		else if ( pDisasm->Flags & FLAG_REL32 )
		{
			if ( NeutralizeFlags & NEUTRALIZE_32BIT_REL || NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
			{
				memset ( Output, 0xCC, sizeof ( uint32_t ) );
			}
			else
			{
				memcpy ( Output, &pDisasm->Relative.Relative32, sizeof ( uint32_t ) );
			}

			Output += sizeof ( uint32_t );
		}
		else if ( pDisasm->Flags & FLAG_SEGMENT_REL )
		{
			if ( NeutralizeFlags & NEUTRALIZE_16BIT_REL || NeutralizeFlags & NEUTRALIZE_RELOCATIONS )
			{					
				memset ( Output, 0xCC, sizeof ( uint16_t ) );
				
			}
			else
			{
				memcpy ( Output, &pDisasm->Segment, sizeof ( uint16_t ) );
			}

			Output += sizeof ( uint16_t );
		}
	}

	return;
}
//=============================================================================================================
void CreatePattern ( PBYTE Signature, size_t SizeOfSignature, size_t NeutralizeFlags, std::vector<uintptr_t>& RelocationBlock, int Mode )
{
	PBYTE CurrentPointer;

	size_t Position;

	int CurrentLength;
	
	CurrentPointer = Signature;

	Position = 0;

	LDEDisasm_t Disasm;

	do
	{		
		memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

		CurrentLength = LDEDisasm ( &Disasm, CurrentPointer, Mode );

		if ( CurrentLength != -1 )
		{								
			if ( Mode == MODE_32 )
			{
				Assemble32 ( CurrentPointer, &Disasm, NeutralizeFlags, RelocationBlock );
			}

			CurrentPointer += CurrentLength;

			Position += CurrentLength;
		}
		else
		{
			Position++;

			CurrentPointer++;
		}
				
	}while ( Position < SizeOfSignature );
}
//=============================================================================================================
size_t GetAdjustedSignatureSize ( PBYTE Signature, size_t SizeOfSignature, int Mode )
{
	PBYTE CurrentPointer;

	size_t Position;

	int CurrentLength;
	
	CurrentPointer = Signature;

	Position = 0;

	LDEDisasm_t Disasm;

	do
	{		
		memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

		CurrentLength = LDEDisasm ( &Disasm, CurrentPointer, Mode );

		if ( CurrentLength != -1 )
		{
			Position += CurrentLength;

			CurrentPointer += CurrentLength;
		}
		else
		{
			Position++;

			CurrentPointer++;
		}
				
	}while ( Position < SizeOfSignature );

	return Position;
}
//=============================================================================================================