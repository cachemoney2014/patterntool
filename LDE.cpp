//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "LDE.h"
//=============================================================================================================
int LDEDisasm ( _Inout_ LDEDisasm_t* pDisasm, _In_ unsigned char* Address, _In_ int Mode )
{
	bool FinishedPrefixes = false;

	unsigned char* CurrentPointer = Address, CurrentValue = 0;

	int Length = 0;

	pDisasm->CurrentPointer = ( uintptr_t )Address;

	for ( int Iterator = MAX_INSTRUCTION_SIZE; Iterator > 0, FinishedPrefixes != true; Iterator-- ) // gather prefixes
	{
		switch ( CurrentValue = *CurrentPointer++ )
		{
			case PREFIX_SEGMENT_CS:
			case PREFIX_SEGMENT_SS:
			case PREFIX_SEGMENT_DS:
			case PREFIX_SEGMENT_ES:
			case PREFIX_SEGMENT_FS:
			case PREFIX_SEGMENT_GS:
			{
				pDisasm->PrefixSegment = CurrentValue;

				if ( CurrentValue == PREFIX_SEGMENT_ES )
				{
					pDisasm->Prefix26 = PREFIX_SEGMENT_ES;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_CS )
				{
					pDisasm->Prefix2E = PREFIX_SEGMENT_DS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_SS )
				{
					pDisasm->Prefix36 = PREFIX_SEGMENT_SS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_DS )
				{
					pDisasm->Prefix3E = PREFIX_SEGMENT_DS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_FS )
				{
					pDisasm->Prefix64 = PREFIX_SEGMENT_FS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_GS )
				{
					pDisasm->Prefix65 = PREFIX_SEGMENT_GS;
				}

				pDisasm->Flags |= FLAG_PREFIX_SEGMENT;
			}
			break;

			case PREFIX_OPERAND_SIZE:
			{
				pDisasm->Prefix66 = pDisasm->PrefixOperandSizeOverride = CurrentValue;

				pDisasm->Flags |= FLAG_PREFIX_OVERRIDE_SIZE;
			}
			break;

			case PREFIX_ADDRESS_SIZE:
			{
				pDisasm->Prefix67 = pDisasm->PrefixAddressSizeOverride = CurrentValue;

				pDisasm->Flags |= FLAG_PREFIX_OVERRIDE_ADDRESS;
			}
			break;

			case PREFIX_LOCK:
			{
				pDisasm->PrefixLock = CurrentValue;

				pDisasm->Flags |= FLAG_PREFIX_LOCK;
			}
			break;

			case PREFIX_REPX:
			case PREFIX_REPNZ:
			{
				pDisasm->PrefixRepeat = CurrentValue;

				if ( CurrentValue == PREFIX_REPNZ )
				{
					pDisasm->PrefixF2 = CurrentValue;
				}
				else
				{
					pDisasm->PrefixF3 = CurrentValue;
				}

				pDisasm->Flags |= FLAG_PREFIX_REPEAT;
			}
			break;

			case PREFIX_WAIT:
			{
				pDisasm->PrefixWait = CurrentValue;

				pDisasm->Prefix9B = CurrentValue;

				pDisasm->Flags |= FLAG_PREFIX_WAIT;
			}
			break;

			default:
			{
				FinishedPrefixes = true;
			}
			break;
		}
	}

	if ( Mode == MODE_32 )
	{
		LDEDisasm32 ( pDisasm, CurrentPointer, CurrentValue );
	}
	else if ( Mode == MODE_64 )
	{
		if ( ( *CurrentPointer & 0xF0 ) == PREFIX_REX )
		{
			pDisasm->REXPrefix = *CurrentPointer++;

			pDisasm->REX_W = ( ( pDisasm->REXPrefix & 0xF ) >> 3 );
			pDisasm->REX_R = ( ( pDisasm->REXPrefix & 7 ) >> 2 );
			pDisasm->REX_X = ( ( pDisasm->REXPrefix & 3 ) >> 1 );
			pDisasm->REX_B = pDisasm->REXPrefix & 1;

			pDisasm->Flags |= FLAG_PREFIX_REX;
		}

		LDEDisasm64 ( pDisasm, CurrentPointer, CurrentValue );
	}

	if ( pDisasm->Flags & FLAG_REL8 )
	{
		pDisasm->Relative.Relative32 = ( signed char )*CurrentPointer++;
	}
	else if ( pDisasm->Flags & FLAG_REL16 )
	{
		pDisasm->Relative.Relative32 = *( signed short* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned short );
	}
	else if ( pDisasm->Flags & FLAG_REL32 )
	{
		pDisasm->Relative.Relative32 = *( signed long* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned long );
	}
	if ( pDisasm->Flags & FLAG_SEGMENT_REL )
	{
		pDisasm->Segment = *( unsigned short* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned short );
	}

	if ( pDisasm->Flags & FLAG_IMM8 )
	{
		pDisasm->Immediate.Immediate64 = *CurrentPointer++;
	}
	else if ( pDisasm->Flags & FLAG_IMM16 )
	{
		pDisasm->Immediate.Immediate64 = *( unsigned short* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned short );
	}
	else if ( pDisasm->Flags & FLAG_IMM32 )
	{
		pDisasm->Immediate.Immediate64 = *( unsigned long* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned long );
	}
	else if ( pDisasm->Flags & FLAG_IMM64 )
	{
		pDisasm->Immediate.Immediate64 = *( unsigned long long* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned long long );
	}
	if ( pDisasm->Flags & FLAG_IMM8_2 )
	{
		pDisasm->Immediate2.Immediate64 = *CurrentPointer++;
	}

	Length = CurrentPointer - Address;

	pDisasm->Length = Length;
		
	return Length;
}
//=============================================================================================================
void LDEDisasm32 ( LDEDisasm_t* pDisasm, unsigned char*& CurrentPointer, unsigned char CurrentValue )
{
	bool ProcessModRM;

	if ( CurrentValue == OPCODE_TWO_BYTE )
	{
		CurrentValue = *CurrentPointer++;

		pDisasm->Flags |= FLAG_PREFIX_0F;

		if ( CurrentValue == PREFIX_3DNOW )
		{
			pDisasm->Flags |= FLAG_3DNOW;

			LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );

			// check last byte for opcode

			pDisasm->Opcode = *CurrentPointer++;
		}
		else
		{
			pDisasm->Opcode = CurrentValue;

			switch ( CurrentValue )
			{
				case 0x00:
				case 0x01:
				case 0x18:
				case 0x19:
				case 0x1A:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
				case 0x71:
				case 0x72:
				case 0x73:
				case 0x90:
				case 0x91:
				case 0x92:
				case 0x93:
				case 0x94:
				case 0x95:
				case 0x96:
				case 0x97:
				case 0x98:
				case 0x99:
				case 0x9A:
				case 0x9B:
				case 0x9C:
				case 0x9D:
				case 0x9E:
				case 0x9F:
				case 0xAE:
				case 0xBA:
				case 0xC7:
				{
					if ( CurrentValue >= 0x71 && CurrentValue <= 0x73 )
					{
						pDisasm->Flags |= FLAG_IMM8;
					}

					pDisasm->Flags |= FLAG_OPCEXT;
				}
				break;

				case 0x02:
				case 0x03:
				case 0x10:
				case 0x11:
				case 0x12:
				case 0x13:
				case 0x14:
				case 0x15:
				case 0x16:
				case 0x17:
				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x26:
				case 0x27:
				case 0x28:
				case 0x29:
				case 0x2A:
				case 0x2B:
				case 0x2C:
				case 0x2D:
				case 0x2E:
				case 0x2F:
				case 0x38:
				case 0x3A:
				case 0x40:
				case 0x41:
				case 0x42:
				case 0x43:
				case 0x44:
				case 0x45:
				case 0x46:
				case 0x47:
				case 0x48:
				case 0x49:
				case 0x4A:
				case 0x4B:
				case 0x4C:
				case 0x4D:
				case 0x4E:
				case 0x4F:
				case 0x50:
				case 0x51:
				case 0x52:
				case 0x53:
				case 0x54:
				case 0x55:
				case 0x56:
				case 0x57:
				case 0x58:
				case 0x59:
				case 0x5A:
				case 0x5B:
				case 0x5C:
				case 0x5D:
				case 0x5E:
				case 0x5F:
				case 0x60:
				case 0x61:
				case 0x62:
				case 0x63:
				case 0x64:
				case 0x65:
				case 0x66:
				case 0x67:
				case 0x68:
				case 0x69:
				case 0x6A:
				case 0x6B:
				case 0x6C:
				case 0x6D:
				case 0x6E:
				case 0x6F:
				case 0x70:
				case 0x74:
				case 0x75:
				case 0x76:
				case 0x78:
				case 0x79:
				case 0x7C:
				case 0x7D:
				case 0x7E:
				case 0x7F:
				case 0xA3:
				case 0xA4:
				case 0xA5:
				case 0xAB:
				case 0xAC:
				case 0xAD:
				case 0xAF:
				case 0xB0:
				case 0xB1:
				case 0xB2:
				case 0xB3:
				case 0xB4:
				case 0xB5:
				case 0xB6:
				case 0xB7:
				case 0xB8:
				case 0xBB:
				case 0xBC:
				case 0xBD:
				case 0xBE:
				case 0xBF:
				case 0xC0:
				case 0xC1:
				case 0xC2:
				case 0xC3:
				case 0xC4:
				case 0xC5:
				case 0xC6:
				case 0xD0:
				case 0xD1:
				case 0xD2:
				case 0xD3:
				case 0xD4:
				case 0xD5:
				case 0xD6:
				case 0xD7:
				case 0xD8:
				case 0xD9:
				case 0xDA:
				case 0xDB:
				case 0xDC:
				case 0xDD:
				case 0xDE:
				case 0xDF:
				case 0xE0:
				case 0xE1:
				case 0xE2:
				case 0xE3:
				case 0xE4:
				case 0xE5:
				case 0xE6:
				case 0xE7:
				case 0xE8:
				case 0xE9:
				case 0xEA:
				case 0xEB:
				case 0xEC:
				case 0xED:
				case 0xEE:
				case 0xEF:
				case 0xF0:
				case 0xF1:
				case 0xF2:
				case 0xF3:
				case 0xF4:
				case 0xF5:
				case 0xF6:
				case 0xF7:
				case 0xF8:
				case 0xF9:
				case 0xFA:
				case 0xFB:
				case 0xFC:
				case 0xFD:
				case 0xFE:
				{
					ProcessModRM = true;

					switch ( CurrentValue )
					{
						case 0x38:
						case 0x3A:
						{
							pDisasm->Opcode2 = *CurrentPointer++;
												
							if ( CurrentValue == 0x3A )
							{
								if ( pDisasm->Opcode2 >= 0x08 && pDisasm->Opcode2 <= 0x0F 
									|| pDisasm->Opcode2 >= 0x14 && pDisasm->Opcode2 <= 0x22
									|| pDisasm->Opcode2 == 0x42
									|| pDisasm->Opcode2 == 0x62
									|| pDisasm->Opcode2 == 0x63 )
								{
									pDisasm->Flags |= FLAG_IMM8;
								}
							}
						}
						break;

						case 0x70:
						{
							pDisasm->Flags |= FLAG_IMM8;
						}
						break;

						case 0x7A:
						case 0x7B:
						{

						}
						break;

						case 0xA4:
						case 0xA5:
						{							
							pDisasm->HasDirectionFlag = true;

							if ( CurrentValue == 0xA4 )
							{
								pDisasm->HasImmediate = true;

								pDisasm->Flags |= FLAG_IMM8;
							}
						}
						break;

						case 0xAB:
						{
							pDisasm->IsLockPrefixValid = true;
						}
						break;

						case 0xAC:
						case 0xAD:
						{
							if ( CurrentValue == 0xAC )
							{
								pDisasm->Flags |= FLAG_IMM8;
							}

							pDisasm->HasDirectionFlag = true;
						}
						break;
						
						case 0xB0: 
						case 0xB1:
						{
							pDisasm->IsPrefixOperandSizeOverideValid = true;

							pDisasm->IsLockPrefixValid = true;
						}
						break;

						case 0xB2:
						case 0xB4:
						case 0xB5:
						{				
							pDisasm->Flags |= FLAG_SEGMENT_REGISTER;
						}
						break;

						case 0xAF:
						case 0xB6:
						case 0xB7:
						case 0xBE:
						case 0xBF:
						{
							pDisasm->IsPrefixOperandSizeOverideValid = true;
							pDisasm->HasDirectionFlag = true;
						}
						break;

						case 0xB8:
						{
							if ( pDisasm->PrefixRepeat != PREFIX_REPX )
							{
								ProcessModRM = false;
							}
						}
						break;

						case 0xC0:
						case 0xC1:
						{
							pDisasm->IsLockPrefixValid = true;
							pDisasm->HasDirectionFlag = true;
						}
						break;

						case 0xC2:
						case 0xC3:
						case 0xC4:
						case 0xC5:
						case 0xC6:
						{
							pDisasm->Flags |= FLAG_IMM8;
						}
						break;
					}
					if ( ProcessModRM == true )
					{						
						pDisasm->Flags |= FLAG_MODRM;
					}
				}
				break;
					
				case 0x80:
				case 0x81:
				case 0x82:
				case 0x83:
				case 0x84:
				case 0x85:
				case 0x86:
				case 0x87:
				case 0x88:
				case 0x89:
				case 0x8A:
				case 0x8B:
				case 0x8C:
				case 0x8D:
				case 0x8E:
				case 0x8F:
				{
					pDisasm->HasRelative = true;

					pDisasm->IsPrefixOperandSizeOverideValid = true;
				
					if ( pDisasm->PrefixOperandSizeOverride )
					{				
						pDisasm->Flags |= FLAG_REL16;
					}
					else
					{				
						pDisasm->Flags |= FLAG_REL32;
					}
				}
				break;
				
				case 0xA0:
				case 0xA1:
				case 0xA8:
				case 0xA9:
				{
					pDisasm->Flags |= FLAG_SEGMENT_REGISTER;
				}
				break;
				
				case 0xC8:
				case 0xC9:
				case 0xCA:
				case 0xCB:
				case 0xCC:
				case 0xCD:
				case 0xCE:
				case 0xCF:
				{
					pDisasm->Register = CurrentValue & 0x07;
				}
				break;
			}
		}
	}
	else
	{
		pDisasm->Opcode = CurrentValue;

		switch ( CurrentValue )
		{
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x03:
			case 0x08:
			case 0x09:
			case 0x0A:
			case 0x0B:
			case 0x10:
			case 0x11:
			case 0x12:
			case 0x13:
			case 0x18:
			case 0x19:
			case 0x1A:
			case 0x1B:
			case 0x20:
			case 0x21:
			case 0x22:
			case 0x23:
			case 0x28:
			case 0x29:
			case 0x2A:
			case 0x2B:
			case 0x30:
			case 0x31:
			case 0x32:
			case 0x33:
			case 0x38:
			case 0x39:
			case 0x3A:
			case 0x3B:
			{				
				switch ( CurrentValue & 0x07 )
				{
					case 0x00:
					case 0x01:
					{
						pDisasm->IsLockPrefixValid = true;
					}
					break;
				}

				pDisasm->HasDirectionFlag = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_MODRM;
			}
			break;


			case 0x04:
			case 0x05:
			case 0x0C:
			case 0x0D:
			case 0x14:
			case 0x15:
			case 0x1C:
			case 0x1D:
			case 0x24:
			case 0x25:
			case 0x2C:
			case 0x2D:
			case 0x34:
			case 0x35:
			case 0x3C:
			case 0x3D:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				if ( ( CurrentValue & 0x07 ) == 5 )
				{
					if ( pDisasm->PrefixOperandSizeOverride )
					{
						pDisasm->Flags |= FLAG_IMM16;
					}
					else
					{
						pDisasm->Flags |= FLAG_IMM32;
					}
				}
				else
				{
					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0x06:
			case 0x07:
			case 0x0E:
			case 0x0F:
			case 0x16:
			case 0x17:
			case 0x1E:
			case 0x1F:
			{
				pDisasm->Flags |= FLAG_SEGMENT_REGISTER;
			}
			break;
			
			case 0x40:
			case 0x41:
			case 0x42:
			case 0x43:
			case 0x44:
			case 0x45:
			case 0x46:
			case 0x47:
			case 0x48:
			case 0x49:
			case 0x4A:
			case 0x4B:
			case 0x4C:
			case 0x4D:
			case 0x4E:
			case 0x4F:
			case 0x50:
			case 0x51:
			case 0x52:
			case 0x53:
			case 0x54:
			case 0x55:
			case 0x56:
			case 0x57:
			case 0x58:
			case 0x59:
			case 0x5A:
			case 0x5B:
			case 0x5C:
			case 0x5D:
			case 0x5E:
			case 0x5F:
			{
				pDisasm->Register = CurrentValue & 0x07;

				pDisasm->OpcodeFlags = 'Z';
			}
			break;

			case 0x60:
			case 0x61:
			{
				pDisasm->Register = ALL_GENERAL_PURPOSE;
			}
			break;

			case 0x62:
			case 0x63:
			{
				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0x68:
			case 0x69:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->PrefixOperandSizeOverride  )
				{
					pDisasm->Flags |= FLAG_IMM16;
				}
				else
				{
					pDisasm->Flags |= FLAG_IMM32;
				}

				if ( CurrentValue == 0x69 )
				{
					pDisasm->Flags |= FLAG_MODRM;
				}
			}
			break;

			case 0x6A:
			case 0x6B:
			{
				if ( CurrentValue == 0x6B )
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_MODRM;
				}
				
				pDisasm->Flags |= FLAG_IMM8;
			}
			break;

			case 0x70:
			case 0x71:
			case 0x72:
			case 0x73:
			case 0x74:
			case 0x75:
			case 0x76:
			case 0x77:
			case 0x78:
			case 0x79:
			case 0x7A:
			case 0x7B:
			case 0x7C:
			case 0x7D:
			case 0x7E:
			case 0x7F:
			{
				pDisasm->HasRelative = true;
						
				pDisasm->Flags |= FLAG_REL8;
			}
			break;
			
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				pDisasm->HasImmediate = true;

				if ( CurrentValue != 0x81 )
				{					
					pDisasm->Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;

					if ( pDisasm->PrefixOperandSizeOverride  )
					{				
						pDisasm->Flags |= FLAG_IMM16;
					}
					else
					{			
						pDisasm->Flags |= FLAG_IMM32;
					}
				}
								
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x84:
			case 0x85:
			case 0x86:
			case 0x87:
			case 0x88:
			case 0x89:
			case 0x8A:
			case 0x8B:
			{
				pDisasm->HasDirectionFlag = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0x8C:
			case 0x8D:
			case 0x8E:
			{
				if ( CurrentValue == 0x8C || CurrentValue == 0x8E )
				{
					pDisasm->HasDirectionFlag = true;
				}

				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0x8F:
			{
				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x90:
			case 0x91:
			case 0x92:
			case 0x93:
			case 0x94:
			case 0x95:
			case 0x96:
			case 0x97:
			case 0x98:
			case 0x99:
			{
				if ( CurrentValue == 0x90 )
				{
					pDisasm->IsPrefixRepeatValid = true;
				}

				pDisasm->Register = CurrentValue & 0x07;

				pDisasm->OpcodeFlags = 'Z';
			}
			break;

			case 0x9A:
			{
				pDisasm->HasRelative = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;
				
				if ( pDisasm->PrefixOperandSizeOverride  )
				{				
					pDisasm->Flags |= FLAG_REL16;
				}
				else
				{				
					pDisasm->Flags |= FLAG_REL32;
				}

				pDisasm->Flags |= FLAG_SEGMENT_REL;
			}
			break;
			
			case 0xA0:
			case 0xA1:
			case 0xA2:
			case 0xA3:
			{
				pDisasm->HasDisplacement = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Displacement.Displacement32 = *( unsigned long* )CurrentPointer;

				CurrentPointer += sizeof ( unsigned long );

				pDisasm->Flags |= FLAG_DISP32;
			}
			break;

			case 0xA8:
			{
				pDisasm->Flags |= FLAG_IMM8;
			}
			break;

			case 0xA9:
			{
				pDisasm->IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->PrefixOperandSizeOverride  )
				{				
					pDisasm->Flags |= FLAG_IMM16;
				}
				else
				{			
					pDisasm->Flags |= FLAG_IMM32;
				}
			}
			break;
			
			case 0xB0:
			case 0xB1:
			case 0xB2:
			case 0xB3:
			case 0xB4:
			case 0xB5:
			case 0xB6:
			case 0xB7:
			case 0xB8:
			case 0xB9:
			case 0xBA:
			case 0xBB:
			case 0xBC:
			case 0xBD:
			case 0xBE:
			case 0xBF:
			{
				pDisasm->HasImmediate = true;

				pDisasm->Register = CurrentValue & 0x07;

				if ( CurrentValue < 0xB8 )
				{
					pDisasm->Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->Register = CurrentValue & 0x07;

					pDisasm->IsPrefixOperandSizeOverideValid = true;

					if ( pDisasm->PrefixOperandSizeOverride )
					{
						pDisasm->Flags |= FLAG_IMM16;
					}
					else
					{
						pDisasm->Flags |= FLAG_IMM32;
					}
				}
			}
			break;

			case 0xC0:
			case 0xC1:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_OPCEXT | FLAG_IMM8;
			}
			break;

			case 0xC2:
			{
				pDisasm->HasImmediate = true;

				pDisasm->Flags |= FLAG_IMM16;
			}
			break;
			
			case 0xC4:
			case 0xC5:
			{
				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0xC6:
			case 0xC7:
			{					
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xC8:
			case 0xCA:
			case 0xCD:
			{
				pDisasm->HasImmediate = true;

				switch ( CurrentValue )
				{
					case 0xC8:
					{
						pDisasm->Flags |= FLAG_IMM16 | FLAG_IMM8_2;
					}
					break;

					case 0xCA:
					{
						pDisasm->Flags |= FLAG_IMM16;
					}
					break;

					case 0xCD:
					{
						pDisasm->Flags |= FLAG_IMM8;
					}
					break;
				}
			}
			break;
			
			case 0xD0:
			case 0xD1:
			case 0xD2:
			case 0xD3:
			{
				pDisasm->IsPrefixOperandSizeOverideValid = true;
				
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xD4:
			case 0xD5:
			{
				if ( *( CurrentPointer + 1 ) == 0x0A )
				{
					pDisasm->Opcode2 = *CurrentPointer++;
				}
				else
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;
			
			case 0xE0:
			case 0xE1:
			case 0xE2:
			case 0xE3:
			{
				pDisasm->HasRelative = true;

				pDisasm->Flags |= FLAG_REL8;
			}
			break;

			case 0xE4:
			case 0xE5:
			case 0xE6:
			case 0xE7:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_IMM8;
			}
			break;
			
			case 0xE8:
			case 0xE9:
			case 0xEA:
			case 0xEB:
			{
				pDisasm->HasRelative = true;

				switch ( CurrentValue )
				{
					case 0xE8:
					case 0xE9:
					case 0xEA:
					{
						pDisasm->IsPrefixOperandSizeOverideValid = true;

						if ( pDisasm->PrefixOperandSizeOverride )
						{				
							pDisasm->Flags |= FLAG_REL16;
						}
						else
						{				
							pDisasm->Flags |= FLAG_REL32;
						}

						if ( CurrentValue == 0xEA )
						{
							pDisasm->Flags |= FLAG_SEGMENT_REL;
						}
					}
					break;

					case 0xEB:
					{
						pDisasm->Flags |= FLAG_REL8;
					}
					break;
				}
			}
			break;

			case 0xD8:
			case 0xD9:
			case 0xDA:
			case 0xDB:
			case 0xDC:
			case 0xDD:
			case 0xDE:
			case 0xDF:
			case 0xF6:
			case 0xF7:
			case 0xFE:
			case 0xFF:
			{
				if ( CurrentValue == 0xF6 )
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;
				}
				
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;
		}
	}

	if ( !( pDisasm->Flags & FLAG_3DNOW ) && pDisasm->Flags & FLAG_MODRM || pDisasm->Flags & FLAG_OPCEXT )
	{
		LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );
	}
	if ( pDisasm->Flags & FLAG_PREFIX_0F )
	{
		switch ( CurrentValue )
		{	
			case 0xBA:
			{
				if ( pDisasm->ModRM_Reg > 4 )
				{
					pDisasm->IsLockPrefixValid = true;
				}

				if ( pDisasm->ModRM_Reg > 3 )
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;
		}
	}
	else
	{
		switch ( CurrentValue )
		{
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				if ( pDisasm->ModRM_Reg != 7 )
				{
					pDisasm->IsLockPrefixValid = true;
				}
			}
			break;

			case 0xC6:
			case 0xC7:
			{
				if ( pDisasm->ModRM_Reg == 0 )
				{
					pDisasm->HasImmediate = true;

					pDisasm->IsPrefixOperandSizeOverideValid = true;

					if ( CurrentValue == 0xC7 )
					{
						if ( pDisasm->PrefixOperandSizeOverride )
						{
							pDisasm->Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->Flags |= FLAG_IMM32;
						}
					}
					else
					{
						pDisasm->Flags |= FLAG_IMM8;
					}
				}
			}
			break;

			case 0xF6:
			{
				if ( pDisasm->ModRM_Reg <= 1 )
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0xF7:
			{
				if ( pDisasm->ModRM_Reg <= 4 )
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;
				}
				if ( pDisasm->ModRM_Reg <= 1 )
				{
					pDisasm->HasImmediate = true;

					if ( pDisasm->PrefixOperandSizeOverride  )
					{				
						pDisasm->Flags |= FLAG_IMM16;
					}
					else
					{				
						pDisasm->Flags |= FLAG_IMM32;
					}
				}
			}
			break;
		}
	}
}
//=============================================================================================================
void LDEDisasm64 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer, _In_ unsigned char CurrentValue )
{
	bool ProcessModRM = true;

	if ( CurrentValue == OPCODE_TWO_BYTE )
	{
		CurrentValue = *CurrentPointer++;

		pDisasm->Flags |= FLAG_PREFIX_0F;

		if ( CurrentValue == PREFIX_3DNOW )
		{
			pDisasm->Flags |= FLAG_3DNOW;

			LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );

			// check last byte for opcode

			pDisasm->Opcode = *CurrentPointer++;
		}
		else
		{
			pDisasm->Opcode = CurrentValue;

			switch ( CurrentValue )
			{
				case 0x00:
				case 0x01:
				case 0x18:
				case 0x19:
				case 0x1A:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
				case 0x71:
				case 0x72:
				case 0x73:
				case 0x90:
				case 0x91:
				case 0x92:
				case 0x93:
				case 0x94:
				case 0x95:
				case 0x96:
				case 0x97:
				case 0x98:
				case 0x99:
				case 0x9A:
				case 0x9B:
				case 0x9C:
				case 0x9D:
				case 0x9E:
				case 0x9F:
				case 0xAE:
				case 0xBA:
				case 0xC7:
				{
					if ( CurrentValue >= 0x71 && CurrentValue <= 0x73 )
					{
						pDisasm->Flags |= FLAG_IMM8;
					}

					pDisasm->Flags |= FLAG_OPCEXT;
				}
				break;

				case 0x02:
				case 0x03:
				case 0x10:
				case 0x11:
				case 0x12:
				case 0x13:
				case 0x14:
				case 0x15:
				case 0x16:
				case 0x17:
				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x26:
				case 0x27:
				case 0x28:
				case 0x29:
				case 0x2A:
				case 0x2B:
				case 0x2C:
				case 0x2D:
				case 0x2E:
				case 0x2F:
				case 0x38:
				case 0x3A:
				case 0x40:
				case 0x41:
				case 0x42:
				case 0x43:
				case 0x44:
				case 0x45:
				case 0x46:
				case 0x47:
				case 0x48:
				case 0x49:
				case 0x4A:
				case 0x4B:
				case 0x4C:
				case 0x4D:
				case 0x4E:
				case 0x4F:
				case 0x50:
				case 0x51:
				case 0x52:
				case 0x53:
				case 0x54:
				case 0x55:
				case 0x56:
				case 0x57:
				case 0x58:
				case 0x59:
				case 0x5A:
				case 0x5B:
				case 0x5C:
				case 0x5D:
				case 0x5E:
				case 0x5F:
				case 0x60:
				case 0x61:
				case 0x62:
				case 0x63:
				case 0x64:
				case 0x65:
				case 0x66:
				case 0x67:
				case 0x68:
				case 0x69:
				case 0x6A:
				case 0x6B:
				case 0x6C:
				case 0x6D:
				case 0x6E:
				case 0x6F:
				case 0x70:
				case 0x74:
				case 0x75:
				case 0x76:
				case 0x78:
				case 0x79:
				case 0x7C:
				case 0x7D:
				case 0x7E:
				case 0x7F:
				case 0xA3:
				case 0xA4:
				case 0xA5:
				case 0xAB:
				case 0xAC:
				case 0xAD:
				case 0xAF:
				case 0xB0:
				case 0xB1:
				case 0xB2:
				case 0xB3:
				case 0xB4:
				case 0xB5:
				case 0xB6:
				case 0xB7:
				case 0xB8:
				case 0xBB:
				case 0xBC:
				case 0xBD:
				case 0xBE:
				case 0xBF:
				case 0xC0:
				case 0xC1:
				case 0xC2:
				case 0xC3:
				case 0xC4:
				case 0xC5:
				case 0xC6:
				case 0xD0:
				case 0xD1:
				case 0xD2:
				case 0xD3:
				case 0xD4:
				case 0xD5:
				case 0xD6:
				case 0xD7:
				case 0xD8:
				case 0xD9:
				case 0xDA:
				case 0xDB:
				case 0xDC:
				case 0xDD:
				case 0xDE:
				case 0xDF:
				case 0xE0:
				case 0xE1:
				case 0xE2:
				case 0xE3:
				case 0xE4:
				case 0xE5:
				case 0xE6:
				case 0xE7:
				case 0xE8:
				case 0xE9:
				case 0xEA:
				case 0xEB:
				case 0xEC:
				case 0xED:
				case 0xEE:
				case 0xEF:
				case 0xF0:
				case 0xF1:
				case 0xF2:
				case 0xF3:
				case 0xF4:
				case 0xF5:
				case 0xF6:
				case 0xF7:
				case 0xF8:
				case 0xF9:
				case 0xFA:
				case 0xFB:
				case 0xFC:
				case 0xFD:
				case 0xFE:
				{
					ProcessModRM = true;

					switch ( CurrentValue )
					{
						case 0x38:
						case 0x3A:
						{
							pDisasm->Opcode2 = *CurrentPointer++;
												
							if ( CurrentValue == 0x3A )
							{
								if ( pDisasm->Opcode2 >= 0x08 && pDisasm->Opcode2 <= 0x0F 
									|| pDisasm->Opcode2 >= 0x14 && pDisasm->Opcode2 <= 0x22
									|| pDisasm->Opcode2 == 0x42
									|| pDisasm->Opcode2 == 0x62
									|| pDisasm->Opcode2 == 0x63 )
								{
									pDisasm->Flags |= FLAG_IMM8;
								}
							}
						}
						break;

						case 0x70:
						{
							pDisasm->Flags |= FLAG_IMM8;
						}
						break;

						case 0xA4:
						case 0xA5:
						{							
							pDisasm->HasDirectionFlag = true;

							if ( CurrentValue == 0xA4 )
							{
								pDisasm->HasImmediate = true;

								pDisasm->Flags |= FLAG_IMM8;
							}
						}
						break;

						case 0xAB:
						{
							pDisasm->IsLockPrefixValid = true;
						}
						break;

						case 0xAC:
						case 0xAD:
						{
							if ( CurrentValue == 0xAC )
							{
								pDisasm->Flags |= FLAG_IMM8;
							}

							pDisasm->HasDirectionFlag = true;
						}
						break;
						
						case 0xB0: 
						case 0xB1:
						{
							pDisasm->IsPrefixOperandSizeOverideValid = true;

							pDisasm->IsLockPrefixValid = true;
						}
						break;

						case 0xB2:
						case 0xB4:
						case 0xB5:
						{				
							pDisasm->Flags |= FLAG_SEGMENT_REGISTER;
						}
						break;

						case 0xAF:
						case 0xB6:
						case 0xB7:
						case 0xBE:
						case 0xBF:
						{
							pDisasm->IsPrefixOperandSizeOverideValid = true;
							pDisasm->HasDirectionFlag = true;
						}
						break;

						case 0xB8:
						{
							if ( pDisasm->PrefixRepeat != PREFIX_REPX )
							{
								ProcessModRM = false;
							}
						}
						break;

						case 0xC0:
						case 0xC1:
						{
							pDisasm->IsLockPrefixValid = true;
							pDisasm->HasDirectionFlag = true;
						}
						break;

						case 0xC2:
						case 0xC3:
						case 0xC4:
						case 0xC5:
						case 0xC6:
						{
							pDisasm->Flags |= FLAG_IMM8;
						}
						break;
					}
					if ( ProcessModRM == true )
					{						
						pDisasm->Flags |= FLAG_MODRM;
					}
				}
				break;
					
				case 0x80:
				case 0x81:
				case 0x82:
				case 0x83:
				case 0x84:
				case 0x85:
				case 0x86:
				case 0x87:
				case 0x88:
				case 0x89:
				case 0x8A:
				case 0x8B:
				case 0x8C:
				case 0x8D:
				case 0x8E:
				case 0x8F:
				{
					pDisasm->HasRelative = true;

					pDisasm->IsPrefixOperandSizeOverideValid = true;
				
					if ( pDisasm->PrefixOperandSizeOverride )
					{				
						pDisasm->Flags |= FLAG_REL16;
					}
					else
					{				
						pDisasm->Flags |= FLAG_REL32;
					}
				}
				break;
				
				case 0xA0:
				case 0xA1:
				case 0xA8:
				case 0xA9:
				{
					pDisasm->Flags |= FLAG_SEGMENT_REGISTER;
				}
				break;
				
				case 0xC8:
				case 0xC9:
				case 0xCA:
				case 0xCB:
				case 0xCC:
				case 0xCD:
				case 0xCE:
				case 0xCF:
				{
					pDisasm->Register = CurrentValue & 0x07;
				}
				break;
			}
		}
	}
	else
	{
		pDisasm->Opcode = CurrentValue;

		switch ( CurrentValue )
		{
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x03:
			case 0x08:
			case 0x09:
			case 0x0A:
			case 0x0B:
			case 0x10:
			case 0x11:
			case 0x12:
			case 0x13:
			case 0x18:
			case 0x19:
			case 0x1A:
			case 0x1B:
			case 0x20:
			case 0x21:
			case 0x22:
			case 0x23:
			case 0x28:
			case 0x29:
			case 0x2A:
			case 0x2B:
			case 0x30:
			case 0x31:
			case 0x32:
			case 0x33:
			case 0x38:
			case 0x39:
			case 0x3A:
			case 0x3B:
			{				
				switch ( CurrentValue & 0x07 )
				{
					case 0x00:
					case 0x01:
					{
						pDisasm->IsLockPrefixValid = true;
					}
					break;
				}

				pDisasm->HasDirectionFlag = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_MODRM;
			}
			break;


			case 0x04:
			case 0x05:
			case 0x0C:
			case 0x0D:
			case 0x14:
			case 0x15:
			case 0x1C:
			case 0x1D:
			case 0x24:
			case 0x25:
			case 0x2C:
			case 0x2D:
			case 0x34:
			case 0x35:
			case 0x3C:
			case 0x3D:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				if ( ( CurrentValue & 0x07 ) == 5 )
				{
					if ( pDisasm->PrefixOperandSizeOverride )
					{
						pDisasm->Flags |= FLAG_IMM16;
					}
					else
					{
						pDisasm->Flags |= FLAG_IMM32;
					}
				}
				else
				{
					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0x06:
			case 0x07:
			case 0x0E:
			case 0x0F:
			case 0x16:
			case 0x17:
			case 0x1E:
			case 0x1F:
			{
				pDisasm->Flags |= FLAG_SEGMENT_REGISTER;
			}
			break;
			
			case 0x40:
			case 0x41:
			case 0x42:
			case 0x43:
			case 0x44:
			case 0x45:
			case 0x46:
			case 0x47:
			case 0x48:
			case 0x49:
			case 0x4A:
			case 0x4B:
			case 0x4C:
			case 0x4D:
			case 0x4E:
			case 0x4F:
			case 0x50:
			case 0x51:
			case 0x52:
			case 0x53:
			case 0x54:
			case 0x55:
			case 0x56:
			case 0x57:
			case 0x58:
			case 0x59:
			case 0x5A:
			case 0x5B:
			case 0x5C:
			case 0x5D:
			case 0x5E:
			case 0x5F:
			{
				pDisasm->Register = CurrentValue & 0x07;

				pDisasm->OpcodeFlags = 'Z';
			}
			break;

			case 0x60:
			case 0x61:
			{
				pDisasm->Register = ALL_GENERAL_PURPOSE;
			}
			break;

			case 0x62:
			case 0x63:
			{
				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0x68:
			case 0x69:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->PrefixOperandSizeOverride  )
				{
					pDisasm->Flags |= FLAG_IMM16;
				}
				else
				{
					pDisasm->Flags |= FLAG_IMM32;
				}

				if ( CurrentValue == 0x69 )
				{
					pDisasm->Flags |= FLAG_MODRM;
				}
			}
			break;

			case 0x6A:
			case 0x6B:
			{
				if ( CurrentValue == 0x6B )
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_MODRM;
				}
				
				pDisasm->Flags |= FLAG_IMM8;
			}
			break;

			case 0x70:
			case 0x71:
			case 0x72:
			case 0x73:
			case 0x74:
			case 0x75:
			case 0x76:
			case 0x77:
			case 0x78:
			case 0x79:
			case 0x7A:
			case 0x7B:
			case 0x7C:
			case 0x7D:
			case 0x7E:
			case 0x7F:
			{
				pDisasm->HasRelative = true;
						
				pDisasm->Flags |= FLAG_REL8;
			}
			break;
			
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				pDisasm->HasImmediate = true;

				if ( CurrentValue != 0x81 )
				{					
					pDisasm->Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;

					if ( pDisasm->PrefixOperandSizeOverride  )
					{				
						pDisasm->Flags |= FLAG_IMM16;
					}
					else
					{			
						pDisasm->Flags |= FLAG_IMM32;
					}
				}
								
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x84:
			case 0x85:
			case 0x86:
			case 0x87:
			case 0x88:
			case 0x89:
			case 0x8A:
			case 0x8B:
			{
				pDisasm->HasDirectionFlag = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0x8C:
			case 0x8D:
			case 0x8E:
			{
				if ( CurrentValue == 0x8C || CurrentValue == 0x8E )
				{
					pDisasm->HasDirectionFlag = true;
				}

				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0x8F:
			{
				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x90:
			case 0x91:
			case 0x92:
			case 0x93:
			case 0x94:
			case 0x95:
			case 0x96:
			case 0x97:
			case 0x98:
			case 0x99:
			{
				if ( CurrentValue == 0x90 )
				{
					pDisasm->IsPrefixRepeatValid = true;
				}

				pDisasm->Register = CurrentValue & 0x07;

				pDisasm->OpcodeFlags = 'Z';
			}
			break;

			case 0x9A:
			{
				pDisasm->HasRelative = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;
				
				if ( pDisasm->PrefixOperandSizeOverride  )
				{				
					pDisasm->Flags |= FLAG_REL16;
				}
				else
				{				
					pDisasm->Flags |= FLAG_REL32;
				}

				pDisasm->Flags |= FLAG_SEGMENT_REL;
			}
			break;
			
			case 0xA0:
			case 0xA1:
			case 0xA2:
			case 0xA3:
			{
				pDisasm->HasDisplacement = true;

				pDisasm->IsPrefixAddressSizeOverrideValid = true;

				if ( pDisasm->REX_W )
				{
					if ( pDisasm->PrefixAddressSizeOverride )
					{
						pDisasm->Displacement.Displacement64 = *( unsigned long* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned long );

						pDisasm->Flags |= FLAG_DISP32;
					}
					else
					{
						pDisasm->Displacement.Displacement64 = *( unsigned long long* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned long long );

						pDisasm->Flags |= FLAG_DISP64;
					}
				}
				else
				{
					if ( pDisasm->PrefixAddressSizeOverride )
					{
						pDisasm->Displacement.Displacement64 = *( unsigned short* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned short );

						pDisasm->Flags |= FLAG_DISP16;
					}
					else
					{
						pDisasm->Displacement.Displacement64 = *( unsigned long* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned long );

						pDisasm->Flags |= FLAG_DISP32;
					}
				}
			}
			break;


			case 0xA8:
			{
				pDisasm->Flags |= FLAG_IMM8;
			}
			break;

			case 0xA9:
			{
				pDisasm->IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->PrefixOperandSizeOverride  )
				{				
					pDisasm->Flags |= FLAG_IMM16;
				}
				else
				{			
					pDisasm->Flags |= FLAG_IMM32;
				}
			}
			break;
			
			case 0xB0:
			case 0xB1:
			case 0xB2:
			case 0xB3:
			case 0xB4:
			case 0xB5:
			case 0xB6:
			case 0xB7:
			case 0xB8:
			case 0xB9:
			case 0xBA:
			case 0xBB:
			case 0xBC:
			case 0xBD:
			case 0xBE:
			case 0xBF:
			{
				pDisasm->HasImmediate = true;

				pDisasm->Register = CurrentValue & 0x07;

				pDisasm->OpcodeFlags = 'Z';

				if ( CurrentValue < 0xB8 )
				{
					pDisasm->Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;
					pDisasm->IsPrefixAddressSizeOverrideValid = true;

					pDisasm->Register = CurrentValue & 0x07;

					if ( pDisasm->REX_W )
					{
						if ( pDisasm->PrefixAddressSizeOverride )
						{
							pDisasm->Flags |= FLAG_IMM32;
						}
						else
						{
							pDisasm->Flags |= FLAG_IMM64;
						}
					}
					else
					{
						if ( pDisasm->PrefixOperandSizeOverride )
						{
							pDisasm->Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->Flags |= FLAG_IMM32;
						}
					}
				}
			}
			break;

			case 0xC0:
			case 0xC1:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_OPCEXT | FLAG_IMM8;
			}
			break;

			case 0xC2:
			{
				pDisasm->HasImmediate = true;

				pDisasm->Flags |= FLAG_IMM16;
			}
			break;
			
			case 0xC4:
			case 0xC5:
			{
				pDisasm->Flags |= FLAG_MODRM;
			}
			break;

			case 0xC6:
			case 0xC7:
			{					
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xC8:
			case 0xCA:
			case 0xCD:
			{
				pDisasm->HasImmediate = true;

				switch ( CurrentValue )
				{
					case 0xC8:
					{
						pDisasm->Flags |= FLAG_IMM16 | FLAG_IMM8_2;
					}
					break;

					case 0xCA:
					{
						pDisasm->Flags |= FLAG_IMM16;
					}
					break;

					case 0xCD:
					{
						pDisasm->Flags |= FLAG_IMM8;
					}
					break;
				}
			}
			break;
			
			case 0xD0:
			case 0xD1:
			case 0xD2:
			case 0xD3:
			{
				pDisasm->IsPrefixOperandSizeOverideValid = true;
				
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xD4:
			case 0xD5:
			{
				if ( *( CurrentPointer + 1 ) == 0x0A )
				{
					pDisasm->Opcode2 = *CurrentPointer++;
				}
				else
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;
			
			case 0xE0:
			case 0xE1:
			case 0xE2:
			case 0xE3:
			{
				pDisasm->HasRelative = true;

				pDisasm->Flags |= FLAG_REL8;
			}
			break;

			case 0xE4:
			case 0xE5:
			case 0xE6:
			case 0xE7:
			{
				pDisasm->HasImmediate = true;

				pDisasm->IsPrefixOperandSizeOverideValid = true;

				pDisasm->Flags |= FLAG_IMM8;
			}
			break;
			
			case 0xE8:
			case 0xE9:
			case 0xEA:
			case 0xEB:
			{
				pDisasm->HasRelative = true;

				switch ( CurrentValue )
				{
					case 0xE8:
					case 0xE9:
					case 0xEA:
					{
						pDisasm->IsPrefixOperandSizeOverideValid = true;

						if ( pDisasm->PrefixOperandSizeOverride )
						{				
							pDisasm->Flags |= FLAG_REL16;
						}
						else
						{				
							pDisasm->Flags |= FLAG_REL32;
						}

						if ( CurrentValue == 0xEA )
						{
							pDisasm->Flags |= FLAG_SEGMENT_REL;
						}
					}
					break;

					case 0xEB:
					{
						pDisasm->Flags |= FLAG_REL8;
					}
					break;
				}
			}
			break;

			case 0xD8:
			case 0xD9:
			case 0xDA:
			case 0xDB:
			case 0xDC:
			case 0xDD:
			case 0xDE:
			case 0xDF:
			case 0xF6:
			case 0xF7:
			case 0xFE:
			case 0xFF:
			{
				if ( CurrentValue == 0xF6 )
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;
				}
				
				pDisasm->Flags |= FLAG_OPCEXT;
			}
			break;
		}
	}

	if ( !( pDisasm->Flags & FLAG_3DNOW ) && pDisasm->Flags & FLAG_MODRM || pDisasm->Flags & FLAG_OPCEXT )
	{
		LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );
	}
	if ( pDisasm->Flags & FLAG_PREFIX_0F )
	{
		switch ( CurrentValue )
		{	
			case 0xBA:
			{
				if ( pDisasm->ModRM_Reg > 4 )
				{
					pDisasm->IsLockPrefixValid = true;
				}

				if ( pDisasm->ModRM_Reg > 3 )
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;
		}
	}
	else
	{
		switch ( CurrentValue )
		{
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				if ( pDisasm->ModRM_Reg != 7 )
				{
					pDisasm->IsLockPrefixValid = true;
				}
			}
			break;

			case 0xC6:
			case 0xC7:
			{
				if ( pDisasm->ModRM_Reg == 0 )
				{
					pDisasm->HasImmediate = true;

					pDisasm->IsPrefixOperandSizeOverideValid = true;

					if ( CurrentValue == 0xC7 )
					{
						if ( pDisasm->PrefixOperandSizeOverride )
						{
							pDisasm->Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->Flags |= FLAG_IMM32;
						}
					}
					else
					{
						pDisasm->Flags |= FLAG_IMM8;
					}
				}
			}
			break;

			case 0xF6:
			{
				if ( pDisasm->ModRM_Reg <= 1 )
				{
					pDisasm->HasImmediate = true;

					pDisasm->Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0xF7:
			{
				if ( pDisasm->ModRM_Reg <= 4 )
				{
					pDisasm->IsPrefixOperandSizeOverideValid = true;
				}
				if ( pDisasm->ModRM_Reg <= 1 )
				{
					pDisasm->HasImmediate = true;

					if ( pDisasm->REX_W )
					{
						if ( pDisasm->PrefixAddressSizeOverride )
						{
							pDisasm->Flags |= FLAG_IMM32;
						}
						else
						{
							pDisasm->Flags |= FLAG_IMM64;
						}
					}
					else
					{
						if ( pDisasm->PrefixOperandSizeOverride )
						{
							pDisasm->Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->Flags |= FLAG_IMM32;
						}
					}
				}
			}
			break;
		}
	}
}
//=============================================================================================================
void LDEModRMAndSIB3264 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer )
{
	pDisasm->Flags |= FLAG_MODRM;

	pDisasm->ModRM = *CurrentPointer++;

	LDEDecodeModRM3264 ( pDisasm );
	
	if ( pDisasm->UsingSIB )
	{
		pDisasm->SIB = *CurrentPointer++;

		LDEDecodeSIB3264 ( pDisasm );

		pDisasm->Flags |= FLAG_SIB;
	}

	if ( pDisasm->DisplacementSize != 0 )
	{
		if ( pDisasm->DisplacementSize == sizeof ( unsigned char ) )
		{
			pDisasm->Displacement.Displacement8 = *CurrentPointer++;

			pDisasm->HasDisplacement = true;

			pDisasm->Flags |= FLAG_DISP8;
		}
		else if ( pDisasm->DisplacementSize == sizeof ( unsigned short ) )
		{
			pDisasm->Displacement.Displacement16 = *( unsigned short* )CurrentPointer;

			pDisasm->HasDisplacement = true;

			CurrentPointer += sizeof ( unsigned short );

			pDisasm->Flags |= FLAG_DISP16;
		}
		else if ( pDisasm->DisplacementSize == sizeof ( unsigned long ) )
		{
			pDisasm->Displacement.Displacement32 = *( unsigned long* )CurrentPointer;

			pDisasm->HasDisplacement = true;

			CurrentPointer += sizeof ( unsigned long );

			pDisasm->Flags |= FLAG_DISP32;
		}
	}
}
//=============================================================================================================
void LDEDecodeModRM3264 ( _Inout_ LDEDisasm_t* pDisasm )
{
	pDisasm->ModRM_Mod = pDisasm->ModRM >> 6;
	pDisasm->ModRM_Reg = ( pDisasm->ModRM & 0x3F ) >> 3;
	pDisasm->ModRM_RM = pDisasm->ModRM & 7;

	pDisasm->DisplacementSize = 0;
	pDisasm->UseRegisterToRegister = false;
	pDisasm->UsingSIB = false;

	switch ( pDisasm->ModRM_Mod )
	{
		case 0:
		{
			// register indirect addressing mode
			// or sib with no displacement when R/M is 100

			if ( pDisasm->PrefixAddressSizeOverride )
			{
				if ( pDisasm->ModRM_RM == 6 )
				{
					pDisasm->DisplacementSize = sizeof ( unsigned short );
				}
			}
			else
			{
				if ( pDisasm->ModRM_RM == 5 )
				{
					pDisasm->DisplacementSize = sizeof ( unsigned long );
				}
			}
		}
		break;

		case 1:
		{
			pDisasm->DisplacementSize = sizeof ( unsigned char );
		}
		break;
		
		case 2:
		{
			pDisasm->DisplacementSize = sizeof ( unsigned short );

			if ( !pDisasm->Prefix67 )
			{
				pDisasm->DisplacementSize <<= 1;
			}
		}
		break;

		case 3:
		{				
			pDisasm->UseRegisterToRegister = true; // register to register
		}
		break;
	}

	if ( pDisasm->ModRM_RM == 4 ) // sib byte follows, unless modrm_mod is 3, in which case we use rsp/esp
	{
		if ( pDisasm->UseRegisterToRegister && !pDisasm->PrefixAddressSizeOverride )
		{
			pDisasm->RMRegister = REG_ESP; // rsp

			pDisasm->UsingSIB = false;
		}
		else
		{
			pDisasm->UsingSIB = true;
		}
	}
	if ( pDisasm->ModRM_RM == 5 ) // displacement addressing
	{
		if ( pDisasm->ModRM_Mod != 0 )
		{
			pDisasm->RMRegister = REG_EBP; // rbp
		}
		else
		{
			pDisasm->UseDisplacementOnly = true;
		}
	}
}
//=============================================================================================================
void LDEDecodeSIB3264 ( _Inout_ LDEDisasm_t* pDisasm )
{
	pDisasm->SIB_Scale = pDisasm->SIB >> 6;
	pDisasm->SIB_Index = ( pDisasm->SIB & 0x3F ) >> 3;
	pDisasm->SIB_Base = pDisasm->SIB & 7;
		
	pDisasm->Scalar = ( 1 << pDisasm->SIB_Scale );

	if ( pDisasm->SIB_Index == 4 ) // illegal, set invalid register
	{
		pDisasm->SIB_Index = REG_INVALID;
	}
	if ( pDisasm->SIB_Base == 5 )
	{
		if ( pDisasm->ModRM_Mod != 1 )
		{
			pDisasm->DisplacementSize = sizeof ( unsigned long );
		}
		else
		{
			pDisasm->DisplacementSize = sizeof ( unsigned char );
		}
	}
}
//=============================================================================================================