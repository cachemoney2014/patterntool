//========================================================================================
typedef struct
{
	uintptr_t BlockStart;
	uintptr_t BlockEnd;
}Block_t;
//========================================================================================
typedef struct
{
	uintptr_t BlockStart;
	uintptr_t BlockEnd;
}BasicBlock_t;
//=============================================================================================================
typedef struct
{
	uintptr_t Address;
	uintptr_t Size;
	uintptr_t Base;
}JumpTable_t;
//=============================================================================================================
BOOL IsUnConditionalBranch ( LDEDisasm_t* pDisasm );
BOOL IsConditionalBranch ( LDEDisasm_t* pDisasm );
uintptr_t GetBranchAddress ( LDEDisasm_t* pDisasm );
//=============================================================================================================
BOOL IsBranchOrReturn ( LDEDisasm_t* pDisasm );
//=============================================================================================================
#define INSTR_PREFIX_0F 0x0F
#define INSTR_FARJMP 0x2D 
#define INSTR_SHORTJCC_BEGIN 0x70
#define INSTR_SHORTJCC_END 0x7F
#define INSTR_JCC_BEGIN 0x80
#define INSTR_JCC_END 0x8F
#define INSTR_NOP 0x90
#define INSTR_RET 0xC2
#define INSTR_RETN 0xC3
#define INSTR_RETFN 0xCA
#define INSTR_RETF 0xCB
#define INSTR_INT3 0xCC
#define INSTR_LOOPNZ 0xE0
#define INSTR_LOOPZ 0xE1
#define INSTR_LOOP 0xE2
#define INSTR_RELJCX 0xE3
#define INSTR_RELCALL 0xE8
#define INSTR_RELJMP 0xE9
#define INSTR_FARJMP 0xEA
#define INSTR_SHORTJMP 0xEB
#define INSTR_FF 0xFF
//=============================================================================================================