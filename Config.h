//=============================================================================================================
typedef struct
{
	std::string ReferenceBinaryPath;
}ReferenceBinary_t;
//=============================================================================================================
enum
{
	NONE = 1,
	USE_DISP = 2,
	USE_IMM = 4,
};
//=============================================================================================================
enum
{
	NEUTRALIZE_NONE = 0,
	NEUTRALIZE_8BIT_DISP = 1,
	NEUTRALIZE_16BIT_DISP = 2,
	NEUTRALIZE_32BIT_DISP = 4,
	NEUTRALIZE_64BIT_DISP = 8,

	NEUTRALIZE_8BIT_IMM = 16,
	NEUTRALIZE_16BIT_IMM = 32,
	NEUTRALIZE_32BIT_IMM = 64,
	NEUTRALIZE_64BIT_IMM = 128,

	NEUTRALIZE_8BIT_REL = 256,
	NEUTRALIZE_16BIT_REL = 512,
	NEUTRALIZE_32BIT_REL = 1024,

	NEUTRALIZE_REGISTERS_32 = 2048,
	NEUTRALIZE_REGISTERS_64 = 4096,

	NEUTRALIZE_RELOCATIONS = 8192
};
//=============================================================================================================
typedef struct
{
	std::string VTableName;

	size_t Index;
	size_t PatternSize;
	
}VTable_Entry_t;
//=============================================================================================================
typedef struct
{
	uintptr_t RVA;
	uintptr_t VA;

	uintptr_t OriginalPatternVA;

	double RatioMatch;

	PBYTE PatternBuffer;

	size_t PatternSize; // condenser
}FOUND_RVAS_t;
//=============================================================================================================
typedef struct
{
	std::string EntryName;
	std::string SearchEntryName1;
	std::string SearchEntryName2;
	
	std::string StringToSearchFor;
			
	bool FindAllOccurances;
	bool IsReferenced;
	bool UseCondenser;

	bool GetDisplacement;
	bool GetImmediate;
	bool GetRelative;

	bool GetRange;

	bool MarkedAsVTable;

	bool RVAToDisplacement;
	bool RVAToImmediate;
	bool RVAToRelative;

	bool SearchBackward;
	bool SearchForward;
	bool SearchBetween;

	intptr_t Range1;
	intptr_t Range2;
	
	intptr_t Occurance1, Occurance2;

	intptr_t RetrieveDisplacement;
	intptr_t RetrieveImmediate;
	intptr_t RetrieveRelative;

	uintptr_t PatternSize;

	uintptr_t NeutralizeFlags;
	uintptr_t RVA;
	uintptr_t ReferenceBinaryIndex;

	uintptr_t MarkAsVTable;
	
	std::vector<VTable_Entry_t> VTableEntries;

	std::vector<FOUND_RVAS_t> m_FoundRVAS;
}MemoryLocation_t;
//=============================================================================================================
class CConfig
{
public:

	CConfig ( char* FileName );
	~CConfig();

	bool ExecuteConfig();

	bool ConfigNotFound()
	{
		return FailedToOpenFile;
	}

	std::vector<MemoryLocation_t>& GetConfigEntries()
	{
		return ConfigEntries;
	}

	std::vector<ReferenceBinary_t>& GetReferenceBinaryVector()
	{
		return ReferenceBinaries;
	}

	bool ProcessLine ( std::vector<std::string>& Line, size_t LineCounter );

	const char* ReturnBinaryToLoad();

	const char* GetOutputPath()
	{
		return OutputPath.c_str();
	}

	const char* GetGlobalName()
	{
		return GlobalName.c_str();
	}

private:
	
	bool FailedToOpenFile;

	std::ifstream FileConfigStream;
	std::vector<MemoryLocation_t> ConfigEntries;

	std::string BinaryToLoad;
	std::string CurrentDirectory;
	std::string GlobalName;
	std::string OutputPath;
	std::string ReferenceBinaryDirectory;

	std::vector<ReferenceBinary_t> ReferenceBinaries;
};
//=============================================================================================================