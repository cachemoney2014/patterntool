//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "LDE.h"
#include "Misc.h"
#include "PEManager.h"
//==================================================================================================================================
#define IMAGE_FIRST_SECTION32( ntheader ) ((PIMAGE_SECTION_HEADER)        \
    ((ULONG_PTR)(ntheader) +                                            \
     FIELD_OFFSET( IMAGE_NT_HEADERS32, OptionalHeader ) +                 \
     ((ntheader))->FileHeader.SizeOfOptionalHeader   \
    ))
//==================================================================================================================================
#define IMAGE_FIRST_SECTION64( ntheader ) ((PIMAGE_SECTION_HEADER)        \
    ((ULONG_PTR)(ntheader) +                                            \
     FIELD_OFFSET( IMAGE_NT_HEADERS64, OptionalHeader ) +                 \
     ((ntheader))->FileHeader.SizeOfOptionalHeader   \
    ))
//==================================================================================================================================
CPEManager::CPEManager()
{
	IsLoaded = false;

	MappedAsNormal = false;

	File = INVALID_HANDLE_VALUE;

	RawFileMap = NULL;
}
//==================================================================================================================================
CPEManager::~CPEManager()
{
	MappedAsNormal = false;

	if ( File != INVALID_HANDLE_VALUE )
	{
		CloseHandle ( File );
	}

	if ( RawFileMap != NULL )
	{
		VirtualFree ( RawFileMap, 0, MEM_RELEASE );
		
		RawFileMap = NULL;
	}
}
//=============================================================================================================
size_t CPEManager::GetSectionCount()
{
	if ( Is64Bit == true )
	{
		return ImageNTHeaders64->FileHeader.NumberOfSections;
	}
	else
	{
		return ImageNTHeaders32->FileHeader.NumberOfSections;
	}

	return 0;
}
//=============================================================================================================
PIMAGE_NT_HEADERS32 CPEManager::GetNTHeaders32()
{
	return ImageNTHeaders32;
}
//=============================================================================================================
PIMAGE_NT_HEADERS64 CPEManager::GetNTHeaders64()
{
	return ImageNTHeaders64;
}
//=============================================================================================================
PIMAGE_SECTION_HEADER CPEManager::GetSectionHeader()
{
	return SectionHeader;
}
//=============================================================================================================
uintptr_t CPEManager::GetAllocationSize()
{
	if ( MappedAsNormal == true )
	{
		if ( Is64Bit == true )
		{
			return ImageNTHeaders64->OptionalHeader.SizeOfImage;
		}
		else
		{
			return ImageNTHeaders32->OptionalHeader.SizeOfImage;
		}
	}
	else
	{
		return FileSize.LowPart;
	}
}
//=============================================================================================================
bool CPEManager::LoadFile()
{
	bool Return = false;

	DWORD BytesRead = 0;

	if ( IsLoaded )
	{
		Return = true;
	}
	if ( File != INVALID_HANDLE_VALUE && FileSize.LowPart != 0 && IsLoaded == false )
	{
		RawFileMap = VirtualAlloc ( 0, FileSize.LowPart, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );

		if ( RawFileMap != NULL )
		{
			if ( ReadFile ( File, RawFileMap, FileSize.LowPart, &BytesRead, NULL ) )
			{
				Return = true;

				IsLoaded = true;

				DosHeader = ( PIMAGE_DOS_HEADER )RawFileMap;

				FileHeader = ( PIMAGE_FILE_HEADER )( ( uintptr_t )DosHeader + DosHeader->e_lfanew + sizeof ( DWORD ) );
				
				Is64Bit = false;
				
				if ( FileHeader->Machine != 0x14C
					&& ( FileHeader->Machine == 0x200 || FileHeader->Machine == 0x8664 ) )
				{
					Is64Bit = true;
				}

				if ( Is64Bit == true )
				{
					ImageNTHeaders64 = ( PIMAGE_NT_HEADERS64 )( ( uintptr_t )DosHeader + DosHeader->e_lfanew );

					SectionHeader = ( PIMAGE_SECTION_HEADER )IMAGE_FIRST_SECTION64 ( ImageNTHeaders64 );
				}
				else
				{
					ImageNTHeaders32 = ( PIMAGE_NT_HEADERS32 )( ( uintptr_t )DosHeader + DosHeader->e_lfanew );

					SectionHeader = ( PIMAGE_SECTION_HEADER )IMAGE_FIRST_SECTION32 ( ImageNTHeaders32 );
				}
			}
			else
			{
				CloseHandle ( File );
			}
		}
		else
		{
			CloseHandle ( File );
		}
	}

	return Return;
}
//=============================================================================================================
bool IsLessThan ( DWORD_PTR dwFirst, DWORD_PTR dwSecond )
{
	return ( dwFirst < dwSecond );
}
//=============================================================================================================
bool CPEManager::MapAsNormal()
{
	using namespace std;

	bool Return = true;

	DWORD BytesRead, Count, ReturnCode;
	uintptr_t Address, Delta, Size, End, RVA;
	PIMAGE_BASE_RELOCATION	RelocationDir;
	PUSHORT TypeOffset;

	LDEDisasm_t Data;

	uintptr_t SizeOfHeaders, SizeOfImage, NumberOfSections;

	if ( Is64Bit == true )
	{
		SizeOfHeaders = ImageNTHeaders64->OptionalHeader.SizeOfHeaders;

		SizeOfImage = ImageNTHeaders64->OptionalHeader.SizeOfImage;

		NumberOfSections = ImageNTHeaders64->FileHeader.NumberOfSections;
	}
	else
	{
		SizeOfHeaders = ImageNTHeaders32->OptionalHeader.SizeOfHeaders;

		SizeOfImage = ImageNTHeaders32->OptionalHeader.SizeOfImage;

		NumberOfSections = ImageNTHeaders32->FileHeader.NumberOfSections;
	}

	if ( RawFileMap != NULL )
	{
		if ( !VirtualFree ( RawFileMap, 0, MEM_RELEASE ) )
		{
			Return = false;
		}
		else
		{
			RawFileMap = NULL;
		}
	}
	
	RawFileMap = VirtualAlloc ( 0, SizeOfImage, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );

	if ( RawFileMap != NULL )
	{
		ReturnCode = SetFilePointer ( File, 0, NULL, FILE_BEGIN );

		if ( ReturnCode != INVALID_SET_FILE_POINTER )
		{
			if ( ReadFile ( File, RawFileMap, SizeOfHeaders, &BytesRead, NULL ) )
			{
				DosHeader = ( PIMAGE_DOS_HEADER )RawFileMap;

				FileHeader = ( PIMAGE_FILE_HEADER )( ( uintptr_t )DosHeader + DosHeader->e_lfanew + sizeof ( DWORD ) );
				
				Is64Bit = false;
				
				if ( FileHeader->Machine != 0x14C
					&& ( FileHeader->Machine == 0x200 || FileHeader->Machine == 0x8664 ) )
				{
					Is64Bit = true;
				}

				if ( Is64Bit == true )
				{
					ImageNTHeaders64 = ( PIMAGE_NT_HEADERS64 )( ( uintptr_t )DosHeader + DosHeader->e_lfanew );

					SectionHeader = ( PIMAGE_SECTION_HEADER )IMAGE_FIRST_SECTION64 ( ImageNTHeaders64 );
				}
				else
				{
					ImageNTHeaders32 = ( PIMAGE_NT_HEADERS32 )( ( uintptr_t )DosHeader + DosHeader->e_lfanew );

					SectionHeader = ( PIMAGE_SECTION_HEADER )IMAGE_FIRST_SECTION32 ( ImageNTHeaders32 );
				}

				for ( size_t SectionIterator = 0; SectionIterator < NumberOfSections; SectionIterator++ )
				{
					if ( !ReadFile ( File, ( LPVOID )( ( uintptr_t )RawFileMap + SectionHeader [ SectionIterator ].VirtualAddress ), SectionHeader [ SectionIterator ].SizeOfRawData, &BytesRead, NULL ) )
					{
						Return = false;
					}
				}

				if ( Is64Bit == false )
				{
					Delta = ( uintptr_t )RawFileMap - ImageNTHeaders32->OptionalHeader.ImageBase;

					RelocationDir = ( PIMAGE_BASE_RELOCATION )( ( uintptr_t )RawFileMap + ImageNTHeaders32->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].VirtualAddress );
				}
				else
				{
					Delta = ( uintptr_t )RawFileMap - ( uintptr_t )ImageNTHeaders64->OptionalHeader.ImageBase;

					RelocationDir = ( PIMAGE_BASE_RELOCATION )( ( uintptr_t )RawFileMap + ImageNTHeaders64->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].VirtualAddress );
				}

				if ( RelocationDir != 0 )
				{
					while ( Delta != 0 && RelocationDir->SizeOfBlock > 0 )
					{
						Count = ( ( RelocationDir->SizeOfBlock ) - sizeof ( IMAGE_BASE_RELOCATION ) ) / sizeof ( USHORT );
						Address = ( uintptr_t )( ( uintptr_t )RawFileMap + RelocationDir->VirtualAddress );
						TypeOffset = ( PUSHORT )( RelocationDir + 1 );

						RelocationDir = LdrProcessRelocationBlockLongLong ( Address,
										Count,
										TypeOffset,
										Delta );

						if ( RelocationDir == NULL )
						{
							break;
						}
					}
				}
					
				std::sort ( RelocationBlock.begin(), RelocationBlock.end(), IsLessThan );
				std::sort ( RelocationBlockRVA.begin(), RelocationBlockRVA.end(), IsLessThan );
				std::sort ( RelocationBlockRVA2.begin(), RelocationBlockRVA2.end(), IsLessThan );

				for ( size_t SectionCounter = 0; SectionCounter < GetSectionCount(); SectionCounter++ )
				{
					if ( SectionHeader [ SectionCounter ].Characteristics & IMAGE_SCN_MEM_EXECUTE )
					{
						bool Skip = false;

						Address = GetAllocationBase() + SectionHeader [ SectionCounter ].VirtualAddress;
						Size = SectionHeader [ SectionCounter ].Misc.VirtualSize;
						End = Address + Size;
						RVA = 0;

						int Length = 0;

						while ( Address < End )
						{	
							memset ( &Data, 0, sizeof ( LDEDisasm_t ) );
								
							RVA = Address - GetAllocationBase();

							if ( std::bsearch ( &RVA, RelocationBlockRVA.data(), RelocationBlockRVA.size(), sizeof ( DWORD_PTR ), compare ) )
							{
								if ( Is64Bit == false )
								{
									Length = sizeof ( unsigned long );
								}
								else
								{
									Length = sizeof ( unsigned long long );
								}

								Skip = true;
							}
							else
							{
								if ( Is64Bit == true )
								{
									Length = LDEDisasm ( &Data, ( PBYTE )Address, MODE_64 );
								}
								else
								{
									Length = LDEDisasm ( &Data, ( PBYTE )Address, MODE_32 );
								}
									
								if ( Length != -1 )
								{
									Skip = false;
								}
								else
								{
									Length = 1;
								}
							}
							if ( Skip == false )
							{
								if ( Data.HasDisplacement && Data.HasImmediate )
								{
									InstructionGeneralData.push_back ( Address );
								}
								else if ( Data.HasDisplacement && !Data.HasImmediate )
								{
									InstructionGeneralData.push_back ( Address );

									Displacement.push_back ( Data.Displacement.Displacement64 );

									DispAndImm.push_back ( Data.Displacement.Displacement64 );
								}
								else if ( !Data.HasDisplacement && Data.HasImmediate )
								{
									InstructionGeneralData.push_back ( Address );

									Immediates.push_back ( Data.Immediate.Immediate64 );

									DispAndImm.push_back ( Data.Immediate.Immediate64 );
								}
								else if ( Data.HasRelative )
								{
									InstructionGeneralData.push_back ( Address );
								}
							}

							Address += Length;
						}
					}
				}
			}
		}
		else
		{
			Return = false;
		}
	}
	if ( Return != false )
	{
		vector<uintptr_t>::iterator Iterator;

		sort ( Displacement.begin(), Displacement.end() );

		Iterator = unique ( Displacement.begin(), Displacement.end() );

		if ( Iterator != Displacement.end() )
		{
			Displacement.erase ( Iterator, Displacement.end() );
		}

		sort ( Displacement.begin(), Displacement.end() );
				
		sort ( Immediates.begin(), Immediates.end() );

		Iterator = unique ( Immediates.begin(), Immediates.end() );

		if ( Iterator != Immediates.end() )
		{
			Immediates.erase ( Iterator, Immediates.end() );
		}

		sort ( Immediates.begin(), Immediates.end() );

		sort ( DispAndImm.begin(), DispAndImm.end() );

		Iterator = unique ( DispAndImm.begin(), DispAndImm.end() );

		if ( Iterator != DispAndImm.end() )
		{
			DispAndImm.erase ( Iterator, DispAndImm.end() );
		}

		sort ( DispAndImm.begin(), DispAndImm.end() );
		
		MappedAsNormal = true;
	}
	
	return Return;
}
//==================================================================================================================================
bool CPEManager::OpenFile ( const char* FileToOpen )
{
	memset ( &FileSize, 0, sizeof ( FileSize ) );

	File = CreateFileA ( FileToOpen, GENERIC_READ, FILE_SHARE_READ, 
			NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );

	if ( File != INVALID_HANDLE_VALUE )
	{
		if ( GetFileSizeEx ( File, &FileSize ) )
		{
			return true;
		}
	}

	return false;
}
//==================================================================================================================================
PIMAGE_BASE_RELOCATION CPEManager::LdrProcessRelocationBlockLongLong ( uintptr_t Address, DWORD Count, PUSHORT TypeOffset, uintptr_t Delta )		
{
    SHORT Offset;
    USHORT Type;
    DWORD i;
    PUSHORT ShortPtr;
    PULONG LongPtr;
    PULONGLONG LongLongPtr;

	for ( i = 0; i < Count; i++ )
	{
		Offset = *TypeOffset & 0xFFF;
		Type = *TypeOffset >> 12;
		ShortPtr = ( PUSHORT )( Address + Offset );
               
		switch ( Type )
		{
			case IMAGE_REL_BASED_ABSOLUTE:
			{
				
			}
			break;

			case IMAGE_REL_BASED_HIGH:
			{
				*ShortPtr = HIWORD ( MAKELONG ( 0, *ShortPtr ) + ( Delta & 0xFFFFFFFF ) );

				RelocationBlock.push_back ( ( uintptr_t )*ShortPtr );
				RelocationBlockRVA.push_back ( ( uintptr_t )ShortPtr - ( uintptr_t )RawFileMap );
				RelocationBlockRVA2.push_back ( ( uintptr_t )*ShortPtr - ( uintptr_t )RawFileMap );
			}
			break;

			case IMAGE_REL_BASED_LOW:
			{
				*ShortPtr = *ShortPtr + LOWORD ( Delta & 0xFFFF );

				RelocationBlock.push_back ( ( uintptr_t )*ShortPtr );
				RelocationBlockRVA.push_back ( ( uintptr_t )ShortPtr - ( uintptr_t )RawFileMap );
				RelocationBlockRVA2.push_back ( ( uintptr_t )*ShortPtr - ( uintptr_t )RawFileMap );

			}
			break;

			case IMAGE_REL_BASED_HIGHLOW:
			{
				LongPtr = ( PULONG )( Address + Offset );
				*LongPtr = *LongPtr + ( Delta & 0xFFFFFFFF );

				RelocationBlock.push_back ( ( uintptr_t )*LongPtr );
				RelocationBlockRVA.push_back ( ( uintptr_t )LongPtr - ( uintptr_t )RawFileMap );
				RelocationBlockRVA2.push_back ( ( uintptr_t )*LongPtr - ( uintptr_t )RawFileMap );

			}
			break;

			case IMAGE_REL_BASED_DIR64:
			{
				LongLongPtr = ( PUINT64 )( Address + Offset );
				*LongLongPtr = *LongLongPtr + Delta;

				RelocationBlock.push_back ( ( uintptr_t )*LongLongPtr );
				RelocationBlockRVA.push_back ( ( uintptr_t )LongLongPtr - ( uintptr_t )RawFileMap );
				RelocationBlockRVA2.push_back ( ( uintptr_t )*LongLongPtr - ( uintptr_t )RawFileMap );

			}
			break;

			case IMAGE_REL_BASED_HIGHADJ:
			case IMAGE_REL_BASED_MIPS_JMPADDR:
			default:
			{
				return ( PIMAGE_BASE_RELOCATION )NULL;
			}
			break;
		}

        TypeOffset++;
    }

    return ( PIMAGE_BASE_RELOCATION )TypeOffset;
}
//==================================================================================================================================
bool CPEManager::DoesResideInExecutableSection ( uintptr_t AddressToCheck )
{
	size_t SectionCounter;

	uintptr_t Address, Size, RegionBase, RegionEnd;
		
	for ( SectionCounter = 0; SectionCounter < GetSectionCount(); SectionCounter++ )
	{
		if ( SectionHeader [ SectionCounter ].Characteristics & IMAGE_SCN_MEM_EXECUTE )
		{
			Address = SectionHeader [ SectionCounter ].VirtualAddress;
			Size = SectionHeader [ SectionCounter ].Misc.VirtualSize;

			RegionBase = ( ( uintptr_t )RawFileMap + Address );
			RegionEnd = RegionBase + Size;
		
			if ( AddressToCheck >= RegionBase 
				&& AddressToCheck <= RegionEnd )
			{
				return true;
			}
		}
	}
	
	return false;
}
//==================================================================================================================================
DWORD CPEManager::GetContainingSectionCharacteristicsRVA ( uintptr_t RVA )
{
	for ( size_t SectionCounter = 0; SectionCounter < GetSectionCount(); SectionCounter++ )
	{
		if ( RVA >= ( ( uintptr_t )RawFileMap + SectionHeader [ SectionCounter ].VirtualAddress ) && RVA <=
			( ( uintptr_t )RawFileMap + SectionHeader [ SectionCounter ].VirtualAddress + SectionHeader [ SectionCounter ].Misc.VirtualSize ) )
		{
			return SectionHeader [ SectionCounter ].Characteristics;
		}
	}

	return 0;
}
//==================================================================================================================================